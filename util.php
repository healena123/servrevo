<?php

@session_start();

// is a user currently logged in ?
function isLoggedin() {
  return ! empty($_SESSION['user_id']);
}
