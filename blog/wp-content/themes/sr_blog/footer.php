<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
		<footer class="footer">
    <div class="center">
        <div class="row">
        	<div class="col-sm-7">
            <a href="#"><img class="logo" src="<?php echo BASE_URI; ?>images/serv_logo_wh.png"></a>
          </div>

          <div class="col-sm-3">
          	<div>
          		<a href="https://www.facebook.com/servrevocorp/" style="margin-right:5px;"><img src="<?php echo BASE_URI; ?>images/facebook.png" style="height:32px; width:32px;"></a>
          		<a href="https://www.linkedin.com/company/servrevo-corp/" style="margin-right:5px;"><img src="<?php echo BASE_URI; ?>images/linkedin.png" style="height:32px; width:32px;"></a>
              <a href="https://www.instagram.com/servrevocoworking/"><img src="<?php echo BASE_URI; ?>images/instagram.png" style="height:32px; width:32px;"></a>
          	</div>
          </div>
        </div>

        <div class="row" style="margin-top:1em;">
          <center>
            <p class="col" style="display:inline;">Phone: (632) 635 7626 | </p>
            <p class="col" style="display:inline;">Email: start@servrevo.com</p>
          </center>
        </div>

        <div class="address" style="margin-top:.1em;">10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</div>

    </div>
    <div class="copy col-sm-12">
    	<ul class="col-sm-6 footer-nav" style="margin:0">
    			<li><a href="<?php echo BASE_URI; ?>terms.php">Terms &amp; Condition</a> |</li>
    			<li><a href="<?php echo BASE_URI; ?>contact.php">Contact Us</a></li>
    		</ul>
    	<p class="col-sm-6">Copyright 2016 ServRevo Corp.</p>
    </div>
</footer>
		
		
		
		<footer id="colophon" class="site-footer " style="display:none" role="contentinfo">
			<div class="wrap">
				<?php
				//get_template_part( 'template-parts/footer/footer', 'widgets' );

				if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentyseventeen' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
							) );
						?>
					</nav><!-- .social-navigation -->
				<?php endif;

				//get_template_part( 'template-parts/footer/site', 'info' );
				?>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
