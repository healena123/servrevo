<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'servrevo_blog_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9RhL_!K)p9Yv+o<CjMXE4Qn88~PBT}xdE#8N^d?9XK.ZYHqO[?I0|@_X`ZcGoSF@');
define('SECURE_AUTH_KEY',  '&-^_b#}F_kDf=l*&8;6+Io3]:qQ;mvL7:0Ai/2T(cJ5(0~!:F:R)ZW%6skm19LCn');
define('LOGGED_IN_KEY',    'lm`!H|B=(t(GvUQ;bBHzoU>8Xp(01i&-|[BdF)YdoU@^}6[#@7pwU/)A{rzq8*!x');
define('NONCE_KEY',        'h{BjU4clO)rqoiqe/z,oE:E*N QM-#*S$mb*`qz]|/n@?-bx1@>ya#Heu[H{9)8K');
define('AUTH_SALT',        '01miX+F552txv$RsXi&7/)}*#OnJj[A3[,hnr<T.Qz(pdC.q2Ni $$+w PGV8N.I');
define('SECURE_AUTH_SALT', '6G2RxJpL}He{EI#@P7K=Z!uW|eQD@(EYRXGy#qZdv{tC3p)fU 5bJdg!YD5mz8FI');
define('LOGGED_IN_SALT',   '7K[m9=*m.xim ,iRi~oMeh9px5;QV3??k4mr/w];WyT;,S6;3kj.rx!(<8xVA#-8');
define('NONCE_SALT',       ')D3bjw%%hg 6=K|1&;8U k!J)s&u*Hc,&.i?q~|/;U(aH?Bx9=5J3{3:bcy%D+>@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
