//
// Main script file for client_page.php

// FIXME: Use asychronous requests; the opposite is deprecated
// FIXME: Make the codes more modular


// Input form validation flags
//
// FIXME: Possibly convert this to a state machine
var g_VALIDATION_FLAGS  = {
    // false if:
    // - already taken
    'username': true,

    // false if:
    // - length < 6
    // - does not match with the currently logged in user's password
    'current-password': true,

    // false if:
    // - length < 6
    'new-password': true,

    // false if:
    // - length < 6
    'confirm-password': true,

    // false if:
    // [new-password] != [confirm-password]
    'new-confirm-matches': true
};

$(document).ready(function() {
    //
    // Helper Functions
    //

    // form.user-id
    // form.pass
    // check if the password matches with the current logged in user's password
    function checkPassword(currentPassword) {
        console.log('checkPassword()');
        var isValid = false;

        $.ajax({
            type: 'POST',
            async: false,
            url: 'api_user_validation_v2.php?func=user-pass-matches',
            data: {
                password: currentPassword
            },
            success: function(e) {
                var x = JSON.parse(e);
                console.log(x);
                isValid = x.found;
            }
        });

        return isValid;
    };

    // checks if a username is unique, (u don't say?)
    function isUnique(username) {
        var res = null;

        $.ajax({
            type: 'GET',
            async: false,
            url: 'api_user_validation_v2.php',
            data: {
                func: 'user-attr-exists',
                x: 'username',
                val: username
            },
            success: function(e) {
                res = !JSON.parse(e).found;
            }
        });

        return res;
    }

    // FIXME: Use a regular expression
    // retuns an object:
    //  msg = the error message
    //  stat = true|false
    function validatePassword(pass) {
        var r = { msg: '', stat: '' };

        // TODO: More Validations
        if(pass.length < 6) {
            r.msg = 'Password length should be greater than 6.';
            r.stat = false;
        } else {
            r.msg = '';
            r.stat = true;
        }

        return r;
    }

    // Note: This is not being used, actually
    // obj: a DOM input object returned by a jquery selector
    // validator: a callback function that will be called on blur

    // Eror divs and matching selectors
    function addValidator(obj, validator, errMsg, errDiv) {
        obj.on('blur', function(e) {
            var elem = $(this);

            if(!validator(elem.val())) {
                // show an error message after the DOM object
                errDiv
                    .html(errMsg)
                    .removeClass()
                    .addClass('field-error');
            } else {
                // remove the error message if there i any
                errDiv.html('');
            }
        });
    }

    //
    // Event Handlers
    //

    // show the edit password fields on click
    $('#change-password').on('click', function() {
        var elem = $(this);
        var updatePassword = $('#edit-form input[name=update-password]');

        // add the required attributes on the password fields
        var switchRequired = function(r) {
            var _PASSWORD_FIELDS = [
                'current-password',
                'new-password',
                'confirm-password'
            ];

            if(r) {
                _PASSWORD_FIELDS.forEach(function(k) {
                    $('#edit-form input[name=' + k + ']').attr('required', "");
                });
            } else {
                _PASSWORD_FIELDS.forEach(function(k) {
                    $('#edit-form input[name=' + k + ']').removeAttr('required');
                });
            }
        };

        // are the password fields currently visible ?
        if(elem.data('edit-mode')) {
            // reset to default
            $('#password-field').css('display', 'none');
            $(elem).html('Change Password');
            elem.data('edit-mode', 0);
            updatePassword.val(0);
            switchRequired(false);

            // remove any error flags related to the password fields
            ([ 'current-password', 'new-password', 'confirm-password', 'new-confirm-matches' ])
                .forEach(function(i) {
                    console.log(i + ' = true; ');
                    g_VALIDATION_FLAGS[i] = true;
            });

            // remove password field values
            $('#password-field input[type=password]').val('');

            // remove error messages as well
            (['#current-password-msg', '#new-password-msg', '#confirm-password-msg'])
                .forEach(function(i) {
                    $(i).removeAttr().html('');
            });
        } else {
            // show password field
            $('#password-field').css('display', 'block');
            $('#change-password').html('Cancel');
            elem.data('edit-mode', 1);
            updatePassword.val(1);
            switchRequired(true);
        }
    });

    //
    // Validations:
    //

    // username
    $('#edit-form [name=username]').on('blur', function() {
        var elem = $(this);
        var isValid = true;

        // skip if username did not change at all
        if(elem.data('current-username') == elem.val()) {
            console.log("same username");

            $('#username-msg')
                .html('')
                .removeClass();
            g_VALIDATION_FLAGS['username'] = true;
            return;
        }

        if(isUnique(elem.val())) {
            $('#username-msg')
                .html('')
                .removeClass();
            isValid = true;
        } else {
            $('#username-msg')
                .html('That username is already taken.')
                .removeClass()
                .addClass('field-error')
            isValid = false;
        }

        g_VALIDATION_FLAGS['username'] = isValid;
    });

    // current password
    $('#edit-form input[name=current-password]').on('blur', function() {
        var elem = $(this);
        var isValid = true;

        // ignore if empty
        if(!elem.val()) {
            $('#current-password-msg')
                .html('')
                .removeClass();
            return;
        }

        if(checkPassword(elem.val())) {
            // show success message
            $('#current-password-msg')
                .html('Current password matches')
                .removeClass('field-error')
                .addClass('field-ok');
        } else {
            // show error message
            $('#current-password-msg')
                .html('Current password does not match')
                .removeClass('field-ok')
                .addClass('field-error');
            isValid = false;
        }

        g_VALIDATION_FLAGS['current-password'] = isValid;
    });

    // new password + confirm password
    $('#edit-form input[name=new-password], #edit-form input[name=confirm-password]')
        .on('blur', function() {
        var elem = $(this);
        var newPass = $('#edit-form input[name=new-password]');
        var confirmPass = $('#edit-form input[name=confirm-password]').val();
        var checkResult = validatePassword(elem.val());
        var name = $(this).attr('name');

        // ignore if empty
        if(!elem.val())  {
            return;
        }

        // validate password pattern first
        if(!checkResult.stat) {
            console.log("error in password: " + checkResult.msg);
            $('#new-password-msg')
                .html(checkResult.msg)
                .removeClass()
                .addClass('field-error');
            g_VALIDATION_FLAGS[name] = false;
            return;
        } else {
            g_VALIDATION_FLAGS[name] = true;
        }

        // proceed by asserting that new & current password matches
        if(newPass.val() == confirmPass) {
            $('#new-password-msg')
                .html('New and Confirm Passwords Match')
                .removeClass('field-error')
                .addClass('field-ok');
            g_VALIDATION_FLAGS['new-confirm-matches'] = true;
        } else {
            console.log("passwords do not match");
            $('#new-password-msg')
                .html('New and Confirm Password do not Match')
                .removeClass('field-ok')
                .addClass('field-error');
            g_VALIDATION_FLAGS['new-confirm-matches'] = false;
        }
    });

    // Form Submit
    // Issue #1: Conflict with the modal plugin

    $('#edit-form').on('submit', function(e) {
        // Check if the fields are valid
        console.log(g_VALIDATION_FLAGS);

        // prevent submission if there is an invalid field
        for(var i in g_VALIDATION_FLAGS) {
            if(!g_VALIDATION_FLAGS[i]) {
                e.preventDefault();
                break;
            }
        }
    });

    // Manually trigger details tab content
    $('#profile-tab-trigger').trigger('click').addClass('active');
});
