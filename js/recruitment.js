/**
 * Created by Lon on 6/30/2017.
 */

jQuery(document).ready(function(){
	
	
    jQuery('#serve-revo-main.serve-revo-offshore .row1-body #mainCarousel').carousel({
        interval: false,
        pause: true
    });

    jQuery(".carousel").swipe({

        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'left') jQuery(this).carousel('next');
            if (direction == 'right') jQuery(this).carousel('prev');

        },

    });

    var heights = $("#serve-revo-main.serve-revo-offshore .row2-container-box .row-box-content").map(function() {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);

    $("#serve-revo-main.serve-revo-offshore .row2-container-box .row-box-content").height(maxHeight);

    var heights = $("#serve-revo-main.serve-revo-offshore .row2-body .row2-icon-box-bordered").map(function() {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);

    $("#serve-revo-main.serve-revo-offshore .row2-body .row2-icon-box-bordered").height(maxHeight);
});

/**
 * Created by Lon on 6/8/2017.
 */
$(document).ready(function(){
	var base_url = $('.baseurl').val();
	
	$.ajax({
		'url': base_url+'json/events.php'
	});
	
    jQuery('#serve-revo-main.serve-revo-offshore .row1-body #mainCarousel').carousel({
        interval: false,
        pause: true
    });

    if($(window).width() >= 768)
    {
        var heights = $(".row2-container-box").map(function() {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".row2-container-box").height(maxHeight);

        var heights = $(".custom-col-r3").map(function() {
                return $(this).height();
            }).get(),

            maxHeight = Math.max.apply(null, heights);

        $(".custom-col-r3").height(maxHeight);
    }

    $(function() {
        var data_slide_index = ""
        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/"));
        // console.log(pgurl);
        $("#serve-header-bar li a").each(function(index, element){
            var link = $(this).attr('href');
            // console.log(index);
            if(link == pgurl){
                $(this).addClass("active");
                data_slide_index = $(this).data("href-index");
            }else{
                $('a[href$="/servrevo"]').addClass('active');
            }
        })

        $("ol.carousel-indicators li").each(function(index, element){
            var data_carousel = $(this).data('slide-to');
            // console.log(data_carousel);
            if(data_carousel == data_slide_index) {
                $(this).addClass("active");
                $(this).trigger("click");
                var path = base_url+"images/recruitment/slider-img0.jpeg";
                //var path = "images/co-working/slider-img"+data_slide_index+".jpeg";
                // alert(path);
                $("#serve-revo-main.serve-revo-offshore .row1-body").css("background-image",'url(' + path + ')');

            }else{
                $('#myCarousel').carousel($(this).data('slide-to')-1).addClass('active');
            }
        })
    });

});