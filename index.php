<?php
session_start();
include_once ('config.php');
?>

<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>BPO Solutions for Better Business Growth | ServRevo Philippines</title>
    <meta name="description" content="We take Business Process Outsourcing to a whole new level. Build your offshore team with ServRevo's cost-effective services. From recruitment to co-working space, we'll take care of your business."/>

    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114355569-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  			gtag('js', new Date());

  			gtag('config', 'UA-114355569-1');
	</script>

</head>
<body id="serve-revo-main">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<?php include('main-header.php'); ?>

<div class = "container-fluid">
    <div class = "row row1-body">
    	<div class = "col-lg-6 col-md-6 col-sm-12 col-xs-12"></div>

        <div class = "col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h1>Simple. Low-cost. <br class="h1-wide"> Looking for space or staff? Start with ServRevo.</h1>
            <p>ServRevo is a business process solutions company providing co-working space for individuals or teams, including shared and staff-leasing services to companies looking for Manila-based staff. </p>
            <p>Our bespoke offshore solutions integrate seamlessly with any business requirement - providing a full range of services covering recruitment, human resources, payroll management and serviced offices.</p>
            <div class = "serve-start">
              <?php if($_SESSION['user_id'] != ''){?>
                <a href="<?php echo BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id'];?>" class="btn btn-serve-start" role="button">LET'S START</a>
              <?php } else { ?>
                <a href="<?php echo BASE_URI; ?>reservation.php" class="btn btn-serve-start" role="button">LET'S START</a>
              <?php }; ?>
            </div>
        </div>
    </div>
</div>

<div class = "container container2">
    <div class = "row row2-body">
        <div class = "col-sm-12 col-xs-12">
            <!--<h3>Bespoke staffing solutions for every need</h3>-->
            <h3>Bespoke solutions at fair prices<br />From workspaces to staffing needs - we’ve got you covered.</h3>
            <p>Scouting for a place to work? ServRevo is located inside the premier Ortigas business district in Manila. <br />Our workspace has desks and rooms ready to rent out for months, days or weeks!</p>
            <p>Trying to build a team or go offshore? Go for our responsive, high-touch approach to outsourcing. <br />We guarantee clients get the talent and expertise they need without breaking the bank.</p>
        </div>
    </div>
    <div class = "row row2-body box">
    	<div class = "col-lg-3 col-md-3 col-sm-6 col-xs-12 custom-col">
        <?php if($_SESSION['user_id'] != ''){?>
            <a href="<?php echo BASE_URI; ?>offshore-services.php?user=<?php echo $_SESSION['user_id']; ?>">
            <?php } else{ ?>
                <a href="<?php echo BASE_URI; ?>offshore-services.php">
                <?php } ?>
                <div class = "row2-container-box">
                    <img src="images/row2-img1.png">
                    <h4>Offshore Services</h4>
                    <p>Get skilled talent for day-to-day back-office, non-voice, technical or  non-technical tasks at a fraction of the cost.</p>
                </div>
            </a>
        </div>
    	<div class = "col-lg-3 col-md-3 col-sm-6 col-xs-12 custom-col">
        <?php if ($_SESSION['user_id'] != '') {?>
          <a href="<?php echo BASE_URI; ?>co-working.php?user=<?php echo $_SESSION['user_id']; ?>">
        <?php } else{ ?>
            <a href="<?php echo BASE_URI; ?>co-working.php">
            <?php } ?>
                <div class = "row2-container-box">
                    <img src="images/row2-img2.png">
                    <!--<h4>Seat Leasing</h4>
                    <p>Secure quick and easy workspaces for lease at a premier district.</p>-->
                    <h4>Co-working Space</h4>
                    <p>Rent seats or rooms inside a fresh, modern workspace at a premier business district.</p>
                </div>
            </a>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-6 col-xs-12 custom-col">
          <?php if($_SESSION['user_id'] != ''){ ?>
            <a href="<?php echo BASE_URI; ?>payroll-management.php?user=<?php echo $_SESSION['user_id']; ?>">
          <?php } else{ ?>
            <a href="<?php echo BASE_URI; ?>payroll-management.php">
            <?php } ?>
                <div class = "row2-container-box">
                    <img src="images/row2-img3.png">
                    <h4>HR & Payroll Management</h4>
                    <p>Focus on growing your business with our managed services that take the hassle out of employee benefits.</p>
                </div>
            </a>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-6 col-xs-12 custom-col">
          <?php if($_SESSION['user_id'] != ''){ ?>
            <a href="<?php echo BASE_URI; ?>recruitment-staffing.php?user=<?php echo $_SESSION['user_id']; ?>">
          <?php }else{ ?>
            <a href="<?php echo BASE_URI; ?>recruitment-staffing.php">
            <?php } ?>
                <div class = "row2-container-box">
                    <img src="images/row2-img4.png">
                    <h4>Recruitment Staffing</h4>
                    <p>Build a team now to increase your capabilities. We can help you find and work with talented professionals.</p>
                </div>
            </a>
        </div>
    </div>

</div>
<div class = "container-fluid container3">
    <div class = "row row3-body">
        <div class = "col-lg-6 col-md-6 col-sm-12 col-xs-12 custom-col-r3 custom-col1">

        </div>
        <div class = "col-lg-6 col-md-6  col-sm-12 col-xs-12 custom-col-r3 custom-col2">
            <h3>Own the race. Dictate the pace.</h3>
            <!--<h4>Outsource in a snap to improve your business and run ahead of the competition.</h4>-->
            <h4>Outsource in a snap to improve your business and stay ahead of the competition.</h4>
            <div class = "row">
                <div class = "col-sm-1 col-xs-1 custom-col-1"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                <div class = "col-col-sm-11 col-xs-11 custom-col-11">
                    <h4>Source</h4>
                    <p>Get expert recruitment and processing to hire the best possible people.</p>
                </div>
                <div class = "col-sm-1 col-xs-1 custom-col-1"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                <div class = "col-sm-11 col-xs-11 custom-col-11">
                    <h4>Setup</h4>
                    <p>Lease a seat, space or room to house your staff.</p>
                </div>
                <div class = "col-sm-1 col-xs-1 custom-col-1"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                <div class = "col-sm-11 col-xs-11 custom-col-11">
                    <h4>Start</h4>
                    <p>Work seamlessly with our shared service support.</p>
                </div>
            </div>
            <div class = "serve-start">
                <a href="#call-back" class="btn btn-serve-start" role="button">LET'S START</a>
            </div>
        </div>
    </div>
</div>
<div class = "container">
    <div class = "row row4-body"></div>
</div>
<div class = "container">
    <div class = "row row5-body">
        <div class = "col-sm-12 col-xs-12">
            <h3>TEAM UP WITH SERVREVO</h3>
            <br />
            <!--<h4>Providing solutions that let you leap over competition.</h4>-->
            <!--<p>ServRevo is a business process solutions company providing shared and staff-leasing services to companies looking for Manila-based staff. </p>-->
            <p>ServRevo is a business process solutions company providing co-working space for individuals or teams, including shared and staff-leasing services to companies looking for Manila-based staff. </p>
            <p>Our flexible offshore solutions enable clients to keep up with changing customer
                demands and competitive global markets by supporting industry, process and
                segment specific needs. Our best-in-class operations can help you achieve your
                business goals and gain the competitive edge needed to grow by leaps and bounds.</p>
        </div>
    </div>
</div>
<div class = "container-fluid">
    <div class = "row row6-body">
        <div class = "col-lg-7 col-md-7">

        </div>
        <!-- modified 05/22/18 -->
        <form id="createAccount" action="process_inquire.php" class="col-lg-4 col-md-4 col-sm-12 col-xs-12" method="post" autocomplete="off">
       		<input type="hidden" name="action" value="1">
            <div id="call-back" class = "call-back">
                <h3>CREATE YOUR OFFSHORE DREAM TEAM</h3>
                <hr>
                <div class="err-msg invi hidden"><i class="fa fa-exclamation-circle"></i>&nbsp;Please fill up the required fields below.</div>

                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control required" type="text" name="inquire_name" id="name" placeholder="&#xf2c0; Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control required" type="text" name="inquire_email" id="email" placeholder="&#xf003; Email">
                            </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control required" type="text" name="phone" id="phone" placeholder="&#xf10b; Phone">
                            </div>
                        </div>
                    -->
                        <div class="form-group">
                            <div class="col-12">
                                <textarea rows="3" class="form-control" name="inquire_msg" id="message" style="resize: none;" placeholder="&#xf1d9; Tell us what you need"></textarea>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-serve-callback" id="registerBtn">LET'S START</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include('footer.php'); ?>

</body>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>
var base_url = $('.baseurl').val();
$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}



});

</script>
</body>
</html>
