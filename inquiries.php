<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
    header("Location: ".BASE_URI);
} else {
    if($_SESSION['access_id'] == '4'){
        header("Location: ".BASE_URI);
    }
}

$user_id = $_GET['user'];
$inquiries = "select * from Inquiries";



/*while($row = mysqli_fetch_array($q)) {
    $inquiries[] = [
        'id' => $row['id'],
        'name' => $row['inquire_name'],
        'email' => $row['inquire_email'],
        'msg' => $row['inquire_msg'],
        'date' => $row['date_created']
    ];
}*/
?>

<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard.css"/>

    <style>
        .fc-view-container {
            background:#fff
        }
        .fc-header-toolbar h2 {
            font-size:16px;
            margin-top:10px
        }
        .fc-content {
            font-size:10px;
        }
        .fc-widget-header span {
            font-size:12px;
        }
    </style>
</head>
<body class="serve-revo-admin">
    <input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
    <?php include('dashboard_header.php'); ?>
    <!-- main content -->
    <div class="content-wrap">
        <div class="left">
            <div class="accordion-menu">
                <div class="col">
                    <div class="acc-menu-link">
                        <a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                            <i class="fa fa-folder"></i> Dashboard
                        </a>
                    </div>
                </div>

                <div class="col">
                    <div class="acc-menu-link">
                        <a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
                            <i class="fa fa-folder"></i> Client Bookings
                        </a>
                    </div>

                </div>
                <div class="col">
                    <div class="acc-menu-link">
                        <a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
                            <i class="fa fa-folder"></i> Users
                        </a>
                    </div>

                </div>
                <div class="col">
                    <div class="acc-menu-link">
                        <a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
                            <i class="fa fa-folder"></i> Archive
                        </a>
                    </div>

                </div>
                <div class="col">
                <div class="acc-menu-link">
                    <a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
                        <i class="fa fa-folder"></i> Reports
                    </a>
                </div>

            </div>
            </div>
        </div>
        <div class="right">
            <div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
                <h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Inquiries</h3>
                <div class="breadcrumbs" style="padding:0;margin-top:3px">
                    <a href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>
                </div>
            </div>
            <div style="background:#fff;">
                <!-- begin table -->
                <table id="inquiriesDataTable" class="table" style="width:100%;font-size:12px;">
                    <thead>
                        <tr>
                            <th style="width:20px; padding-right:0"></th> <!-- delete icon -->
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
    					 	if($result = mysqli_query($connect, $inquiries)){
								if(mysqli_num_rows($result) > 0){
									while($i = mysqli_fetch_array($result)){
								?>
                        <tr data-user-id="">
                            <td style="width:20px;padding-right:0">
                                <!--<a class="trash-tr fa fa-trash"> </a>-->
                            </td>

                            <td style="width:150px;"><?php echo $i['inquire_name']; ?></td>
                            <td style="width:150px;"><?php echo $i['inquire_email']; ?></td>
                            <td><?php echo $i['inquire_msg']; ?></td> <!-- fixme: excerpt-->
                            <td style="width:150px;"><?php echo $i['date_created']; ?></td>
                        </tr>
                        <?php }}} ?>
                    </tbody>
                </table>
                

            </div>
        </div>
    </div>


    <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.dataTables.js"></script>
    <script src="js/dataTables.bootstrap.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/datatable.responsive.js"></script>
    <script src="js/dataTables.fixedColumns.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>

    <!-- CUSTOM SCRIPTS -->
    <script src="js/main.js" type="text/javascript"></script>

    <script>
        var base_url = $('.baseurl').val();

        $(document).ready(function() {
            $('inquiriesDataTable').DataTable({
                "paging": true,
                "info": true,
                "bLengthChange": false,
                "bDestroy": true,
                "order": [[ 1, "desc"]],
                responsive: true,
                "dom": '<"pull-left"B><"pull-right"lfr>tip',
                buttons: [
                ]
            })

        });

        // trash button
        $(document).on('click', 'trash-tr', function() {

        });
    </script>

</body>
</html>
