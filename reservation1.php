<?php
session_start();

include_once ('config.php');

$user_id = isset($_GET['user']) ? $_GET['user'] : '';
if($user_id){
	$user_info = "Select * from Users where id='$user_id'";

	$seat_ref = "Select * from Seats_ref";

	$reserved_seats = "
	Select sr.id as reservation_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
";



 if($result = mysqli_query($connect, $user_info)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		$username = $row['username'];
    		$password = $row['password'];
    		$email = $row['email'];
    	}
    }
}

}

?>

<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>


    <link rel="stylesheet" type="text/css" href="css/admin.css"/>

     <link rel="stylesheet" type="text/css" href="css/style.css"/>
     <link rel="stylesheet" type="text/css" href="css/layout.css"/>
     <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114355569-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  			gtag('js', new Date());

  			gtag('config', 'UA-114355569-1');
	</script>

<style>

/*	.ui-widget-header {
    	border: transparent !important;
    	background: transparent !important;
    	color: #333333;
    	font-weight: bold;
	}
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
    	border: transparent;
    	background: #f6f6f6;
    	font-weight: normal;
    	color: #ccc;
    	font-size: 14px;
	}
	.ui-datepicker table {
    	width: 100%;
    	font-size: 12px;
   	 	border-collapse: collapse;
   	 	margin: 0 0 .4em;
	}



	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
    	border: transparent !important;
    	background: transparent;
    	color: #fff;
	}
	.ui-state-highlight {
		background:transparent;
		border:transparent;
	}
	.ui-state-highlight .ui-state-default {
		border:transparent;
		background:rgba(136,168,31,1) !important;
		color:#fff !important;
	}

	.ui-datepicker-today > a {
		border: transparent !important;
    	background: #fffa90 !important;
    	color: #777620 !important;
	}




.ui-widget.ui-widget-content {
    border: transparent !important;
}*/
/*.ui-datepicker {
    width: 100%;

}
.ui-datepicker td span, .ui-datepicker td a {
    padding: 14px .2em;


}*/

</style>

</head>
<body id="serve-revo-main" class="reservation-page">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">

<?php include('main-header.php'); ?>

<div class="container" style="padding:40px 0 120px;display:inline-block;width:100%">
	<h3 class="col-sm-12" style="margin-bottom:20px;">Available Seats</h3>
	<div class="center-lg">
 	   <div class="container-m">
 		<div class="left-content" style="overflow:hidden">


 		<div class="left-content-inner" style="overflow:scroll">
        	<input type="hidden" id="selected-dates">
        	<!-- Legend-->
        	<div class="row reservation-legend-holder hidden">

        				<div class="seat-placement-legend col-xs-12">
        					<div class="col-xs-4 legend-item">
	        					<p class="legend-p">
    	    						<span class="legend picked"></span>
        							<strong class="legend-name">PICKED</strong><br />
        							You chose these seats. <br /> Click to unchoose.
        						</p>
	        				</div>
    	    				<div class="col-xs-4 legend-item">
	        					<p class="legend-p">
    	    						<span class="legend available"></span>
        							<strong class="legend-name">AVAILABLE</strong><br />
        							These seats are available. <br /> Click to choose.
	        					</p>
    	    				</div>
        					<div class="col-xs-4 legend-item">
        						<p class="legend-p">
        							<span class="legend reserved"></span>
	        						<strong class="legend-name">RESERVED</strong><br />
    	    						These seats are reserved. <br /> You can’t pick them.
        						</p>
        					</div>
        				</div>
        			</div><!--/ Legend-->


        	<div class="content-loader hidden" style="position:absolute;top:0;left:0;z-index:999;background:#eee;text-align:center;opacity:0.6;width:100%;height:100%">
        		<img style="display:inline-block;margin-top:100px;width:94px;height:94px;" src="images/loader_blocks.gif"></div>
        	<div id="dateSelected" style="display:none"></div>
        	<div class="seat-placement-holder" style="">
        		<img src="images/seat-placement-bg1.png" style="width:100%;position:absolute;top:0;left:0;z-index:0">
        		<div id="seatPlacement"></div>
        	</div>

        	<div id="recipientEmailList" style="display:none">
        		<textarea name="recipient_email_address" class="form-control" rows="5"></textarea>
        	</div>
        </div><!-- left content inner-->


        </div><!-- left content-->

 		<div class="right-content" style="border: solid 3px #EBEBEB;background-color: #fff;text-align: center;padding: 1% 3%;">
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="paypal">
										<input type="hidden" name="cmd" value="_s-xclick">
										<input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN">
										<!--<input type="submit" class="btn btn-block book-reservation-btn" style="text-transform:uppercase;font-size:10px !important;background:#87a900;color:#fff;" value="Pay">
										<!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
										<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
										</form>
        	<form id="processReservation" action="<?php echo BASE_URI; ?>process_reserve.php" method="post" autocomplete="off" style="display:inline-block;height:100%">
            	<h3>Reserve Seats</h3>
            	<h4 style="margin-top:20px;">Select Dates</h4>
				<br />
				<input type="hidden" name="reservation_dates" id="reservation-dates">
				<input type="hidden" name="reservation_seats" id="reservation-seats">
				<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
				<input type="hidden" name="total_days_reserved" id="total-days">
				<input type="hidden" name="total_seats_reserved" id="total-seats">
				<input type="hidden" name="total_reservation_amt" class="total-amt-hdn" value="0">

				<div class="err-msg"></div>
		<div class="row">
			<div class="col-xs-12" style="">
                <div class="form-group" style="display:inline-block;text-align:right;">
                	<label class="col-xs-4" style="padding:5px 15px 0 0">Start</label>
                	<span class="col-xs-7" style="padding:0;">
                		<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
    					<input type="text" style="padding-left:30px;" name="date_from" onkeyup="getnewdate()" class="form-control required datepicker-input" id="datepickerInput1" size="50">
  					</span>
  				</div>
  			</div>
  			<div class="col-xs-12">
                <div class="form-group" style="display:inline-block;text-align:right;">
                	<label class="col-xs-4" style="padding:5px 15px 0 0">End</label>
                	<span class="col-xs-7" style="padding:0;">
                		<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
    					<input type="text" style="padding-left:30px;" name="date_to" class="form-control datepicker-input" id="datepickerInput2" size="50">
  					</span>
  				</div>
             </div>

             <div class="reserve-other-info" style="display:inline-block;width:100%;font-size:16px;font-weight:400;margin-bottom:20px;">

  				<div class="col-xs-12">
                	<div class="form-group" style="width:100%;display:inline-block;text-align:right;">
                		<label class="col-xs-4" style="padding:5px 15px 0 0">Qty</label>
                		<span class="col-xs-7" style="padding:0;">
                			<input type="text" style="width:70px;display:inline-block;margin-right:10px;" class="form-control required disabled" value="0" placeholder="0" name="total_seat_reserved" id="total-seats-reserve" size="50">
                			x P<strong class="seat-price-unit">400</strong>
                		</span>
               		</div>
  					<div class="date-range-view" style="margin-bottom:10px;"></div>
  					<strong style="color:#000;font-weight:700;font-size:20px;margin-top:5px;line-height:15px;">P</strong>
  					<strong class="seat-total-amt" style="color:#000;font-weight:700;font-size:20px;margin-top:5px;line-height:15px;">0</strong>
  				</div>
  			</div>
         </div>



                <div class="input" style="margin-bottom:20px;">
                    <!--<button type="button" id="checkAvailabilityBtn" class="btn btn-serve-callback">Check Availability</button>-->
                    <?php if(isset($_SESSION['user_id'])){ ?>
					<input form="paypal" id="reserveBtn" type="submit" class="btn btn-serve-callback" value="Pay">
					
                    <!--<button type="button" id="reserveBtn" class="btn btn-serve-callback" >Pay</button>-->
					
                    <?php }else { ?>
                    	<a href="<?php echo BASE_URI; ?>login.php" class="btn btn-serve-callback">Pay</a>
					<?php } ?>
					
                </div>
				
				 <div class="input" style="margin-bottom:20px;">
                    <!--<button type="button" id="checkAvailabilityBtn" class="btn btn-serve-callback">Check Availability</button>-->
                    <?php if(isset($_SESSION['user_id'])){ ?>
                    <button type="button" id="reserveBtn" class="btn btn-serve-callback" style=" background-color: #004d00 !important;">Reserve</button>
                    <?php }else { ?>
                    	<a href="<?php echo BASE_URI; ?>login.php" class="btn btn-serve-callback">Reserve</a>
					<?php } ?>
					
                </div>

            	<div class="seat-status">
            		<p><strong class="no-of-seats">0</strong> seats selected</p>
            	</div>
        	</form>
        </div>
       </div>
    </div>

    <div class="modal fade account-modal" id="myModal" role="dialog" aria-labelledby="myModal">
    	<div class="modal-dialog" role="document" style="width:30%;">
        	<div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                	<h6 class="modal-title" id="addAccountLabel">New Check Voucher</h6>
            	</div>
           	 	<div class="modal-body">
           	 		<?php if($username=='' && $password==''){ ?>
           	 			<div class="alert alert-danger" style="font-size:12px;">
           	 				Please fill up the following to complete the registration.
           	 			</div>
           	 		<?php } ?>
           	 		<form action="process_register.php" method="post" id="completeRegisterForm" autocomplete="off">
            			<input type="hidden" name="action" value="2">
            			<input type="hidden" name="user" value="<?php echo $user_id; ?>">
            			<div class="form-group mb20">
    						<label for="username" class="lbl-sm">Username</label>
    						<input type="text" class="form-control required" name="username" id="username" size="50">
  						</div>

                		<div class="form-group mb20">
                    		<label for="emailAddressFld" class="lbl-sm">Password</label>
                    		<input type="password" class="form-control required" name="password" id="user-password" size="50">
                    	</div>

                		<div class="form-group mb20">
                   			<label for="emailAddressFld" class="lbl-sm">Confirm Password</label>
                    		<input type="password" class="form-control required" name="confirm_password" id="confirm-password" size="50">
                 		</div>

                		<div class="form-group text-left" style="font-size:12px;">
                			<input type="checkbox" name="terms-condition" id="terms-condition">&nbsp;I agree to the Terms and conditions
                		</div>

                		<div class="form-group text-left">
                			<button type="button" class="btn btn-serve-start" id="completeRegisterBtn">PROCEED</button>
                		</div>

        			</form>
           	 	</div>
           	 </div>
        </div>
    </div>

	<div class="modal fade account-modal" id="datepicker-modal" role="dialog" aria-labelledby="datepicker-modal">
    	<div class="modal-dialog" role="document" style="width:40%;">
        	<div class="modal-content">
            	<div class="modal-body">
           	 		<div id="datepicker"></div>
           	 	</div>
           	 </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/jquery-ui.min.js"></script>
<!-- PLUGIN SCRIPTS -->
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>

<script type="text/javascript">
var base_url = $('.baseurl').val();

function init(){
	var currentDate = new Date();

	var m = currentDate.getMonth()+1;
	var d = currentDate.getDate();
	m = m<10?'0'+m:m;
	d = d<10?'0'+d:d;
	var date_from = currentDate.getFullYear() +'-'+m+'-'+d;
	var date_to = currentDate.getFullYear() +'-'+m+'-'+d;

	var w = date_from + ','+date_to;

	getAvailableSeats(w);
}
$(document).ready(function(){
	seatPlacement('');
	init();

})
$( document ).ready(function() {
});
$(document).on('focus', '.datepicker-input', function(){
	
	    var dateToday = new Date();
	    var dates = $("#datepickerInput1, #datepickerInput2").datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: dateToday,
				beforeShowDay: noWeekendsOrHolidays,
	      onSelect: function(selectedDate) {
	          var option = this.id == "datepickerInput1" ? "minDate" : "maxDate",
	          instance = $(this).data("datepicker"),
	          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	          dates.not(this).datepicker("option", option, date);
									console.log(selectedDate);
									getAvailableSeats('');
									updateReservationDetails();
									computeTotalAmount();
	      }
	    });
	// var dateToday = new Date();
	// $(this).datepicker({
	// 	dateFormat: 'yy-mm-dd',
	// 	minDate: dateToday,
	// 	beforeShowDay: noWeekendsOrHolidays,
	// 	onSelect: function(dateStr) {
	// 			console.log(dateStr);
	// 			getAvailableSeats('');
	// 			updateReservationDetails();
	// 			computeTotalAmount();
	// 		},
	// 		onClose: function(selectedDate) {
	//         	$('.content-loader').removeClass('hidden');
	//         	if(selectedDate == '') $('.content-loader').addClass('hidden');
	//       	}
	// 	});
})
function getAvailableSeats(w){
if(w==''){
	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	w = date_from + ','+date_to;
}
	var url = base_url+'reserved_seats.php?dates='+w;

	$.post(url, '', function (data) {
		//clear Seats selection
		$('#total-seats').val(0);
		$('#total-seats-reserve').val(0);

		arr=[];
		if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
				$('.content-loader').addClass('hidden');
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});
			$('.content-loader').addClass('hidden');
		}

		});
}
$(document).on('click', '#checkAvailabilityBtn', function(){

	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	var w = date_from + ','+date_to;

	if(date_from == ''){
		$('.err-msg').append('Please select dates.');
	} else {
		$(this).html('<img src="'+base_url+'images/ajax-loader.gif">');
		$('.seat-placement-holder').css({'opacity':'0.5'});

		$('.err-msg').empty();
		var url = base_url + 'reserved_seats.php?dates='+w;

		$.post(url, '', function (data) {
			if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});

		}

		});
	}
});
var arr = [];
$(document).on('change', '#seatPlacement .chk', function(){

	var el = $(this).val();

	$(this).parent().find('input, label').addClass('selected');

	arr.push(el);


	if (!$(this).is(":checked")) {
		for(var i = arr.length; i--;) {
          if(arr[i] === el) {
              arr.splice(i, 1);
          }
        }
	}

	$('.no-of-seats').html(arr.length);
	$('#total-seats-reserve').val(arr.length);
	$('#total-seats').val(arr.length);



	var p = "";
	var total = 0;
	$.each(arr, function(i,v){
    	p += v+',';

    });
    $('#reservation-seats').val(p);


	if(arr.length > 0) {
		$('.reserve-other-info').removeClass('hidden');
		$('#checkAvailabilityBtn').addClass('hidden');
	} else {
		$('.reserve-other-info').addClass('hidden');
		$('#checkAvailabilityBtn').removeClass('hidden');
	}
	updateReservationDetails();
	computeTotalAmount();
});
$(document).on('click', '#reserveBtn', function(e){

		if($('#datepickerInput1').val() == '' || $('#datepickerInput2').val() == ''){
			$('.err-msg').html('Fields cannot be empty.');
			e.preventDefault();
		} else if($('#total-seats-reserve').val() == 0){
			e.preventDefault();
			$('.err-msg').html('Please select seats.');
		}else{
			
			$('.err-msg').empty();
			$('#processReservation').submit();
		}


});
function computeTotalAmount(){
	var d = ($('#total-days').val() != '') ? $('#total-days').val() : 0;
	var s = ($('#total-seats').val() != '')?$('#total-seats').val() : 0;
	var p = 400;
	var total = parseInt(d)*parseInt(s)*parseInt(p);

	$('.total-amt-hdn').val(total);
	$('.seat-total-amt').text(total);
}
function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }
function seatPlacement(data){

	var content = '';
		var s_id = 0;
		for(var i=1;i<=9;i++){
			content += '<div class="entry-tbl tbl-"'+i+'">';


        	for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";
        		var reserved_class="reserved";

        	//content += '<span><input type="checkbox" name="seat_no[]" value="'+seat_id+'" '+check_class+'></span>';
        		content += '<span style="position:relative;">'
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';

        					}
        				}
        				el += ' class="chk reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk reserved"></label>'

				content += '</span>';
        	}
        	content += '</div>';
        	}

        	content += '</div>';

        	if(i%3==0){
        		content += '<div class="col-xs-12" style="display:inline-block;width:100%;float:left:margin:0;"></div>';
        	}
        }
        // LAST TABLE
        content += '<div class="entry-tbl last-tbl-placement tbl-'+i+'">';

        for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";

        		content += '<span style="position:relative;">';
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';
        					}
        				}
        				el += ' class="chk chk1 reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk chk1">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk lbl-chk1 reserved"></label>'

				content += '</span>';

        	}
        	content += '</div>';
        }
        content += '</div>';


        $('#seatPlacement').html(content);
        $('.reservation-legend-holder').removeClass('hidden');
	}
$(document).on('change', '.datepicker-input', function(){
	var el = $(this).val();
	updateReservationDetails();
});
function updateReservationDetails(){
		var start = $("#datepickerInput1").datepicker("getDate"),
   		end = $("#datepickerInput2").datepicker("getDate"),
    	currentDate = new Date(start.getTime()),
    	between = [],
    	count = 0,
    	sd,
		reservation_dates = '',
		start_date = changeDateFormat(start),
		date_range="";
		console.log(end);
		//COUNT NUMBER OF DAYS
		//LIST DATES BETWEEN DATE RANGE
		if(start != null && end != null) {
			while (currentDate <= end) {

				if(currentDate.getDay() < 6 && currentDate.getDay() != 0)
					between.push(new Date(currentDate));
    			currentDate.setDate(currentDate.getDate() + 1);
			}
			count = between.length;
		}
		if(start != null && end == null){
			count = 1;
		}

		//CHANGE FORMAT OF DATES ALL DATES
		//SAVE TO INPUT HIDDEN

		if(start_date != null) {
			reservation_dates = $("#datepickerInput1").val();
		}
		if(start != null && end != null) {
			reservation_dates = "";
			$.each(between, function(i,v){

				sd = new Date(v);
				var m = sd.getMonth() + 1;
				var d = sd.getDate();
				m = m < 10? '0'+m: m;
				//d = d < 10? '0'+d: d;
				var nd = sd.getFullYear()+'-'+m+'-'+d;
				reservation_dates += nd+',';
			});
		}
		//FORMAT DATE RANGE FOR VIEW
		if(start != null && end != null) {
			date_range = changeDateFormat(start) +' - '+ changeDateFormat(end);
		}else {
			if(start != null && end == null)
				date_range = changeDateFormat(start);
			if(start == null && end != null)
				date_range = changeDateFormat(end);
		}
		//DISPLAY INFO
		$('#total-days').val(count);
		$('.date-range-view').text(date_range);
		$('#reservation-dates').attr('value', reservation_dates);
	}
function changeDateFormat(date){

	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

	var d = new Date(date);
	return monthNames[d.getMonth()] + ' '+ d.getDate() + ', ' + d.getFullYear();
}
function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}
var natDays = [
  [1, 1, 'ph'],
  [12, 25, 'ph']
];
function nationalDays(date) {
    for (i = 0; i < natDays.length; i++) {
      if (date.getMonth() == natDays[i][0] - 1
          && date.getDate() == natDays[i][1]) {
        return [false, natDays[i][2] + '_day'];
      }
    }
  return [true, ''];
}
/*$(".selector").datepicker({ beforeShowDay: noWeekendsOrHolidays})

function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}*/
/*$(document).on('click', '.datepicker-input', function(ev){
	ev.preventDefault();
	$('#datepicker-modal').removeData();
	$('#datepicker-modal').val(null).trigger("change");
	var el = $(this).attr('id');

	$('#datepicker-modal').modal('show');
	var dateToday = new Date();

	jQuery("#datepicker").datepicker({
		minDate: dateToday,
    	onSelect: function (dateText, inst) {
    		var dates = addOrRemoveDate(dateText);

			$(this).data('datepicker').inline = true;

			$('#datepicker-modal').modal('hide');
        },
        onClose: function() {
    		$(this).data('datepicker').inline = false;
		},
        beforeShowDay: function (date) {
            var year = date.getFullYear();
            // months and days are inserted into the array in the form, e.g "01/01/2009", but here the format is "1/1/2009"
            var month = padNumber(date.getMonth() + 1);
            var day = padNumber(date.getDate());
            // This depends on the datepicker's date format
            var dateString = month + "/" + day + "/" + year;

            var gotDate = jQuery.inArray(dateString, dates);
            if (gotDate >= 0) {
                // Enable date so it can be deselected. Set style to be highlighted
                return [true, "ui-state-highlight"];
            }
            // Dates not in the array are left enabled, but with no extra style
            return [true, ""];
        }

    });
});
// Maintain array of dates
var dates = new Array();

function addDate(date) {
    if (jQuery.inArray(date, dates) < 0) {
        dates.push(date);
        $('#selected-dates').val(dates);
   	}
   	$('#reservation-dates').val(dates);

   	$('#dateSelected').empty();
   	var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    }else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

   	$.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });

}

function removeDate(index) {
    dates.splice(index, 1);
    $('#selected-dates').val(dates);
    $('#reservation-dates').val(dates);

    $('#dateSelected').empty();

    var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    } else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

    $('#datepicker-input').val(dates.length + ' selected');
    $.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });
}

// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
    var index = jQuery.inArray(date, dates);
    if (index >= 0) {
        removeDate(index);

    } else {
        addDate(date);

     }

}*/
// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
    var ret = new String(number);
    if (ret.length == 1)
        ret = "0" + ret;
    return ret;
}
//jQuery(function () {
$(document).on('click', '#completeRegisterBtn', function(){
	$('#completeRegisterForm').submit();
});
//});

$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>
</body>
</html>
