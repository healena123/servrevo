<?php
session_start();
$err = isset($_GET['err'])?$_GET['err']:'';

include_once ('config.php');

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">


<?php include('main-header.php'); ?>

<div class="container-fluid user-page-content">
    <div class ="">



 				<h1>Welcome Back</h1>
 				<div class="user-login-panel">
        			<form action="process_login.php?strt=<?php echo $_GET['strt']; ?>&end=<?php echo $_GET['end']; ?>&check=<?php echo $_GET['check']; ?>" method="post" autocomplete="off">


            	<?php if($err != ''){ ?><div style="color:red;margin-bottom:20px">Invalid user credentials.</div><?php } ?>
            	<div class="form-group mb20">
    				<label for="username" class="lbl-sm">Email</label>
    				<input type="text" class="form-control" name="email" id="name" size="50">
  				</div>

                <div class="form-group mb20" style="position:relative">
                    <label for="emailAddressFld" class="lbl-sm">Password</label>

                    <input type="password" class="form-control" name="password" id="user-password" size="50">
                    <input type="hidden" name="ipaddress" id="ipaddress" >
                </div>

                <div style="height:10px;"></div>
                <div class="form-group text-left">
                	<button type="submit" class="btn btn-block btn-serve-start" id="registerBtn" style="font-size:75%;">LOGIN</button>
                </div>
                <div style="text-align:center;margin:10px 0;">
                	<small style="font-size:12px;">
  						<a href="forgot.php" class="forgot-pass def-link" style="color:rgba(91,94,108,1)">Forgot Password</a>
  					</small>
                </div>
               <div class="col-sm-12" style="font-size:15px;">Or, create account? <a href="<?php echo BASE_URI; ?>register.php">Sign Up here.</a></div>

        			</form>
        		</div>


    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>
$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>
</body>
</html>
