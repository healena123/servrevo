<?php
$err = isset($_GET['err'])?$_GET['err']:'';
$user_id = isset($_GET['user'])?$_GET['user']:'';
$code = isset($_GET['code'])?$_GET['code']:'';


?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">

<?php include('main-header.php'); ?>

<div class="container-fluid user-page-content">
    <div class ="row">
    	<h1>Change your password</h1>
 			
 		<div class="user-login-panel">
        	<form id="resetPasswordForm" action="<?php echo BASE_URI; ?>process_reset_password.php" method="post" autocomplete="off">
        		<input type="hidden" name="action" value="1">
        		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        		<input type="hidden" name="token" value="<?php echo $code; ?>">
        		<div class="err-msg"></div>
            			<?php if($err != ''){ ?><div style="color:red;margin-bottom:20px">The password reset code is no longer valid.</div><?php } ?>
            	<div class="form-group mb20">
    				<label for="email" class="lbl-sm">Password</label>
    				<input type="password" class="form-control" name="password" id="password" size="50">
  				</div>
				<div class="form-group mb20">
    				<label for="email" class="lbl-sm">Confirm Password</label>
    				<input type="password" class="form-control" name="confirm_password" id="confirm-password" size="50">
  				</div>
               
                <div style="height:10px;"></div>
                <div class="form-group text-left">
                	<button type="button" class="btn btn-block btn-serve-start" id="resetBtn">Reset Password</button>
                </div>
                <div style="text-align:center;margin:10px 0;">
                	<small style="font-size:12px;">
  						Return to <a href="<?php echo BASE_URI; ?>login.php">Login here.</a>
  					</small>
                </div>
               <div class="col-sm-12" style="font-size:10px;">Or, create account? <a href="<?php echo BASE_URI; ?>register.php">Sign Up here.</a></div>
            
        	</form>
   
		</div>
    </div>
</div>

<footer class="footer-m">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-sm-12 col-xs-12">
                <img src="images/footer-logo.png" />
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Phone: +63 917 539 8008</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Email: start@servrevo.com</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h5>Copyright 2016 ServRevo Corp.</h5>
            </div>
        </div>
    </div>
</footer>

<footer class="footer">
    <div class = "container">
        <div class = "footer-nav">
            <a href="#"><img class="logo" src="images/footer-logo.png"></a>
            <p>Copyright 2016 ServRevo Corp.</p>
        </div>
        <div class = "footer-nav-items">
            <ul>
                <li><a>Phone: +63 917 539 8008</a></li>
                <li><a>Email: start@servrevo.com</a></li>
                <li><small>10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</small></li>
            </ul>
        </div>
    </div>
</footer>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>

<script>
$(document).on('click', '#resetBtn', function(){
	var err = false;
	var err_msg="";
	var pass = $('#password').val();
	
	if($('#password').val() == ''){
		$('#password').parent().addClass('has-error');
		err = true;
		err_msg += ' Password is required';
	}else if( pass.length < 6){
		$('#password').parent().addClass('has-error');
		err = true;
		err_msg += ' Password not less than 6 characters';
	}else if($('#password').val() != $('#confirm-password').val()){
		$('#password').parent().addClass('has-error');
		$('#confirm-password').parent().addClass('has-error');
		err_msg += ' Password and Confirm Password does not match.';
		err = true;
	}else{
		$('#password').parent().removeClass('has-error');
		$('#confirm-password').parent().removeClass('has-error');
		err = false;
		
	}
	
	if(err==true){
		$('.err-msg').html(err_msg);
	}else {
		$('.err-msg').empty();
		$('#resetPasswordForm').submit();
	}
});
</script>
</body>
</html>