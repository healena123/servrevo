<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
} else {
	if($_SESSION['access_id'] == '4'){
		header("Location: ".BASE_URI);
	}
}



error_reporting(0);

$user_id = $_GET['user'];
date_default_timezone_set('Asia/Manila');


$latest_users = "select * from Users ORDER BY date_created DESC limit 3";

$inquiries = "select * from Inquiries ORDER BY date_created DESC limit 3";

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
	<link href='css/fullcalendar.css' rel='stylesheet' />
	<link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />


		<style>



  #calendar {
    max-width: 900px;
    margin: 20px auto;

  }
  .fc-view-container {
  	background:#fff
  }
	.fc-header-toolbar h2 {
		font-size:16px;
		margin-top:10px
	}
	.fc-content {
		font-size:10px;
	}
	.fc-widget-header span {
		font-size:12px;
	}
</style>
</head>
<body class="serve-revo-admin">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<?php include('dashboard_header.php'); ?>

<div class="content-wrap">
    <div class="left">
    	<div class="accordion-menu">
    		<div class="col">
  				<div class="acc-menu-link">
  					<a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
  						<i class="fa fa-folder"></i> Dashboard
  					</a>
    			</div>
    		</div>

  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Client Bookings
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Users
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Archive
  					</a>
    			</div>

  			</div>
			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Reports
  					</a>
    			</div>

  			</div>
		</div>
    </div>

    <!-- Right Content -->
    <div class="right">
    	<div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
    			<h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Dashboard</h3>
    			<div class="breadcrumbs" style="padding:0;margin-top:3px">
    			<a href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>

    			</div>
    		</div>
    <div class="row">
    	<div class="col-sm-12">

    		<div class="col-sm-8">
    		<div style="background:#fff;">

    		<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">Latest Bookings</h6>
    		<div style="padding:10px 20px;">
    		<div style="font-size:12px;">
    			<span style="margin-right:10px;"><i style="width:10px;height:10px;display:inline-block;background:#81A594"></i> Reserved</span>
    			<span style="margin-right:10px;"><i style="width:10px;height:10px;display:inline-block;background:#FF7A5A"></i> Booked</span>
    			<span style="margin-right:10px;"><i style="width:10px;height:10px;display:inline-block;background:#0099CC"></i> Paid Booking</span>
    		</div>
    		<div id='calendar'></div>
    		</div>
    	</div>
    	</div>
    		<div class="col-sm-4">
    			<div style="background:#fff;margin-bottom:20px">
    				<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">New Registered Users</h6>
    				<ul style="padding:20px">
    					<?php
    					 if($result = mysqli_query($connect, $latest_users)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		echo '<li style="margin-bottom:10px;"><img src="images/avatar.png" style="width:40px;height:40px;float:left;display:inline-block;margin-right:10px;">';
    		echo '<div style="line-height:16px;">'.$row['firstname'].' ' .$row['lastname'].'</div>';
    		echo '<span style="font-size:12px;color:#999"> '.$row['email'].'| '.$row['date_created'].'</span></span></li>';
    	}
    }
}
    					?>
    					<li><a href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" style="text-decoration:underline;font-size:12px;color:#999">View all</a></li>
    				</ul>

    			</div>
    			<div style="background:#fff;">
    				<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">Recent Inquiries</h6>
    				<ul style="padding:20px;font-size:12px;">
    					<?php
    					 if($result = mysqli_query($connect, $inquiries)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		echo '<li style="margin-bottom:20px;">';
    		echo '<div style="line-height:16px;">'.$row['inquire_name'].'<span style="font-size:10px;color:#999;display:block;margin-bottom:2px"> '.$row['inquire_email'].'| '.$row['date_created'].'</span></div>';
    		echo '<div style="font-size:12px;">'.$row['inquire_msg'].'</div></li>';
    	}
    	?>
    	<li style="display:inline-block;width:100%">
    						<a href="<?php echo BASE_URI; ?>inquiries.php?user=<?php echo $user_id; ?>" style="text-decoration:underline;font-size:12px;color:#999">
    							View all
    						</a>
    					</li>
    	<?php
    } else { ?>
    	<li style="display:inline-block;width:100%;font-size:12px;">
    							No inquiry found.

    					</li>
   <?php }
}
    					?>

    				</ul>

    			</div>
    		</div>
    	</div>
    </div>
    </div>
</div>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>



<script src='js/lib/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>

<script>
var base_url = $('.baseurl').val();

// step 2: convert data structure to JSON


  $(document).ready(function() {
	var d = new Date();
	var m = ((d.getMonth()+1) < 10) ? '0'+(d.getMonth()+1) : d.getMonth();
	var day = ((d.getDate()+1) < 10) ? '0'+(d.getDate()+1) : d.getDate();
	var today = d.getFullYear() + '-' + m +'-' + day;

    $('#calendar').fullCalendar({
      defaultDate: today,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: base_url + 'php/get-events.php',
        error: function() {
          $('#script-warning').show();
        }
      },
      loading: function(bool) {
        $('#loading').toggle(bool);
      }
    });

  });

</script>

</body>
</html>
