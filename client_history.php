<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
} else {
	if($_SESSION['access_id'] == '4'){
		header("Location: ".BASE_URI);
	}
}



error_reporting(0);

$user_id = $_GET['user'];
$option = $_GET['option'];

date_default_timezone_set('Asia/Manila');

$seat_ref = "Select * from Seats_ref";

$reserved_seats = "
Select sr.id as reservation_id, sr.booking_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
";

// where b.is_cancelled='1' or b.is_deleted != '1' or b.status = '0' -- to -- where b.is_deleted != '1'
$bookings = "select b.*, u.name, u.company, u.email, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id
where b.is_deleted != '1'
order by b.date_created desc";

//var_dump($bookings);die;
$latest = "select b.*, u.name, u.email, u.date_created as date_registered, max(b.date_created) as latest_date
from Booking b
left join Users u on u.id=b.user_id where is_deleted != '1'";



?>

<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>

    <link rel="stylesheet" href="css/dataTables.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/datatable.responsive.css">
    <link rel="stylesheet" href="css/fixedColumns.dataTables.min.css">


    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
	<link href='css/fullcalendar.min.css' rel='stylesheet' />
	<link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />


<style>
  #calendar {
    max-width: 900px;
    margin: 40px auto;

  }
  .fc-view-container {
  	background:#fff
  }
	.fc-header-toolbar h2 {
		font-size:26px;
	}
	.fc-content {
		font-size:12px;
	}
	.dataTables_info {font-size:10px}
	.pagination-mds * {font-size:10px}
	.search-sm {position:relative;}
	.search-icon {position: absolute;
    top: 3px;
    left: 5px;
    z-index: 2;
    color: #999;}
	.search-sm input {z-index:0}
	.dataTables_wrapper .dataTables_filter input {margin-left:0 !important;min-width:230px;padding-left:30px}
	table.dataTable tbody td * {font-size:12px;}
	.dt-buttons {visibility:hidden}
</style>
</head>
<body class="serve-revo-admin">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<input type="hidden" value="<?php echo $user_id; ?>" class="user-id">
<?php include('dashboard_header.php'); ?>

<div class="content-wrap">
    <div class="left">
    	<div class="accordion-menu">
    		<div class="col">
  				<div class="acc-menu-link">
  					<a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
  						<i class="fa fa-folder"></i> Dashboard
  					</a>
    			</div>
    		</div>

  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Client Bookings
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Users
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Archive
  					</a>
    			</div>

  			</div>
			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Reports
  					</a>
    			</div>
  			</div>
		</div>
    </div>

    <!-- Right Content -->
    <div class="right">
    	<div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
    			<h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Bookings</h3>
    			<div class="breadcrumbs" style="padding:0;margin-top:3px">
    			<a href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>

    			</div>
    		</div>

    	<div class="">
    		<div class="col-sm-12">
    			<div style="background:#fff;">
    				<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">Booking List</h6>

    				<div style="padding:20px;position:relative">
    				<div class="booking-filters">
    					<div class="tab-actions">
                    		<button class="btn btn-default print_pdf btn-sm ml10"><i class="fa fa-print"></i>&nbsp; Print PDF</button>
                    		<button class="btn btn-default csv btn-sm ml10"><i class="fa fa-download"></i>&nbsp; CSV</button>
                    	</div>
    				</div>
    				<table id="bookingsDatatable" class="table" style="width:100%;font-size:12px;">
    					<thead>
    						<tr>
    							<th style="width:20px;padding-right:0"></th>
    							<th>Details</th>
    							<th>Name/Company</th>
    							<th>No. of Seats</th>
    							<th>No. of Days</th>
    							<th>Date Scheduled</th>
    							<th>Amount</th>
    							<th>Status</th>
								<th></th>
    						</tr>
    					</thead>
<?php
   if($result = mysqli_query($connect, $bookings)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){ ?>
    		<tr data-booking-id="<?php echo $row['id']; ?>">
    			<td style="width:20px;padding-right:0;"><a class="trash-tr fa fa-trash"></a></td>
    			<td style="margin-bottom:10px;">
    				<strong>Booking ID. <?php echo $row['booking_id']; ?></strong><br />
    				<span style="color:#999;font-size:10px">Booking Date: <?php echo date("d M Y | H:s A", strtotime($row['date_created'])); ?></span>
    			</td>
    			<td><?php echo $row['name']; ?><br /><?php echo $row['company']; ?></td>
    			<td><?php echo $row['total_seat_reserved']; ?></td>
    			<td><?php echo $row['total_days_reserved']; ?></td>
    			<td><?php echo date("M d", strtotime($row['date_from'])).'-'.date("M d", strtotime($row['date_to'])); ?></td>
    			<td style="text-align:right;font-weight:bold;">
    				P<?php echo $row['total_reservation_amt']; ?><br />
				</td>
				<td>
    				<?php
    											if($row['is_paid'] == 0 && $row['status'] == 1){
    												echo '<span style="color:#000;">Unpaid</span><br />';
    											}else if($row['is_paid'] == 1 && $row['status'] == 1){
    												echo '<strong><span style="color:#33691e;">Paid</span></strong><br />';
    											}else if($row['is_paid'] == 0 && $row['status'] == 0){
    												echo '<span style="color:#FF0000;">Cancelled</span><br />';
    											}
    										?>
    			</td>
    			<td>
    				<?php if($row['is_deleted'] == '1'){ echo '<strong style="color:red">Deleted</strong>';} ?>
    				<?php if($row['is_cancelled'] == '1'){ echo '<strong style="color:red">Cancelled</strong>';} ?>
    			</td>
    		<?php
    	}
   }
}
    					?>
    				</table></div>

    			</div>
    		</div>
    	</div>
    </div>
</div>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/dataTables.bootstrap.js"></script>
<script src="js/dataTables.buttons.min.js"></script>
<script src="js/datatable.responsive.js"></script>
<script src="js/dataTables.fixedColumns.min.js"></script>
<script src="js/buttons.print.min.js"></script>
<script src="js/buttons.flash.min.js"></script>
<script src="js/buttons.html5.min.js"></script>

<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>



<script src='js/lib/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>

<script>
var base_url = $('.baseurl').val();

// step 2: convert data structure to JSON
$(document).on('click', '.print_pdf', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	}, 200);
});
$(document).on('click', '.csv', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	}, 200);
});

$(document).on('click', '.trash-tr', function(e){
  if(!confirm('Are you sure you want to delete this item?')) {
    e.preventDefault();
    return false;
  }

	var count = $(this).closest('table').find('tbody tr.tr-item').length;

	if($(this).closest('table').find('tbody tr.tr-item').length === 1){
		return;
	}

	else {
		$(this).parent().closest('tr').animate( {backgroundColor:'#FAFEFF'}, 200).fadeOut(200,function() {
			var booking_id = $(this).attr('data-booking-id');
			var obj = {
				'item_id' : booking_id,
				'table' : 'booking'
			}

			$.post(base_url+"process_delete.php", obj, function (data) {
				$(this).remove();
			});
		});
	}
});//delete table row

  $(document).ready(function() {
		$('#bookingsDatatable').DataTable({
				"paging":   true,
		        "info":     true,
		        "bLengthChange": false,
		        "bDestroy": true,
		        "order": [[ 1, "desc" ]],
		        responsive: true,
		        "dom": '<"pull-left"B><"pull-right"lfr>tip',
		        buttons: [
            		'copy', 'csv', 'excel', 'pdf', 'print'
        		]
		  	});
  });

function updateTable(obj){
	console.log(obj);
	//----DISPLAY new Branch Sales
	$.post(base_url + 'getBookings.php', obj, function (data) {
	    data = $.parseJSON(data);

	    var datarows = [];

	    data.forEach(function(entry){
	    	var datacol = [];
	        var details = '<strong>Booking ID. '+entry.booking_id +'</strong><br />'+
    				'<span style="color:#999;font-size:10px">Booking Date: '+ entry.date_created +'</span>';
    		var company = (entry.company != null) ? entry.company:'';
    		var name = entry.name + '<br />' + company;

    		var stat = '';
    											if(entry.is_paid == 0 && entry.status == 1){
    												stat = '<span style="color:#000;text-align:center";">Unpaid</span><br />';
    											}else if(entry.is_paid == 1 && entry.status == 1){
    												stat = '<strong><span style="color:#33691e;text-align:center";">Paid</label></strong><br />';
    											}else if(entry.is_paid == 0 && entry.status == 0){
    												stat = '<span style="color:#FF0000;text-align:center";">Cancelled</span><br />';
    											}
    		var amt = '<div style="text-align:right"><strong>P'+entry.total_reservation_amt+'</strong><br />'+stat+'</div>';


    			datacol.push('<a class="fa fa-trash trash-tr"></a>');
       		datacol.push(details);
       		datacol.push(name);
       		datacol.push(entry.total_seat_reserved);
       		datacol.push(entry.total_days_reserved);
       		datacol.push(entry.date_from +' - '+entry.date_to);
       		datacol.push(amt);
       		datacol.push('<a class="btn btn-default">Cancel</a><a class="btn btn-default">Pay</a><a class="btn btn-default">Book</a>');

       		datarows.push(datacol);
	    });

	    $('#bookingsDatatable').DataTable({
			data: datarows,
            "paging":   true,
		        "info":     true,
		        "bLengthChange": false,
		        "bDestroy": true,
		        "order": [[ 1, "desc" ]],
		        responsive: true,
		        "dom": '<"pull-left"B><"pull-right"lfr>tip',
		        buttons: [
            		'copy', 'csv', 'excel', 'pdf', 'print'
        		]
	  	});
	});
}
</script>
<script>
var base_url = $('.baseurl').val();
$(document).on('click', '#filterByStatus a', function(){
	var opt = $(this).attr('data-opt');
	var user_id = $('.user-id').val();
	var obj = {
		'option' : opt,
		'user_id' : user_id
	}
	updateTable(obj);
});
/*$(document).ready(function(){

});
$(document).ready(function(){
	var options = {
    	valueNames: [ 'client-name','booking-id', 'booking-date'],
    	page: 10,
  		pagination: true
	};

	var listObj = new List('client-list', options);

	$('.search').on('keyup', function() {
  		var searchString = $(this).val();
  		listObj.search(searchString);
  		var count = $('#client-list .list li').length;
  		if(count == 0){
  			$('#client-list .list').html('<li style="padding-top:20px">No record found.</li>');
  		}
  	});
  	var itemsInList = [
{ filter1: "Paid" }
, { filter1: "Unpaid" }
, { filter1: "Cancelled" }
];
  	listObj.filter(function(item) {
		if (item.values().label > 1) {
   			return true;
		} else {
   			return false;
		}
	}); // Only items with id > 1 are shown in list
	listObj.filter();
});*/
$(document).on('click', '.book-reservation-btn', function(){
	var user_id = $('.user-id').val();
	var action = $(this).attr('data-action');
	var booking_id = $(this).closest('.booking-entry').find('input[name="booking_id"]').val();

	var url = base_url+'process_booking.php?booking_id='+booking_id+'&user_id='+user_id+'&action='+action;

	$.post(url, '', function (data) {
		window.location.reload();
	})
});
$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>
</body>
</html>
