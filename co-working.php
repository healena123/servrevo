<?php
session_start();
include_once ('config.php');

if(isset($_GET['check'])){
	$start = $_GET['strt'];
	$end = $_GET['end'];
}
else{
	$start = '';
	$end = '';
}
$user_id = isset($_GET['user']) ? $_GET['user'] : '';

if($user_id){
	$user_info = "Select * from Users where id='$user_id'";

	$seat_ref = "Select * from Seats_ref";

	$reserved_seats = "
	Select sr.id as reservation_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
";



 if($result = mysqli_query($connect, $user_info)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		$username = $row['username'];
    		$password = $row['password'];
    		$email = $row['email'];
    	}
    }
}

}

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Coworking Space Ortigas Center, Seat Leasing | ServRevo Philippines</title>
    <meta name="description" content="Coworking office for modern remote workers. We provide a collaborative working environment for your offshore team. Perfect workplace for startups, freelancers, and entrepreneurs."/>
    
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/offshore-services.css"/>
		<link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/seat-leasing.css"/>
	 	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
	  <link rel="stylesheet" type="text/css" href="css/admin.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<style>


.modal-open {overflow-y: auto}
body { padding-right: 0 !important }
hr {
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		

<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114355569-1"></script>
		<script>
	  		window.dataLayer = window.dataLayer || [];
	  		function gtag(){dataLayer.push(arguments);}
	  			gtag('js', new Date());

	  			gtag('config', 'UA-114355569-1');
		</script>
			<script type="application/ld+json">
			{       
			  "@context": "http://schema.org",      
			  "@type": "Service",     
			  "ServiceType": "Co-working Space",
			  "url": "http://www.servrevo.com/co-working.php",
			  "description": "Need a place where you can set-up and work right away with your team? Our newly renovated, modern 
			workspace is open for lease and ready for every need - from office rooms to hot desks and workstations.",
			"aggregateRating": {        
			   "@type":"AggregateRating",     
			   "ratingValue": " 5 ",  
			   "ratingCount": " 5  "  
							}  
			 }     
			</script>
		
</head>
<body id="serve-revo-main" class="serve-revo-offshore">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">

<?php include('main-header.php'); ?>



<div class = "container-fluid">
    <div class = "row row1-body carousel-row">
        <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="main-carousel-container">
                <div id="mainCarousel" class="carousel slide" data-ride="carousel" data-interval="false"><!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#mainCarousel" data-slide-to="0" class=""></li>
                        <li data-target="#mainCarousel" data-slide-to="1" class=""></li>
                        <li data-target="#mainCarousel" data-slide-to="2" class=""></li>
                        <li data-target="#mainCarousel" data-slide-to="3" class=""></li>
                    </ol>
                    <div class = "serve-start">
											<?php if($_SESSION['user_id']){ ?>
												<a href="<?php BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id']; ?>" class="btn btn-serve-start" role="button">LET'S START</a>
											<?php }else{ ?>
                        <a href="<?php BASE_URI; ?>reservation.php" class="btn btn-serve-start" role="button">LET'S START</a>
											<?php } ?>
                    </div>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <p style="text-shadow: 1px 0px 1px #000;color:#fff">Source the right people<br>
                                Set-up shop in our space.<br>
                                start working with your team.<br>
                            </p>
                        </div>
                        <div class="item">
                            <p style="text-shadow: 1px 0px 1px #000;color:#fff">Fresh and modern workspaces for<br>
                                lease in a premier business district<br>
                            </p>
                        </div>
                        <div class="item">
                            <p style="text-shadow: 1px 0px 1px #000;color:#fff">Focus on growing your business<br>
                                with our managed services.<br>
                            </p>
                        </div>
                        <div class="item">
                            <p style="text-shadow: 1px 0px 1px #000;color:#fff">Boost your business - <br>
                                work with the right people now.<br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class = "container container2">
    <div class = "row row2-body">
        <div class ="row2-body-custom">
            <div class = "col-sm-12 col-xs-12">
                <h3>Ready. Set. Work!</h3>
                <p>Need a place where you can set-up and work right away with your team? Our newly renovated, modern <br>
                    workspace is open for lease and ready for every need - from office rooms to hot desks and workstations.</p>
            </div>
        </div>
    </div>
    <div class = "row row2-body box">
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon1.png" alt="location-icon" />
                 <p>Located in the Ortigas <br> Central Business District</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon2.png" alt="fast-connection-icon"/>
                <p>Fast, reliable wired and <br> wireless connections</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon3.png" alt="reception-icon" />
                <p>Reception and <br> Common Area</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon4.png" alt="laptop-icon" />
                <p>Laptop & Headset <br> (Optional)</p>
            </div>
        </div>
    </div>
    <div class = "row row2-body box">
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon5.png" alt="conference-icon" />
                <p>Conference Room</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon6.png" alt="lockers-icon"/>
                <p>Lockers</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon7.png" alt="pantry-icon" />
                <p>Pantry</p>
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class = "row2-icon-box">
                <img src="images/seat-leasing/row2-icon8.png" alt="coffee-icon" />
                <p>Free coffee</p>
            </div>
        </div>
    </div>
    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">

        <div class = "serve-start">
					<?php if($_SESSION['user_id']){ ?>
						<a href="<?php BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id']; ?>" class="btn btn-serve-start contactus-now" role="button">BOOK A SEAT</a>
					<?php }else{ ?>
            <a href="<?php BASE_URI; ?>reservation.php" class="btn btn-serve-start contactus-now" role="button">BOOK A SEAT</a>
					<?php } ?>
        </div>

    </div>
</div>

<div id="container" class="container-fluid">
	<div class="row" style="background-image: url(images/co-working/confe.jpg); height:35em; background-position: center; background-repeat: no-repeat; background-size: cover;">
    <h1 style="text-shadow: 1px 0px 1px #000;font-size:2.5em;color:#fff; margin-bottom:2em;margin-top:1em;">Book a Conference Room</h1>
		<div style="background-color:white;padding:1em;width:35em;opacity: 0.9;">
			<div style="margin-left: 5em;">
				<h4 style="color:#33691e;font-size:1.5em;"><strong>&#8369; 4,000</strong> / per day</h4>
				<strong>&#9679; &nbsp;8-seat conference table<br/>
				<span>&#9679; &nbsp;Fiber internet connection</span><br/>
				<span>&#9679; &nbsp;Air-Conditioning</span><br/>
				<span>&#9679; &nbsp;43-inch smart TV</span><br/>
				<span>&#9679; &nbsp;Whiteboard</span><br/>
				<span>&#9679; &nbsp;Coffee and water consumption</span><br/></strong>
				<span>&nbsp;&nbsp;&nbsp;&nbsp;*max of 10 seats</span>
				<div>
					<a id="show-modal" class="btn btn-serve-start" style="margin-top:1em;" data-toggle="modal" data-target="#modal-conference">BOOK NOW!</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal for Book Conference Room -->
	<div id="modal-conference" class="modal fade">
		<div class="modal-dialog" style="width:20em;">
			<div class="modal-content">
				<form id="processReservation" action="<?php echo BASE_URI; ?>process_reserve.php" method="post" autocomplete="off">
				<div class="modal-header">
						<h3 class="modal-title">Reserve A Conference Room</h3>
	      </div>
				<div class="modal-body">
					<h4 class="text-center" style="margin-top:1em;margin-bottom:1em;">Select Dates</h4>
					<div class="text-center err-msg"></div>
					<input type="hidden" name="reservation_dates" id="reservation-dates">
					<input type="hidden" name="reservation_seats" id="reservation-seats">
					<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
					<input type="hidden" name="total_days_reserved" id="total-days">
					<input type="hidden" name="total_seats_reserved" id="total-seats">
					<input type="hidden" name="total_reservation_amt" class="total-amt-hdn" value="0">
					<input type="hidden" name="BookingIdInfoReserve" id="BookingIdInfoPass">
					<input type="hidden" name="Check" value="1"/>

					<div class="row">
						<div class="col-sm" style="display:inline-block;text-align:right;">
							<label class="col-xs-4" style="margin-left:1px;">Start</label>
							<span class="col-xs-7" style="padding:0;">
								<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
								<input type="text" value="<?php echo $start; ?>" placeholder="Start Date" style="padding-left:30px;" name="date_from" onkeyup="getnewdate()" class="form-control required datepicker-input" id="datepickerInput1" size="50">
							</span>
						</div>
						<div class="col-sm" style="display:inline-block;text-align:right;margin-top:1em;">
							<label class="col-xs-4" style="margin-left:2px;">End</label>
							<span class="col-xs-7" style="padding:0;">
								<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
								<input type="text" value="<?php echo $end; ?>" placeholder="End Date" style="padding-left:30px;" name="date_to" class="form-control datepicker-input" id="datepickerInput2" size="50">
							</span>
						</div>

						<div class="row" style="display:inline-block;text-align:left;margin-left:1em;margin-top:1em;">
							<center>
								<label class="col-6" style="margin-right:1em;">Days</label>
								<span class="col" style="padding:0;">
									<input type="text" style="display:inline-block;width:30%;" class="form-control required disabled" value="0" placeholder="0" name="total_days_reserved" id="total-seats-reserve" size="50" readonly/><p style="display:inline-block; margin-left:1em;">x P<strong>4000</strong></p>
								</span>
							</center>
						</div>
					</div>
				</div>
					<div style="text-align:center;">
					<strong style="color:#000;font-weight:700;font-size:1em;margin-top:5px;line-height:15px;">P</strong>
  					<strong class="seat-total-amt" style="color:#000;font-weight:700;font-size:1em;margin-top:5px;line-height:15px;">0</strong>
					</div>
				<div class="modal-footer" style="margin-bottom:1em;">
					<button type="button" id="reserveBtn" class="btn btn-serve-callback" style="!important;width:100%;">Reserve</button>
					<p style="text-align:center;font-size:12px;margin-top:1em;">*max of 10 seats</p>
				</div>
			</form>
		</div>
	</div>
</div>


<div class = "container-fluid container3">
    <div class = "row row3-body">
        <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row3-body-custom">
                <h3>Team up with </h3>
                <img src="images/servrevo-logo-m.png" alt="servrevo-logo">
                <h4>Providing solutions that let you leap over competition.</h4>
                <p>ServRevo is a business process solutions company providing shared and staff-leasing services to companies looking for Manila-based staff. </p>
                <p>Our flexible offshore solutions enable clients to keep up with changing customer
                    demands and competitive global markets by supporting industry, process and
                    segment specific needs. Our best-in-class operations can help you achieve your
                    business goals and gain the competitive edge needed to grow by leaps and bounds.
                </p>
            </div>
        </div>
    </div>
</div>
<div class = "container-fluid">
    <div class = "row row6-body">
        <div class = "col-lg-7 col-md-7">

        </div>
        <div class = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div id="call-back" class = "call-back">
                <h3>CREATE YOUR OFFSHORE <br class="h1-wide"> DREAM TEAM</h3>
                <hr>
                <div class = "row">
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" type="text" id="name" placeholder="&#xf2c0; Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" type="text" id="email" placeholder="&#xf003; Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" type="text" id="phone" placeholder="&#xf10b; Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <textarea rows="3" class="form-control" id="message" style="resize: none;" placeholder="&#xf1d9; Tell us what you need"></textarea>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <a href="#" class="btn btn-serve-callback" role="button">LET'S START</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

</body>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/co-working.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/jquery-ui.min.js"></script>

<script type="text/javascript">
var base_url = $('.baseurl').val();

function init(){
	var currentDate = new Date();

	var m = currentDate.getMonth()+1;
	var d = currentDate.getDate();
	m = m<10?'0'+m:m;
	d = d<10?'0'+d:d;
	var date_from = '';
	var date_to = '';
	date_from = currentDate.getFullYear() +'-'+m+'-'+d;
	date_to = currentDate.getFullYear() +'-'+m+'-'+d;
	var check = "<?php echo $_GET['check']; ?>";

	if(check != ''){
		date_from = "<?php echo $start; ?>";
		date_to = "<?php echo $end; ?>";
	}
		var w = date_from + ','+date_to;

		var BkngId= Math.floor((Math.random()*90000)+10000);
		document.getElementById("BookingIdInfoPass").value = BkngId;
		getDays();
		// getAvailableSeats(w);
			if(check != null){
			computeTotalAmount();
		}
}
$(document).ready(function(){
	seatPlacement('');
	init();

    if(window.location.href.indexOf('#modal') != -1) {
        $('#show-modal').trigger('click');
    }

})
$(document).on('focus', '.datepicker-input', function(){

	    var dateToday = new Date();
	    var dates = $("#datepickerInput1, #datepickerInput2").datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: dateToday,
				beforeShowDay: noWeekendsOrHolidays,
	      onSelect: function(selectedDate) {
	          var option = this.id == "datepickerInput1" ? "minDate" : "maxDate",
	          instance = $(this).data("datepicker"),
	          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	          dates.not(this).datepicker("option", option, date);
									console.log(selectedDate);
									getDays();
									// getAvailableSeats('');
									// updateReservationDetails();
									computeTotalAmount();
	      }
	    });
	// var dateToday = new Date();
	// $(this).datepicker({
	// 	dateFormat: 'yy-mm-dd',
	// 	minDate: dateToday,
	// 	beforeShowDay: noWeekendsOrHolidays,
	// 	onSelect: function(dateStr) {
	// 			console.log(dateStr);
	// 			getAvailableSeats('');
	// 			updateReservationDetails();
	// 			computeTotalAmount();
	// 		},
	// 		onClose: function(selectedDate) {
	//         	$('.content-loader').removeClass('hidden');
	//         	if(selectedDate == '') $('.content-loader').addClass('hidden');
	//       	}
	// 	});

})
function getDays() {
	var date_from = new Date($('#datepickerInput1').val());
	var date_to = new Date($('#datepickerInput2').val());
	var w = calculateDays(date_from, date_to);
	if(w>=0){
		document.getElementById("total-seats-reserve").value = w;
		document.getElementById("total-days").value = w;
	}
}
function calculateDays(first, last) {
  var aDay = 24 * 60 * 60 * 1000,
  daysDiff = ((Math.abs(first-last))/aDay)+1;
  if (daysDiff>0) {
    for (var i = Math.abs(first), lst = Math.abs(last); i <= lst; i += aDay) {
      var d = new Date(i);
      if (d.getDay() == 6 || d.getDay() == 0) {
          daysDiff--;
      }
    }
  }
  return daysDiff;
}
function getAvailableSeats(w){
	if(w==''){
	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	w = date_from + ','+date_to;
}
	// var url = base_url+'reserved_seats.php?dates='+w;

	$.post(url, '', function (data) {
		//clear Seats selection
		$('#total-seats').val(0);
		$('#total-seats-reserve').val(0);

		arr=[];
		if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
				$('.content-loader').addClass('hidden');
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});
			$('.content-loader').addClass('hidden');
		}

		});
}
$(document).on('click', '#checkAvailabilityBtn', function(){

	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	var w = date_from + ','+date_to;

	if(date_from == ''){
		$('.err-msg').append('Please select dates.');
	} else {
		$(this).html('<img src="'+base_url+'images/ajax-loader.gif">');
		$('.seat-placement-holder').css({'opacity':'0.5'});

		$('.err-msg').empty();
		var url = base_url + 'reserved_seats.php?dates='+w;

		$.post(url, '', function (data) {
			if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});

		}

		});
	}
});
var arr = [];
$(document).on('change', '#seatPlacement .chk', function(){

	var el = $(this).val();

	$(this).parent().find('input, label').addClass('selected');

	arr.push(el);


	if (!$(this).is(":checked")) {
		for(var i = arr.length; i--;) {
          if(arr[i] === el) {
              arr.splice(i, 1);
          }
        }
	}

	$('.no-of-seats').html(arr.length);
	$('#total-seats-reserve').val(arr.length);
	$('#total-seats').val(arr.length);



	var p = "";
	var total = 0;
	$.each(arr, function(i,v){
    	p += v+',';

    });
    $('#reservation-seats').val(p);


	if(arr.length > 0) {
		$('.reserve-other-info').removeClass('hidden');
		$('#checkAvailabilityBtn').addClass('hidden');
	} else {
		$('.reserve-other-info').addClass('hidden');
		$('#checkAvailabilityBtn').removeClass('hidden');
	}
	updateReservationDetails();
	computeTotalAmount();
});
$(document).on('click', '#reserveBtn', function(e){

		if($('#datepickerInput1').val() == '' || $('#datepickerInput2').val() == ''){
			$('.err-msg').html('Fields cannot be empty.');
			e.preventDefault();
		} else if($('#total-seats-reserve').val() == 0){
			e.preventDefault();
			$('.err-msg').html('Please select seats.');
		}else{
			var userid = "<?php echo $_GET['user'];?>";
			if(userid == ''){
					var strtdt = document.getElementById('datepickerInput1').value;
					var enddt = document.getElementById('datepickerInput2').value;
					var bkid = document.getElementById('BookingIdInfoPass').value;
					var url = base_url+'login.php?strt='+strtdt+'&end='+enddt+'&check=2';
					window.location.href = url;
                    return false;
			}
			else{
				$('.err-msg').empty();
				$('#processReservation').submit();
			}
		}


});
function computeTotalAmount(){
	var d = ($('#total-days').val() != '') ? $('#total-days').val() : 0;
	// var s = ($('#total-seats').val() != '')?$('#total-seats').val() : 0;
	var p = 4000;
	var total = parseInt(d)*parseInt(p);

	$('.total-amt-hdn').val(total);
	$('.seat-total-amt').text(total);
}
function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }
function seatPlacement(data){

	var content = '';
		var s_id = 0;
		for(var i=1;i<=9;i++){
			content += '<div class="entry-tbl tbl-"'+i+'">';


        	for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";
        		var reserved_class="reserved";

        	//content += '<span><input type="checkbox" name="seat_no[]" value="'+seat_id+'" '+check_class+'></span>';
        		content += '<span style="position:relative;">'
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';

        					}
        				}
        				el += ' class="chk reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk reserved"></label>'

				content += '</span>';
        	}
        	content += '</div>';
        	}

        	content += '</div>';

        	if(i%3==0){
        		content += '<div class="col-xs-12" style="display:inline-block;width:100%;float:left:margin:0;"></div>';
        	}
        }
        // LAST TABLE
        content += '<div class="entry-tbl last-tbl-placement tbl-'+i+'">';

        for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";

        		content += '<span style="position:relative;">';
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';
        					}
        				}
        				el += ' class="chk chk1 reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk chk1">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk lbl-chk1 reserved"></label>'

				content += '</span>';

        	}
        	content += '</div>';
        }
        content += '</div>';


        $('#seatPlacement').html(content);
        $('.reservation-legend-holder').removeClass('hidden');
	}
$(document).on('change', '.datepicker-input', function(){
	var el = $(this).val();
	updateReservationDetails();
});
function updateReservationDetails(){
		var start = $("#datepickerInput1").datepicker("getDate"),
   		end = $("#datepickerInput2").datepicker("getDate"),
    	currentDate = new Date(start.getTime()),
    	between = [],
    	count = 0,
    	sd,
		reservation_dates = '',
		start_date = changeDateFormat(start),
		date_range="";
		console.log(end);
		//COUNT NUMBER OF DAYS
		//LIST DATES BETWEEN DATE RANGE
		if(start != null && end != null) {
			while (currentDate <= end) {

				if(currentDate.getDay() < 6 && currentDate.getDay() != 0)
					between.push(new Date(currentDate));
    			currentDate.setDate(currentDate.getDate() + 1);
			}
			count = between.length;
		}
		if(start != null && end == null){
			count = 1;
		}

		//CHANGE FORMAT OF DATES ALL DATES
		//SAVE TO INPUT HIDDEN

		if(start_date != null) {
			reservation_dates = $("#datepickerInput1").val();
		}
		if(start != null && end != null) {
			reservation_dates = "";
			$.each(between, function(i,v){

				sd = new Date(v);
				var m = sd.getMonth() + 1;
				var d = sd.getDate();
				m = m < 10? '0'+m: m;
				//d = d < 10? '0'+d: d;
				var nd = sd.getFullYear()+'-'+m+'-'+d;
				reservation_dates += nd+',';
			});
		}
		//FORMAT DATE RANGE FOR VIEW
		if(start != null && end != null) {
			date_range = changeDateFormat(start) +' - '+ changeDateFormat(end);
		}else {
			if(start != null && end == null)
				date_range = changeDateFormat(start);
			if(start == null && end != null)
				date_range = changeDateFormat(end);
		}
		//DISPLAY INFO
		$('#total-days').val(count);
		$('.date-range-view').text(date_range);
		$('#reservation-dates').attr('value', reservation_dates);
	}
function changeDateFormat(date){

	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

	var d = new Date(date);
	return monthNames[d.getMonth()] + ' '+ d.getDate() + ', ' + d.getFullYear();
}
function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}
var natDays = [
  [1, 1, 'ph'],
  [12, 25, 'ph']
];
function nationalDays(date) {
    for (i = 0; i < natDays.length; i++) {
      if (date.getMonth() == natDays[i][0] - 1
          && date.getDate() == natDays[i][1]) {
        return [false, natDays[i][2] + '_day'];
      }
    }
  return [true, ''];
}
/*$(".selector").datepicker({ beforeShowDay: noWeekendsOrHolidays})

function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}*/
/*$(document).on('click', '.datepicker-input', function(ev){
	ev.preventDefault();
	$('#datepicker-modal').removeData();
	$('#datepicker-modal').val(null).trigger("change");
	var el = $(this).attr('id');

	$('#datepicker-modal').modal('show');
	var dateToday = new Date();

	jQuery("#datepicker").datepicker({
		minDate: dateToday,
    	onSelect: function (dateText, inst) {
    		var dates = addOrRemoveDate(dateText);

			$(this).data('datepicker').inline = true;

			$('#datepicker-modal').modal('hide');
        },
        onClose: function() {
    		$(this).data('datepicker').inline = false;
		},
        beforeShowDay: function (date) {
            var year = date.getFullYear();
            // months and days are inserted into the array in the form, e.g "01/01/2009", but here the format is "1/1/2009"
            var month = padNumber(date.getMonth() + 1);
            var day = padNumber(date.getDate());
            // This depends on the datepicker's date format
            var dateString = month + "/" + day + "/" + year;

            var gotDate = jQuery.inArray(dateString, dates);
            if (gotDate >= 0) {
                // Enable date so it can be deselected. Set style to be highlighted
                return [true, "ui-state-highlight"];
            }
            // Dates not in the array are left enabled, but with no extra style
            return [true, ""];
        }

    });
});
// Maintain array of dates
var dates = new Array();

function addDate(date) {
    if (jQuery.inArray(date, dates) < 0) {
        dates.push(date);
        $('#selected-dates').val(dates);
   	}
   	$('#reservation-dates').val(dates);

   	$('#dateSelected').empty();
   	var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    }else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

   	$.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });

}

function removeDate(index) {
    dates.splice(index, 1);
    $('#selected-dates').val(dates);
    $('#reservation-dates').val(dates);

    $('#dateSelected').empty();

    var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    } else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

    $('#datepicker-input').val(dates.length + ' selected');
    $.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });
}

// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
    var index = jQuery.inArray(date, dates);
    if (index >= 0) {
        removeDate(index);

    } else {
        addDate(date);

     }

}*/
// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
    var ret = new String(number);
    if (ret.length == 1)
        ret = "0" + ret;
    return ret;
}
//jQuery(function () {
$(document).on('click', '#completeRegisterBtn', function(){
	$('#completeRegisterForm').submit();
});
//});

$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});


</script>
</body>
</html>
