<?php
session_id(uniqid());
session_start();
error_reporting(0);

include ('config.php');

$err=0;
$email = $_POST['email'];
$password = $_POST['password'];

// No data supplied ?
if($email == '' || $password =='') {
    $err=1;
    header("Location: ".BASE_URI."login.php?err=$err");
}

$md5 = md5($password);
$query = "Select * from Users where email='$email' and password='$md5' and is_deleted != '1'";

if($result=mysqli_query($connect, $query)) {
    if(mysqli_num_rows($result) > 0) {
        $row=mysqli_fetch_array($result);
        $email=$row['email'];
        $session_key = session_id();
        $login_timestamp = date('Y-m-d H:i:s');

        // Store session data
        $_SESSION['session_key'] = session_id();
        $_SESSION["user_id"] = $row['id'];
        $_SESSION["firstname"] = $row['firstname'];
        $_SESSION["lastname"] = $row['lastname'];
        $_SESSION["email"] = $row['email'];
        $_SESSION["user_type"] = $row['user_type'];
        $_SESSION["access_id"] = $row['access_id'];
        $user_id =$row['id'];

        $session ="INSERT INTO Access_session_logs (user_id, email, login_timestamp, session_key)
                VALUES ('$user_id', '$email', '$login_timestamp', '$session_key')";

        $search_query=mysqli_query($connect, $session);

        // Is there a pre-filled data?
        if(isset($_GET['check'])) {
            if($_GET['check'] == 1){
                $start = $_GET['strt'];
                $end = $_GET['end'];
                $seat = $_GET['stcnt'];
                $check = $_GET['check'];
                //echo 'redireting to reservation.php';
                //die;
                header("Location: ".BASE_URI."reservation.php?user=$user_id&strt=$start&end=$end&check=$check");
                exit;
            } else if($_GET['check'] == 2){
                $start = $_GET['strt'];
                $end = $_GET['end'];
                $seat = $_GET['stcnt'];
                $check = $_GET['check'];
                header("Location: ".BASE_URI."co-working.php?user=$user_id&strt=$start&end=$end&check=$check#modal");
                exit;
            } else { // Invalid argument supplied
                header("Location: ".BASE_URI);
                exit;
            }
        }

        // redirect to the appropriate home page
        switch($row['access_id']) {
            case 1:
            case 2:
                // administrators
                $home = sprintf('%sclient_bookings.php?user=%d', BASE_URI,  $_SESSION['user_id']);
                break;
            case 3:
            case 4:
                $home = sprintf('%sbookings.php?user=%d', BASE_URI, $_SESSION['user_id']);
                break;
            default:
                $home = BASE_URI;
        }

        header('Location: ' . $home);
    } else {
        // No account matches
        $err=1;
        header("Location: ".BASE_URI."login.php?err=$err");
    }
}

