<?php
error_reporting(0);

include ('config.php');

$option = $_POST['option'];
$user_id = $_POST['user_id'];

if($option == 'reserved'){
	$where = " where b.status='1' and b.is_deleted != '1' and is_cancelled='0'";
} else if($option == 'all'){
	$where = " where b.is_deleted != '1' and b.status!='0'";
} else if($option == 'paid'){
	$where = " where b.is_paid='1' and b.is_deleted != '1'";
} else if($option == 'cancelled'){
	$where = " where b.is_cancelled='1' and b.is_deleted != '1'";
} else if($option == 'booked'){
	$where = " where b.is_booked='1' and b.status != '0' and b.is_deleted != '1'";
} else {
	$where = " where b.is_deleted != '1' and b.is_cancelled !='1' and b.booking_type = '0' and b.status!='0'";
}
date_default_timezone_set('Asia/Manila');

$bookings = "select b.*, u.name, u.email, u.company, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id $where and b.user_id=$user_id order by b.date_created desc";

$data_array = array();

if($result = mysqli_query($connect, $bookings)){

    	if(mysqli_num_rows($result) > 0){
    		while($row = mysqli_fetch_array($result)){
					$ddate = date("d M Y | h:i A", strtotime($row['date_created']));
					$datefrom = date("M d", strtotime($row['date_from']));
					$dateto = date("M d", strtotime($row['date_to']));
     			$data_array[] = array(
    				'booking_id' => $row['booking_id'],
    				'user_id' => $row['user_id'],
    				'total_reservation_amt' => $row['total_reservation_amt'],
    				'total_seat_reserved' => $row['total_seat_reserved'],
    				'total_days_reserved' => $row['total_days_reserved'],
    				'date_from' => $datefrom,
    				'date_to' => $dateto,
    				'date_created' => $ddate,
    				'name' => $row['name'],
    				'company' => $row['company'],
    				'date_registered' => $row['date_registered'],
    				'is_paid' => $row['is_paid'],
    				'is_booked' => $row['is_booked'],
    				'is_paid' => $row['is_paid'],
    				'status' => $row['status'],
						'is_cancelled' => $row['is_cancelled']
    			);
			}
    	}
	}


echo json_encode($data_array);
?>
