<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">

<?php include('main-header.php'); ?>

<div class = "container-fluid" style="position:relative;padding:40px 0;">
    <div class="centralize">

    	<div class="row terms-condition-content">
    		<div class="col-sm-12">
    			<h1>TERMS AND CONDITIONS</h1>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>OVERVIEW</h4>
    			<br />
    			<p>
    				ServRevo Corp. (ServRevo) is an outsourcing company providing shared services, co-working spaces, HR/Payroll, and staffing solutions.
    				In consideration for the mutual benefits exchanged by ServRevo and the undersigned (“You”, the “Member”, collectively the “Parties”),
    				the Parties hereby agree, warrant, consent and covenant to the following terms, conditions and representations:
    			</p>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>ACCEPTANCE OF TERMS</h4>
    			<br />
    			<p>
    				The Member agrees to conduct himself or herself according to the policies that ServRevo implements from time to
    				time regarding personal behaviour in the co-working space located at Unit 10B, 10F Strata 2000 Building,
    				Emerald Avenue, Ortigas Center, Pasig 1605, Philippines (the “Co-working Space”).
    				At ServRevo’s sole discretion, your membership at the Co-Working Space may be terminated for behaviour that
    				violates any such policies.
				</p>
				<p>
					The Co-working Space provided to you are subject to the following terms and conditions.
					ServRevo reserves the right to update the terms and conditions at any time without notice to you.
					ServRevo membership application does not create a tenancy and/or residency but a prepaid usage to use the provided
					amenities on a monthly or casual basis.
    			</p>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>MEMBERSHIP DETAILS</h4>
    			<br />
    			<p>
    				Membership entitles for reserved Co-working Space, depending on availability, with options of:
				</p>
				<p>
					a. Daily rate of four hundred Philippine Peso (PhP 400.00)
					b. Monthly rate of five thousand eight hundred Philippine Peso (Php 5,800.00)
				</p>
				<ul class="list-style-circle">
					<li>Fully furnished work station(s)</li>
					<li>Free use of conference room with at least 24-hour reservation</li>
					<li>Water & coffee consumption</li>
					<li>Fiber internet connection</li>
					<li>Air-conditioning and utilities</li>
					<li>Admin support (IT/receptionist/maintenance)</li>
    			</ul>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>PAYMENT TERMS</h4>
    			<br />
    			<ul>
    				<li>(a) Fees are exclusive of any applicable taxes.</li>
  					<li>(b) Mode of payment is via check or cash payment only.</li>
  					<li>(c) Invoice shall be issued upon confirmation of booked seat(s).</li>
    			</ul>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>TERMINATION</h4>
    			<br />
    			<p>
    				You agree not to use the Co-Working Space for any purpose that is unlawful, prohibited, or that could damage,
    				disable or impair the property of ServRevo or of other members, or prevents other members from enjoying the
    				Co-Working Space, or that would damage the reputation or business of ServRevo and the Co-Work Space.
    			</p>
    			<p>
    				You also agree not to use the Co-Working Space in connection with:
    			</p>
    			<ul>
    				<li>(a) Lottery contests, pyramid schemes, chain letters, junk email, spamming or similar behavior;</li>
					<li>(b) Defaming, abusing, harassing, threatening or otherwise violating the legal rights (such as privacy and publicity) of others;</li>
					<li>(c ) Posting, distributing or disseminating inappropriate, profane, defamatory, obscene, indecent, or unlawful material or information;</li>
					<li>
						(d) Uploading, reproducing, using, performing or otherwise making available, images, software or other material or information
						which infringes another’s rights, or is protected by intellectual property laws where you don’t own or
						license such rights; and
					</li>
					<li>
						(e) Uploading or using files that contain viruses, corrupted files, or any other similar software or
						programs that may damage the computers or property of the Co-Working Space or another member.
					</li>
    			</ul>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>CONFIDENTIALITY</h4>
    			<br />
    			<p>
    				In your presence at the Co-Working Space, you may learn of confidential information of the Company or of its members.
    				Such confidential information may include business information, trade secrets, technology, processes,
    				customers and prospects that is intended to be confidential and proprietary. You hereby agree and consent to not
    				disclose information that you obtain that was intended to remain confidential.
    			</p>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>RELEASE OF LIABILITY</h4>
    			<br />
    			<p>
    				You hereby waive and hold ServRevo harmless, its members, officers, directors, shareholders, contractors and
    				employees (the “Releasees”) from any claims, liability, actions, or suits with respect to any damages,
    				injuries or losses you suffer to your person or property, whatsoever, including as a result of negligence
    				or gross negligence on the part of the Releasees, including but not limited to any direct, special,
    				incidental, indirect, punitive, consequential or other damages whatsoever (including, but not limited to,
    				damages for lost profits, loss of confidential or other information, business interruption, personal injury,
    				loss of privacy, failure to meet any duty (including of good faith or of reasonable care), negligence, and
    				any other loss) arising out of or in any way related to the Company’s services or otherwise.
    			</p>
    		</div>
    		<div class="col-sm-12 entry-terms-condition">
    			<h4>OTHER</h4>
    			<br />
    			<p>
    				This agreement may not be assigned without the prior written consent of ServRevo.
    				This agreement shall be governed by the laws of the Philippines.
    				Any dispute shall be brought to the regular courts of Taguig City.
    				In the event that a provision in this agreement is determined to be invalid or unenforceable,
    				the remaining provisions of this agreement shall be unaffected and shall remain in full force and effect.
    			</p>
    		</div>
    		<div class="col-sm-12 entry-terms-condition hrule">
    			<h4>ACCEPTANCE OF TERMS AND CONDITIONS</h4>
    			<br />
    			<p>
    				I hereby acknowledge that I have read and understood the terms and conditions indicated in this Proposal.
    				I shall comply with the ServRevo policies, rules and regulations, as well as those which may hereafter be issued,
    				including but not limited to those governing order and discipline, honesty, safety and security,
    				work assignments and standard operating procedures, use of Company properties and access to matters of
    				confidentiality, and such other rules deemed necessary in the conduct of ServRevo business.
    			</p>
    		</div>
    	</div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>
$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>
</body>
</html>
