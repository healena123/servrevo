<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
} else {
	if($_SESSION['access_id'] == '4'){
		header("Location: ".BASE_URI);
	}
}



error_reporting(0);

$user_id = $_GET['user'];
$option = $_GET['option'];
$where="";
if($option == 'reserved'){
	$where = " where status='1'";
} else if($option == 'all'){
	$where = "";
} else if($option == 'paid'){
	$where = " where is_paid='1'";
} else if($option == 'cancelled'){
	$where = " where is_cancelled='1' or status='0'";
} else if($option == 'booked'){
	$where = " where is_booked='1' and status != '0' and is_cancelled != '1'";
}
date_default_timezone_set('Asia/Manila'); 

$seat_ref = "Select * from Seats_ref";

$reserved_seats = "
	Select sr.id as reservation_id, sr.booking_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone 
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
";

$bookings = "select b.*, u.name, u.email, u.date_created as date_registered 
from Booking b
left join Users u on u.id=b.user_id $where  order by b.date_created desc";


$latest = "select b.*, u.name, u.email, u.date_created as date_registered, max(b.date_created) as latest_date 
from Booking b
left join Users u on u.id=b.user_id";


            			


?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>

</head>
<body class="serve-revo-admin">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<input type="hidden" value="<?php echo $user_id; ?>" class="user-id">
<?php include('dashboard_header.php'); ?>

<div class="content-wrap">
    <div class="left">
    	<div class="accordion-menu">
    		<div class="col">
  				<div class="acc-menu-link">
  					<a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
  						<i class="fa fa-folder"></i> Dashboard
  					</a>
    			</div>
    		</div>
    		
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Client Bookings
  					</a>
    			</div>
    			
  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Users
  					</a>
    			</div>
    			
  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Archive
  					</a>
    			</div>
    		
  			</div>
		</div>
    </div>
    </div>
    
    <!-- Right Content -->
    <div class="right">
    	<div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
    			<h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Client Database</h3>
    			<div class="breadcrumbs" style="padding:0;margin-top:3px">
    			<a href="<?php echo BASE_URI; ?>client_database.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>
    			<span style="color:#999;font-weight:normal;font-size:14px;">  >  </span>
    			<a style="color:#999;font-weight:normal;font-size:14px;">Profile</a>
    			</div>
    		</div>
    	<div class="row">
    		<div id="client-list" class="col-sm-12">
    			<div class="col-sm-12" style="padding-top:20px;padding-bottom:10px;">
    				<div style="float:left">
    					<div class="dropdown filter-drpdown">
  							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    							Filter by <small><img src="images/sort-desc.svg"></small>
  							</button>
  							<div class="dropdown-menu" id="filterByStatus" aria-labelledby="dropdownMenuButton">
  								<a data-opt="all" class="dropdown-item">All</a>
    							<a data-opt="reserved" class="dropdown-item">Reserved</a>
    							<a data-opt="paid" class="dropdown-item">Paid</a>
    							<a data-opt="booked" class="dropdown-item">Booked</a>
    							<a data-opt="cancelled" class="dropdown-item">Cancelled</a>
  							</div>
						</div>
    				</div>
    				<!-- Filters/Search -->
    				<div class="col-sm-4 client-list-filters">
    					<div class="search-holder col-sm-7" style="float:right;padding:0;position:relative;">
    						<i class="fa fa-search" style="color:#ccc;font-size:16px;position:absolute;top:9px;right:10px;"></i>
    						<input class="search form-control" placeholder="Search" style="width:100%;padding-right:30px;border-radius:20px;background:#fff"/>
    					</div>
  					</div>
  				</div>
  			
				<!-- Booking List -->
    			<ul class="booking-list-holder list col-sm-12">
    			<input type="hidden" class="user-id" value="<?php echo $user_id; ?>">
    			<?php 
    				
    				if($result = mysqli_query($connect, $bookings)){
    					if(mysqli_num_rows($result) > 0){
    						$count=0;
    						while($row = mysqli_fetch_array($result)){
    						
    						?>
    						
    						<?php
    						$count++;
    						echo '<li class="booking-entry">';
    						?>
    								<input type="hidden" name="booking_id" value="<?php echo $row['booking_id']; ?>">
    								<div class="col-sm-3 col-xs-12 booking-item">
    									
    									<h4 class="entry-title">
    										<a class="client-name" href="<?php echo BASE_URI; ?>client_page.php?user=<?php echo $row['user_id']; ?>">
    											<?php echo $row['name']; ?>
    										</a>
    									</h4>
    									<ul class="item-li">
    										<li class="booking-id">Booking ID: <?php echo $row['booking_id']; ?></li>
    										<li>Date of Registered: <?php echo date("d M Y", strtotime($row['date_registered'])); ?></li>
    										<li class="booking-date">Booking Date: <?php echo date("d M Y | H:s A", strtotime($row['date_created'])); ?></li>
    									</ul>
    								</div>
    								<div class="col-sm-2 col-xs-12 booking-item card ta-c">
    									<div class="card-body">
    										<h4>
    											<?php echo $row['total_seat_reserved']; ?>
    										</h4>
    										<small class="text-light">
    											No. of Seats
    										</small>
    									</div>
    								</div>
    								<div class="col-sm-2 col-xs-12 booking-item card ta-c">
    									<div class="card-body">
    										<h4>
    											<?php echo $row['total_days_reserved']; ?>
    										</h4>
    										<small class="text-light">
    											No. of Days
    										</small>
    									</div>
    								</div>
    								<div class="col-sm-2 col-xs-12 booking-item card ta-c">
    									<div class="card-body">
    										<h4 class="title-lite">
    											<?php echo date("M d", strtotime($row['date_from'])).'-'.date("M d", strtotime($row['date_to'])); ?>
    										</h4>
    										<small class="text-light">
    											Calendar
    										</small>
    									</div>
    								</div>
    								<div class="col-sm-2 col-xs-12 booking-item card ta-c">
    									<div class="card-body">
    										<h4 class="title-lite">
    											P<?php echo $row['total_reservation_amt']; ?>
    										</h4>
    										<?php 
    											if($row['is_paid'] == 0 && $row['status'] == 1){
    												echo '<label class="label label-warning filter1">Unpaid</label><br />';
    											}else if($row['is_paid'] == 1 && $row['status'] == 1){
    												echo '<label class="label label-info filter1">Paid</label><br />';
    											}else if($row['is_paid'] == 0 && $row['status'] == 0){
    												echo '<label class="label label-danger filter1">Cancelled</label><br />';
    											}
    										?>
    										
    									</div>
    								</div>
    								<div class="col-sm-1 col-xs-12 booking-item ta-c" style="padding:10px;">
    									<?php if($row['is_booked'] == '0') { ?>
    									<div style="margin-bottom:10px;">
    										<a class="btn btn-block book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="1" style="text-transform:uppercase;font-size:10px !important;background:#87a900;color:#fff;">
    											Book
    										</a>
    									</div>
    									<?php } ?>
    									<?php if($row['is_booked'] == '1') { ?>
    									<div style="margin-bottom:10px;">
    										<a class="btn btn-block book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="0" style="text-transform:uppercase;font-size:10px !important;background:rgba(30, 176, 237,1);color:#fff;">
    											Booked
    										</a>
    									</div>
    									<?php } ?>
    									
    									<?php if($row['is_paid'] == '0') { ?>
    									<div style="margin-bottom:10px;">
    										<a class="btn btn-block book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="4" style="text-transform:uppercase;font-size:10px !important;background:#e5bc5e;color:#fff;">
    											Pay
    										</a>
    									</div>
    									<?php } ?>
    									<?php if($row['is_paid'] == '1') { ?>
    									<div style="margin-bottom:10px;">
    										<a class="btn btn-block book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="5" style="text-transform:uppercase;font-size:10px !important;background:#e55e84;color:#fff;">
    											Paid
    										</a>
    									</div>
    									<?php } ?>
    									
    									
    									<?php if($row['is_cancelled'] == '0') { ?>
    									<div>
    										<a class="btn btn-block btn-default book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>"" data-action="2" style="font-size:10px;text-transform:uppercase;background:#eee;color:#000;">
    											Cancel
    										</a>
    									</div>
    									<?php } ?>
    									
    									
    									<?php if($row['is_cancelled'] == '1') { ?>
    									<div>
    										<a class="btn btn-block btn-danger book-reservation-btn <?php if($row['status'] == '0'){ echo 'btn-disable'; } ?>"" data-action="3" style="font-size:10px;text-transform:uppercase;background:#d9534f;color:#fff;padding-left:0;padding-right:0;text-align:center">
    											Cancelled
    										</a>
    									</div>
    									<?php } ?>
    								</div>
    						<?php 
    						echo '</li>';
    						}
   	 					} else {
   	 						?>
   	 						<br />
   	 						<li>No record found.</li>
   	 						<?php
   	 					}
					}
    			?>
    		</ul><!--/ Booking List -->
			
				<!-- Pagination -->
				<div class="col-sm-12 ta-r">
					<ul class="pagination"></ul>
				</div><!-- Pagination -->
			</div>
   		</div><!--/ Right Content -->
    </div>
</div>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/list.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>
var base_url = $('.baseurl').val();
$(document).on('click', '#filterByStatus a', function(){
	var opt = $(this).attr('data-opt');
	var user_id = $('.user-id').val();
	window.location.href = base_url+'client_database.php?user='+user_id+'&option='+opt;
});
$(document).ready(function(){
	
});
$(document).ready(function(){
	var options = {
    	valueNames: [ 'client-name','booking-id', 'booking-date'],
    	page: 10,
  		pagination: true
	};

	var listObj = new List('client-list', options);
	
	$('.search').on('keyup', function() {
  		var searchString = $(this).val();
  		listObj.search(searchString);
  		var count = $('#client-list .list li').length;
  		if(count == 0){
  			$('#client-list .list').html('<li style="padding-top:20px">No record found.</li>');
  		}
  	});
  	var itemsInList = [
{ filter1: "Paid" }
, { filter1: "Unpaid" }
, { filter1: "Cancelled" }
];
  	listObj.filter(function(item) {
		if (item.values().label > 1) {
   			return true;
		} else {
   			return false;
		}
	}); // Only items with id > 1 are shown in list
	listObj.filter();
});
$(document).on('click', '.book-reservation-btn', function(){
	var user_id = $('.user-id').val();
	var action = $(this).attr('data-action');
	var booking_id = $(this).closest('.booking-entry').find('input[name="booking_id"]').val();
	
	var url = base_url+'process_booking.php?booking_id='+booking_id+'&user_id='+user_id+'&action='+action;
	
	$.post(url, '', function (data) {
		window.location.reload();
	})
});
$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})
	
	if(err==true){
		$('.err-msg').removeClass('invi');
		
	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>
</body>
</html>