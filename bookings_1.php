<?php
session_start();

include_once ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
}

error_reporting(0);

date_default_timezone_set('Asia/Manila');


$user_id = $_GET['user'];
$user_info = "Select * from Users where id='$user_id'";

$seat_ref = "Select * from Seats_ref";

$reserved_seats = "
	Select sr.id as reservation_id, sr.booking_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
where user_id='$user_id'
";

$bookings = "select b.*, u.name, u.email, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id where user_id='$user_id' and b.is_deleted != '1' order by b.date_created desc";


$latest = "select b.*, u.name, u.email, u.date_created as date_registered, max(b.date_created) as latest_date
from Booking b
left join Users u on u.id=b.user_id where user_id='$user_id' and b.is_deleted != '1'";

if($result = mysqli_query($connect, $user_info)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		$username = $row['username'];
    		$password = $row['password'];
    		$email = $row['email'];
    	}
    }
}

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>

    <link rel="stylesheet" href="css/dataTables.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/datatable.responsive.css">
    <link rel="stylesheet" href="css/fixedColumns.dataTables.min.css">


    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
	<link href='css/fullcalendar.min.css' rel='stylesheet' />
	<link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
	<style>



  #calendar {
    max-width: 900px;
    margin: 40px auto;

  }
  .fc-view-container {
  	background:#fff
  }
	.fc-header-toolbar h2 {
		font-size:26px;
	}
	.fc-content {
		font-size:12px;
	}
	.dataTables_info {font-size:10px}
	.pagination-mds * {font-size:10px}
	.search-sm {position:relative;}
	.search-icon {position: absolute;
    top: 3px;
    left: 5px;
    z-index: 2;
    color: #999;}
	.search-sm input {z-index:0}
	.dataTables_wrapper .dataTables_filter input {margin-left:0 !important;min-width:230px;padding-left:30px}
	table.dataTable tbody td * {font-size:12px;}
	.dt-buttons {visibility:hidden}

</style>
</head>

<body class="serve-revo-client">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">

<?php include('dashboard_header.php'); ?>

<div class="content-wrap">

  <!-- Right Content -->
    <div class="centralize">


    	<div class="">
    		<div class="col-sm-12">
    			<div style="background:#fff;">
    				<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">My Bookings</h6>

    				<div style="padding:20px;position:relative">
					<div class="booking-filters">
<<<<<<< HEAD
						<div class="dropdown filter-drpdown">
							<button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Filter by <small><img src="images/sort-desc.svg"></small>
							</button>
							<div class="dropdown-menu" id="filterByStatus" aria-labelledby="dropdownMenuButton">
								<a data-opt="all" class="dropdown-item">All</a>
								<a data-opt="reserved" class="dropdown-item">Reserved</a>
								<a data-opt="paid" class="dropdown-item">Paid</a>
								<a data-opt="cancelled" class="dropdown-item">Cancelled</a>
							</div>
					</div>
=======
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00

						<div class="tab-actions">
                    		<button class="btn btn-default csv btn-sm ml10"><i class="fa fa-download"></i>&nbsp; CSV</button>
                    	</div>
    				</div>
    				<table id="bookingsDatatable" class="table" style="width:100%;font-size:12px;">
    					<thead>
    						<tr>
    							<th style="width:20px;padding-right:0"></th>
    							<th>Details</th>
    							<th>No. of Seats</th>
    							<th>No. of Days</th>
    							<th>Date Scheduled</th>
    							<th>Amount</th>
								<th>Status</th>
    							<th></th>
    						</tr>
    					</thead>
						<?php
    					 if($result = mysqli_query($connect, $bookings)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){ ?>
    		<tr data-booking-id="<?php echo $row['id']; ?>">
    			<td style="width:20px;padding-right:0;"><a class="trash-tr fa fa-trash"></a></td>
    			<td style="margin-bottom:10px;">
    				<strong><?php echo $row['name']; ?></strong><br/>Booking ID. <?php echo $row['booking_id']; ?><br />
    				<span style="color:#999;">Booking Date: <?php echo date("d M Y | h:i A", strtotime($row['date_created'])); ?></span>
    			</td>

    			<td style="text-align:center;"><?php echo $row['total_seat_reserved']; ?></td>
    			<td style="text-align:center;"><?php echo $row['total_days_reserved']; ?></td>
    			<td><?php echo date("M d", strtotime($row['date_from'])).'-'.date("M d", strtotime($row['date_to'])); ?></td>
    			<td style="text-align:center;font-weight:bold;">
    				P<?php echo $row['total_reservation_amt']; ?></td>
				<td>
    				<?php
													if($row['is_cancelled'] == 1){
														echo '<span style="color:#FF0000;">Cancelled</span><br />';
													}
    											else if($row['is_paid'] == 0 && $row['status'] == 1){
    												echo '<span style="color:#000;">Unpaid</span><br />';
    											}
													else if($row['is_paid'] == 1 && $row['status'] == 1){
    												echo '<strong><span style="color:#33691e;">Paid</span></strong><br />';
    											}
    										?>
    			</td>
    			<td>

    									<?php if($row['is_paid'] == '0') { ?>
										<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
										<input type="hidden" name="cmd" value="_s-xclick">
										<input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN">
<<<<<<< HEAD
										<input type="submit" class="btn btn-block book-reservation-btn1 <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;" value="Pay">
										<!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
										<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
=======
										<input type="submit" class="btn btn-block book-reservation-btn1 <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;margin-bottom:5px;" value="Pay">
										<!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
										<!--<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
										</form>

    									<?php } ?>
    									<?php if($row['is_paid'] == '1') { ?>
    										<a class="btn btn-block book-reservation-btn <?php if($row['is_cancelled'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="" style="text-transform:uppercase;font-size:10px !important;background:#00695c;color:#fff;">
    											Paid
    										</a>
    									<?php } ?>
    									<?php if($row['is_cancelled'] == '0') { ?>
    										<a data-booking-id="<?= $row['booking_id'] ?>" class="btn btn-block book-reservation-btn <?php if($row['is_paid'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="2" style="text-transform:uppercase;font-size:10px !important;background:#78909c;color:#fff;">
    											Cancel

    										</a>

    									<?php } ?>
    									<?php if($row['is_cancelled'] == '1') { ?>
<<<<<<< HEAD
    										<a data-booking-id="<?= $row['booking_id'] ?>" class="btn btn-block book-reservation-btn <?php if($row['is_paid'] == '1' || $row['status'] == '0'){ echo 'btn-disable'; } ?>" data-action="3" style="text-transform:uppercase;font-size:10px !important;background:#263238;color:#fff;">
=======
    										<a data-booking-id="<?= $row['booking_id'] ?>" class="btn btn-block book-reservation-btn <?php if($row['is_paid'] == '1' || $row['is_cancelled'] == '1'){ echo 'btn-disable'; } ?>" data-action="3" style="text-transform:uppercase;font-size:10px !important;background:#263238;color:#fff;">
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
    											Cancelled
    										</a>
    									<?php } ?>




    			</td>
    		<?php
    	}
    }
}
    					?>
    				</table></div>

    			</div>
    		</div>
    	</div>
    </div>
</div>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
 <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.buttons.min.js"></script>
         <script src="js/datatable.responsive.js"></script>
        <script src="js/dataTables.fixedColumns.min.js"></script>
        <script src="js/buttons.print.min.js"></script>
        <script src="js/buttons.flash.min.js"></script>
        <script src="js/buttons.html5.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>



<script src='js/lib/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>
<script>
var base_url = $('.baseurl').val();

// step 2: convert data structure to JSON
$(document).on('click', '.print_pdf', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	}, 200);
});
$(document).on('click', '.csv', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	}, 200);
});
$(document).on('click', '.trash-tr', function(){

	var count = $(this).closest('table').find('tbody tr.tr-item').length;

	if($(this).closest('table').find('tbody tr.tr-item').length === 1){
		return;
	}else {
		$(this).parent().closest('tr').animate( {backgroundColor:'#FAFEFF'}, 200).fadeOut(200,function() {
			var booking_id = $(this).attr('data-booking-id');
			var obj = {
				'item_id' : booking_id,
				'table' : 'Booking'
			}
			$.post(base_url+"process_delete.php", obj, function (data) {

			});
			$(this).remove();
		});
	}
});//delete table row
$(document).ready(function() {
		$('#bookingsDatatable').DataTable({
				"paging":   true,
		        "info":     true,
		        "bLengthChange": false,
		        "bDestroy": true,
		   //     "order": [[ 1, "desc" ]],
		        responsive: true,
		        "dom": '<"pull-left"B><"pull-right"lfr>tip',
		        buttons: [
            		'copy', 'csv', 'excel', 'pdf', 'print'
        		]
		  	});
  });

// function updateTable(obj){
// 	console.log(obj);
// 	//----DISPLAY new Branch Sales
// 	$.post(base_url + 'getBookings.php', obj, function (data) {
// 	    data = $.parseJSON(data);
//
// 	    var datarows = [];
//
// 	    data.forEach(function(entry){
// 	    	var datacol = [];
// 			var company = (entry.company != null) ? entry.company:'';
// 	        var details = '<strong>' + entry.name + '</strong><br />' + company + '<strong>Booking ID. '+entry.booking_id +'</strong><br />'+
//     				'<span style="color:#999;font-size:10px">Booking Date: '+ entry.date_created +'</span>';
//
//
//
//     		var stat = '';
//     											if(entry.is_paid == 0 && entry.status == 1){
//     												stat = '<strong><span style="color:#ff33ff;">Unpaid</span></strong><br />';
//     											}else if(entry.is_paid == 1 && entry.status == 1){
//     												stat = '<strong><span style="color:#4da6ff;">Paid</label></strong><br />';
//     											}else if(entry.is_paid == 0 && entry.status == 0){
//     												stat = '<strong><span style="color:#FF0000;">Cancelled</span></strong><br />';
//     											}
//     		var amt = '<div style="text-align:right"><strong>P'+entry.total_reservation_amt+'</strong><br />'+stat+'</div>';
//
//
//     		datacol.push('<a class="fa fa-trash trash-tr"></a>');
//        		datacol.push(details);
//        		datacol.push(name);
//        		datacol.push(entry.total_seat_reserved);
//        		datacol.push(entry.total_days_reserved);
//        		datacol.push(entry.date_from +' - '+entry.date_to);
//        		datacol.push(amt);
//        		datacol.push('<a class="btn btn-default">Cancel</a><a class="btn btn-default">Pay</a><a class="btn btn-default">Book</a>');
//
//        		datarows.push(datacol);
// 	    });
//
// 	    $('#bookingsDatatable').DataTable({
// 			data: datarows,
//             "paging":   true,
// 		        "info":     true,
// 		        "bLengthChange": false,
// 		        "bDestroy": true,
// 		      //  "order": [[ 1, "desc" ]],
// 		        responsive: true,
// 		        "dom": '<"pull-left"B><"pull-right"lfr>tip',
// 		        buttons: [
//             		'copy', 'csv', 'excel', 'pdf', 'print'
//         		]
// 	  	});
// 	});
// }
function updateTable(obj){
	// console.log(obj);
	//----DISPLAY new Branch Sales
	$.post(base_url + 'client_getBookings.php', obj, function (data) {
	    data = $.parseJSON(data);

	    var datarows = [];

	    data.forEach(function(entry){
	    	var datacol = [];
	        var details = 'Booking ID. '+entry.booking_id +'<br />'+
    				'<span style="color:#999;font-size:10px">Booking Date: '+ entry.date_created +'</span>';
    		var company = (entry.company != null) ? entry.company:'';
    		var name = '<div><strong>'+entry.name+'</strong></div>';

    		var stat = '';
													if(entry.is_cancelled == 1){
														stat = '<span style="color:#FF0000;text-align:center";">Cancelled</span><br />';
													}
    											else if(entry.is_paid == 0 && entry.status == 1){
    												stat = '<span style="color:#000;text-align:center";">Unpaid</span><br />';
    											}else if(entry.is_paid == 1 && entry.status == 1){
    												stat = '<strong><span style="color:#33691e;text-align:center";">Paid</span></strong><br />';
    											}
    		var amt = '<div style="text-align:center"><strong>P'+entry.total_reservation_amt+'</strong></div>';
				var fltrpy= '';
									if(entry.is_paid == 0) {
										if(entry.is_cancelled == 1 || entry.status == 0){
<<<<<<< HEAD
											fltrpy='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN"><input type="submit" class="btn btn-block book-reservation-btn1 btn-disable" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;" value="Pay"><img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"></form>';
										}
										else{
											fltrpy='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN"><input type="submit" class="btn btn-block book-reservation-btn1" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;" value="Pay"><img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"></form>';
=======
											fltrpy='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN"><input type="submit" class="btn btn-block book-reservation-btn1 btn-disable" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;margin-bottom:5px;" value="Pay"></form>';
										}
										else{
											fltrpy='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="X4K77RPBBKCKN"><input type="submit" class="btn btn-block book-reservation-btn1" style="text-transform:uppercase;font-size:10px !important;background:#4db6ac;color:#fff;margin-bottom:5px;" value="Pay"></form>';
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
										}
									}
									else if(entry.is_paid == 1) {
										if(entry.is_cancelled == 1 || entry.status == 0){
											fltrpy='<a data-booking-id='+entry.booking_id+' class="btn btn-disable btn-block book-reservation-btn" data-action="5" style="text-transform:uppercase;font-size:10px !important;background:#00695c;color:#fff;">Paid</a>';
										}
										else {
											fltrpy='<a data-booking-id='+entry.booking_id+' class="btn btn-disable btn-block book-reservation-btn" data-action="5" style="text-transform:uppercase;font-size:10px !important;background:#00695c;color:#fff;">Paid</a>';
										}
									}
				var fltrcncl='';
									if(entry.is_cancelled == 0) {
										if(entry.is_paid == 1){
										fltrcncl ='<a data-booking-id='+entry.booking_id+' class="btn btn-disable btn-block book-reservation-btn" data-action="2" style="text-transform:uppercase;font-size:10px !important;background:#78909c;color:#fff;">Cancel</a>';
									}
										else{
											fltrcncl ='<a data-booking-id='+entry.booking_id+' class="btn btn-block book-reservation-btn" data-action="2" style="text-transform:uppercase;font-size:10px !important;background:#78909c;color:#fff;">Cancel</a>';
										}
									}
									else if(entry.is_cancelled == 1) {
<<<<<<< HEAD
										if(entry.is_paid == 1){
=======
										if(entry.is_paid == 1 || entry.is_cancelled == 1){
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
										fltrcncl ='<a data-booking-id='+entry.booking_id+' class="btn btn-disable btn-block book-reservation-btn" data-action="3" style="text-transform:uppercase;font-size:10px !important;background:#263238;color:#fff;">Cancelled</a>';
									}
									else{
										fltrcncl ='<a data-booking-id='+entry.booking_id+' class="btn btn-block book-reservation-btn" data-action="3" style="text-transform:uppercase;font-size:10px !important;background:#263238;color:#fff;">Cancelled</a>';
									}
								}


    		datacol.push('<a class="fa fa-trash trash-tr"></a>');
					datacol.push(name+details);
       		datacol.push('<div style="text-align:center";>'+entry.total_seat_reserved+'</div>');
       		datacol.push('<div style="text-align:center";>'+entry.total_days_reserved+'</div>');
       		datacol.push('<div style="text-align:center";>'+entry.date_from +' - '+entry.date_to+'</div>');
       		datacol.push(amt);
					datacol.push(stat);
       		datacol.push(fltrpy+fltrcncl);

       		datarows.push(datacol);
	    });


	    $('#bookingsDatatable').DataTable({
			data: datarows,
            "paging":   true,
		        "info":     true,
		        "bLengthChange": false,
		        "bDestroy": true,
		     //   "order": [[ 1, "desc" ]],
		        responsive: true,
		        "dom": '<"pull-left"B><"pull-right"lfr>tip',
		        buttons: [
            		'copy', 'csv'
        		]
	  	});
	});
}

$(document).on('click', '.book-reservation-btn', function(){
	var user_id = <?php echo $user_id ?>;
	var action = $(this).attr('data-action');
	//var booking_id = $(this).closest('.booking-entry').find('input[name="booking_id"]').val();
	var booking_id = $(this).attr('data-booking-id');
<<<<<<< HEAD
=======
	if(action == "2"){
			var cnfrm = confirm("Are you sure you want to Cancel?");
			if(cnfrm == true){
				var url = base_url+'process_booking.php?booking_id='+booking_id+'&user_id='+user_id+'&action='+action;
				// console.log(url);
				$.post(url, '', function (data) {
					var opt = $('#filterByStatus a').attr('data-opt');
					var user_id = <?php echo $user_id ?>;
					var obj = {
						'option' : opt,
						'user_id' : user_id
					}
					// window.location.reload();
					updateTable(obj);
				})
			}
	}
	else{
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
	var url = base_url+'process_booking.php?booking_id='+booking_id+'&user_id='+user_id+'&action='+action;
	// console.log(url);
	$.post(url, '', function (data) {
		var opt = $('#filterByStatus a').attr('data-opt');
		var user_id = <?php echo $user_id ?>;
		var obj = {
			'option' : opt,
			'user_id' : user_id
		}
		// window.location.reload();
		updateTable(obj);
	})
<<<<<<< HEAD
});

$(document).on('click', '#filterByStatus a', function(){
	var opt = $(this).attr('data-opt');
	var user_id = <?php echo $user_id ?>;
	var obj = {
		'option' : opt,
		'user_id' : user_id
=======
>>>>>>> 3a52179b2f32d528fb10432fe2e05950d9248a00
	}
	updateTable(obj);
});

$(document).on('click', '#filterByStatus a', function(){
	var opt = $(this).attr('data-opt');
	var user_id = <?php echo $user_id ?>;
	var obj = {
		'option' : opt,
		'user_id' : user_id
	}
	updateTable(obj);
});
</script>

</body>
</html>
