<?php
session_start();
$err = isset($_GET['err'])?$_GET['err']:'';
include_once ('config.php');
?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<?php include('main-header.php'); ?>

<div class="container-fluid user-page-content">
    <div class ="">
    	<h1>Welcome to ServRevo</h1>
 		<div class="user-register-panel">
        		<form id="createAccount" action="process_register.php" method="post" autocomplete="off">
       				<input type="hidden" name="action" value="1">
       				<input type="hidden" name="user_type" value="co_working">
            		<div class="row">
            			<div class="err-msg"></div>
            			<?php if($err != ''){ ?><div style="color:red;margin-bottom:20px">Email already exists.</div><?php } ?>
            			<div class="col-sm-6">
            				<div class="form-group mb20">
    				<label for="username" class="lbl-sm">First Name</label>
    				<input type="text" class="form-control required" name="firstname" id="firstname" size="50">
  				</div>
            	<div class="form-group mb20">
    				<label for="username" class="lbl-sm">Last Name</label>
    				<input type="text" class="form-control required" name="lastname" id="lastname" size="50">
  				</div>
  				<div class="form-group mb20">
    				<label for="username" class="lbl-sm">Company</label>
    				<input type="text" class="form-control" name="company" id="company" size="50">
  				</div>
  				<div class="form-group mb20">
    				<label for="username" class="lbl-sm">Phone</label>
    				<input type="text" class="form-control" name="phone" id="phone" size="50">
  				</div>
  				<div class="form-group mb20">
                	<div class="col-12">
                		<label for="emailAddressFld" class="lbl-sm">How did you hear about us?</label>
                		<select class="form-control" name="source" id="source">
                                	<option value="">Please Select</option>
                                	<option value="Facebook">Facebook</option>
                                	<option value="Referral">Referral</option>
                                	<option value="Website">Website</option>
                                	<option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="form-group mb20 source-other hidden">
    				<input type="text" class="form-control" name="source_other" id="source_other" size="50">
  				</div>
                <div class="form-group">
                    <div class="col-12">
                    	<label for="emailAddressFld" class="lbl-sm" >Tell us what you need</label>
                        <textarea rows="3" class="form-control" name="message" id="message" style="resize: none;"></textarea>
                    </div>
                </div>
            	</div>
            	<div class="col-sm-6">
            	<div class="form-group mb20">
    				<label for="username" class="lbl-sm">Email</label>
    				<input type="text" class="form-control required" name="email" id="email" size="50">
  				</div>


                <div class="form-group mb20">
                    <label for="emailAddressFld" class="lbl-sm">Password</label>
                    <input type="password" class="form-control required" name="password" id="password" size="50">
                    <input type="hidden" name="ipaddress" id="ipaddress" >
                </div>

                <div class="form-group mb20">
                    <label for="emailAddressFld" class="lbl-sm">Confirm Password</label>
                    <input type="password" class="form-control required" name="confirm_password" id="confirm-password" size="50">
                    <input type="hidden" name="ipaddress" id="ipaddress" >
                </div>
                <div class="form-group text-left" style="font-size:12px;">
                	<input type="checkbox" value="0" id="terms-condition">&nbsp;&nbsp;<span class="asterisk">*</span>&nbsp; I agree to the <a href="<?php echo BASE_URI; ?>terms.php" target="_blank" style="font-weight:bold">Terms and conditions</a>
                </div>

                <div class="form-group text-left">
                	<button type="button" class="btn btn-block btn-serve-start" id="registerBtn" style="font-size:80%;"><strong>REGISTER</strong></button>
                </div>
                <div class="col-sm-12" style="font-size:12px;text-align:center">Already have a ServRevo account? <a href="<?php echo BASE_URI; ?>login.php">Login here.</a></div>
            	</div>
            </div>
      			</form>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>
var base_url = $('.baseurl').val();
$(document).on('change', '#source', function(){
	var el = $(this).val();

	if(el == 'Other'){
		$('.source-other').removeClass('hidden');
	}else {
		$('.source-other').addClass('hidden');
	}
});
$(document).on('change', '#terms-condition', function(){
	if ($(this).is(':checked')) {
		$(this).val(1);
	}else {
		$(this).val(0);
	}
});
 function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
 function validatePhone($phone) {
  var phoneReg = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  return phoneReg.test( $phone );
}
$(document).on('click', '#registerBtn', function(){

	var err=[];
	var err_msg=[];
	var err_msg1 = "",
		err_msg2 = "",
		err_msg3 = "",
		err_msg4 = "",
		err_msg5 = "",
		err_msg6 = "";

	$('#createAccount .required').each(function(){
		var el = $(this).val();
		var _this = $(this);

		if(el == ''){
			$(this).parent().addClass('has-error');
			if (err_msg.indexOf('Please input required fields below.') == -1) {
				err_msg.push('Please input required fields below.');
			}

			err_msg1 = 'Please input required fields below.';
			err.push(1);
		} else {

			if(_this.attr('name') == 'password' || _this.attr('name') == 'confirm_password'){
				var pass_val = $('#password').val();
				if( pass_val.length < 6){
					$('#password').parent().addClass('has-error');
					err.push(1);
					if (err_msg.indexOf(' Password should not less that 6 characters.') == -1) {
						err_msg.push(' Password should not less that 6 characters.');
					}

					err_msg3 = ' Password should not less that 6 characters.';
				}else if($('#password').val() != $('#confirm-password').val()){
					$('#password').parent().addClass('has-error');
					$('#confirm-password').parent().addClass('has-error');
					err.push(1);
					if (err_msg.indexOf(' Password and Confirm password not match.') == -1) {
						err_msg.push(' Password and Confirm password not match.');
					}
					err_msg2 = ' Password and Confirm password not match.';
				}else {
					$('#password').parent().removeClass('has-error');
					$('#confirm-password').parent().removeClass('has-error');
					err_msg2 = "";
				}
			}else if(_this.attr('name') == 'email'){
				var email = $('#email').val();

				if( !validateEmail(email)) {
					err.push(1);
						$('#email').parent().addClass('has-error');
						if (err_msg.indexOf(' Invalid email format.') == -1) {
							err_msg.push(' Invalid email format.');
						}
						err_msg5 = ' Invalid email format.';
				}else {
					$('#email').parent().removeClass('has-error');
					err_msg5 = '';
				}

				var res = function () {
					var tmp = null;

    				$.ajax({
        				'async': false,
        				'type': "POST",
        				'global': false,
        				'dataType': 'html',
        				'url': base_url + 'form_validation.php',
        				'data': { 'email': "", email },
        				'success': function (data) {
            			tmp = data;
       	 				}
    				});
    				return tmp;
				}();

				if(res > 0){
						err.push(1);
						if (err_msg.indexOf(' Email already exists.') == -1) {
							err_msg.push(' Email already exists.');
						}

						$('#email').parent().addClass('has-error');
						err_msg4 = ' Email already exists.';
					}else {
						$('#email').parent().removeClass('has-error');
						err_msg4 = '';
					}
			} else {
				$(this).parent().removeClass('has-error');
				err_msg=[];
				err_msg1 = '';
				err_msg2 = '';
				err_msg3 = '';
				err_msg4 = '';
				err_msg5 = '';
				err_msg6 = '';
			}


		}

	})

	if($('#phone').val() != ''){
		var phone = $('#phone').val();

		//Valid formats:
		/*(123) 456-7890
		(123)456-7890
		123-456-7890
		123.456.7890
		1234567890
		+31636363634
		075-63546725	*/

		if( !validatePhone(phone)) {
					err.push(1);
						$('#phone').parent().addClass('has-error');
						if (err_msg.indexOf(' Invalid phone format.') == -1) {
							err_msg.push(' Invalid phone format.');
						}

						err_msg6 = ' Invalid phone format.';
				}else {
					$('#phone').parent().removeClass('has-error');
					err_msg6 = '';
				}
			}

	if(err.length > 0){
	$('.err-msg').empty();
		$.each(err_msg, function(i,v){
			$('.err-msg').append(v);
		});
		//$('.err-msg').html(err_msg1 + err_msg2 + err_msg3 + err_msg4 + err_msg5 + err_msg6);
	}else{
		//console.log($('#terms-condition').val());
		if($('#terms-condition').val() == 0){
			$('.err-msg').html('Terms and condition is required.');
		} else {
			$('.err-msg').empty();
			$('#createAccount').submit();
		}

	}
});
</script>
</body>
</html>

