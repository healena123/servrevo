<?php
//error_reporting(E_ALL);

// redirect on direct access
if(!isset($_POST['submit'])) {
    header('Location: ' . BASE_URI);
}

include('config.php');

// collect datas
$data = [
    'lastname' => $_POST['lastname'],
    'firstname' => $_POST['firstname'],
    'phone' => $_POST['phone'],
    'company' => $_POST['company'],
    'email' => $_POST['email'],
    'password' => $_POST['pass'],
    'access_id' => $_POST['access_id'], // user type (guest=coworking)
];

// access_id => type
$usertype = [
    1 => 'super_admin',
    'admin',
    'staff',
    'co_working'
];

$stmt = $connect->prepare(
    'insert into users (firstname, lastname, email, company, ' .
        'password, phone, user_type, access_id, date_created, ' .
        'last_updated, is_deleted) values ' .
        '(?, ?, ?, ?, md5(?), ?, ?, ?, now(), now(), 0)');
$stmt->bind_param("sssssssi",
    $data['firstname'],
    $data['lastname'],
    $data['email'],
    $data['company'],
    $data['password'],
    $data['phone'],
    $usertype[$data['access_id']],
    $data['access_id']);

if($stmt->execute()) {
    header('Location: ' . BASE_URI . 'clients.php?user=' . $_GET['user']);
} else {
    die('unable to add user: ' . $connect->error);
}
