<?php
session_start();
$res = isset($_GET['res'])?$_GET['res']:'';

include_once ('config.php');

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>
	
</head>
<body id="serve-revo-main">


<?php include('main-header.php'); ?>

<div class="container-fluid" style="padding:0 0 60px">
	<div style="border-bottom:1px solid #ddd">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.210004163093!2d121.05978431408421!3d14.587106181319662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c817359bf515%3A0x4957cdef25403536!2sStrata+2000!5e0!3m2!1sen!2sph!4v1519798914160" style="width:100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
    <div class ="centralize" style="padding:0">
    	
		<div class="">
			<div class="col-sm-8">
				<h3 class="col-sm-12" style="text-align:left">Get In Touch</h3>
				<form id="inquiryForm" action="<?php echo BASE_URI; ?>process_inquire.php" method="post">
					<?php if($res != ''){ ?>
					<div class="col-sm-12" style="color:#87a900;margin-bottom:10px">Your inquiry has been sent successfully.</div>
					<?php } ?>
					<div class="col-sm-12 err-msg" style="text-align:left"></div>
					<div class="col-sm-8" style="margin-bottom:10px;position:relative"> 
						<label style="position:absolute;top:5px;left:25px;font-size:11px;color:#999;font-weight:bold">NAME <span class="asterisk">*</span></label>
						<input type="text" name="inquire_name" class="required" style="width: 100%;
    padding: 20px 15px 10px;
    border: 0;background:#eee" maxlength="50">
					</div>
					<div class="col-sm-8" style="margin-bottom:10px;position:relative"> 
						<label style="position:absolute;top:5px;left:25px;font-size:11px;color:#999;font-weight:bold">EMAIL <span class="asterisk">*</span></label>
						<input type="text" name="inquire_email" class="required" style="width: 100%;
    padding: 20px 15px 10px;
    border: 0;background:#eee" maxlength="50">
					</div>
					<div class="col-sm-10" style="margin-bottom:10px;position:relative;"> 
						<label style="position:absolute;top:5px;left:25px;font-size:11px;color:#999;font-weight:bold;">MESSAGE <span class="asterisk">*</span></label>
						<textarea name="inquire_msg" class="required" style="width: 100%;resize: none;
    padding: 20px 15px 10px;
    border: 0;background:#eee" maxlength="300"></textarea>
					</div>
					<div class="col-sm-3" style="">
						<button type="button" id="submitBtn" class="btn btn-serve-callback" style="display:inline-block;">SUBMIT</button>
					</div>
				</form>
			</div>
			<div class="col-sm-4" style="padding-top:60px">
				<div class="col-sm-12">
					<strong>Email</strong>
					<p>start@servrevo.com</p>
				</div>
				<div class="col-sm-12">
					<strong>Phone</strong>
					<p>(632) 635 7626</p>
				</div>
				<div class="col-sm-12">
					<strong>Address</strong>
					<p>10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</p>
				</div>
				<div class="social-media col-sm-12">
					
            		<a href="https://www.facebook.com/servrevocorp/" class="fa fa-facebook fa-lg facebook"></a>
            		<a href="https://www.linkedin.com/company/servrevo-corp/" class="fa fa-linkedin fa-lg linkedin"></a>
            	
				</div>
			</div>
		</div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script>

$(document).on('click', '#submitBtn', function(){
	var err=[];
	var err_msg1 = "",
		err_msg2 = "";
	$('#inquiryForm .required').each(function(){
		var el = $(this).val();
		var _this = $(this);
		if(el == ''){
			$(this).parent().addClass('has-error');
			err_msg1 = 'Please input required fields below.';
			err.push(1);
		} else {
			if(_this.attr('name') == 'inquire_email'){
				var email = $('input[name="inquire_email"]').val();
				
				if( !validateEmail(email)) { 
					err.push(1);
						$('input[name="inquire_email"]').parent().addClass('has-error');
						err_msg2 = ' Invalid email format.';
				}else {
					$('input[name="inquire_email"]').parent().removeClass('has-error');
					err_msg2 = '';
				}
			}
		}
	});
	console.log(err);
	if(err.length > 0){
		$('.err-msg').html(err_msg1 + err_msg2);
	}else{
		
			$('.err-msg').empty();
			$('#inquiryForm').submit();
		
		
	}
});
function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
</script>
</body>
</html>

