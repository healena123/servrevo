<?php
error_reporting(0);

include_once ('config.php');
include_once ('util.php');

$uri = $_SERVER['REQUEST_URI'];
$uri_arr = explode('/', $uri);

$active_page = array_pop((array_slice($uri_arr, -1)));

//var_dump($active_page);
?>
<nav class="navbar navbar-default" style="margin-bottom:0">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#serve-header-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand">
              <?php if($_SESSION['user_id'] == ''){?>
                <a href="<?php echo BASE_URI; ?>" class="logo-a-m"><img class="logo-m" src="images/serv_logo_gr.png"></a>
                <a href="<?php echo BASE_URI; ?>" class="logo-a"><img class="logo" src="images/serv_logo_gr.png"></a>
              <?php } else { ?>
                <!-- disable main index hyperlinks if a user is logged in -->
                <a href="<?php '#' /*echo BASE_URI; ?>?user=<?php echo $_SESSION['user_id']; */?>" class="logo-a-m"><img class="logo-m" src="images/serv_logo_gr.png"></a>
                <a href="<?php '#' /*echo BASE_URI; ?>?user=<?php echo $_SESSION['user_id']; */?>" class="logo-a"><img class="logo" src="images/serv_logo_gr.png"></a>
              <?php } ?>
                <p>Source. Setup. Start.</p>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" aria-expanded="true" id="serve-header-bar">
          <?php if($_SESSION['user_id'] == ''){?>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="hvr-underline-reveal <?php if($active_page == ''){ echo 'active';} ?>" href="<?php echo BASE_URI; ?>">Home</a></li>
                <li><a class="hvr-underline-reveal <?php if($active_page == 'offshore-services.php'){ echo 'active';} ?>" data-href-index="0" href="<?php echo BASE_URI; ?>offshore-services.php">Offshore Services</a></li>
                <li><a class="hvr-underline-reveal <?php if($active_page == 'co-working.php'){ echo 'active';} ?>" data-href-index="1" href="<?php echo BASE_URI; ?>co-working.php">Co-Working</a></li>
                <li><a class="hvr-underline-reveal <?php if($active_page == 'payroll-management.php'){ echo 'active';} ?>" data-href-index="2" href="<?php echo BASE_URI; ?>payroll-management.php">HR & Payroll Management</a></li>
                <li><a class="hvr-underline-reveal <?php if($active_page == 'recruitment-staffing.php'){ echo 'active';} ?>" data-href-index="3" href="<?php echo BASE_URI; ?>recruitment-staffing.php">Recruitment Staffing</a></li>
              <?php } else{ ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="hvr-underline-reveal <?php if($active_page == ''){ echo 'active';} ?>" href="<?php echo BASE_URI; ?>?user=<?php echo $_SESSION['user_id'];?>">Home</a></li>
                    <li><a class="hvr-underline-reveal <?php if($active_page == 'offshore-services.php'){ echo 'active';} ?>" data-href-index="0" href="<?php echo BASE_URI; ?>offshore-services.php?user=<?php echo $_SESSION['user_id'];?>">Offshore Services</a></li>
                    <li><a class="hvr-underline-reveal <?php if($active_page == 'co-working.php'){ echo 'active';} ?>" data-href-index="1" href="<?php echo BASE_URI; ?>co-working.php?user=<?php echo $_SESSION['user_id'];?>">Co-Working</a></li>
                    <li><a class="hvr-underline-reveal <?php if($active_page == 'payroll-management.php'){ echo 'active';} ?>" data-href-index="2" href="<?php echo BASE_URI; ?>payroll-management.php?user=<?php echo $_SESSION['user_id'];?>">HR & Payroll Management</a></li>
                    <li><a class="hvr-underline-reveal <?php if($active_page == 'recruitment-staffing.php'){ echo 'active';} ?>" data-href-index="3" href="<?php echo BASE_URI; ?>recruitment-staffing.php?user=<?php echo $_SESSION['user_id'];?>">Recruitment Staffing</a></li>
                  <?php }?>
              <?php if(isset($_SESSION['email'])){ ?>
                <li class="login-li">
                 	<div class="dropdown">
                 		<a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-href-index="1">
                 			<i class="fa fa-user"></i>&nbsp;<?php echo $_SESSION['email']; ?>
                 		</a>
                 		<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <a class="dropdown-item" href="<?= BASE_URI . 'client_page.php?user=' . $_SESSION['user_id'] ?>">My Profile</a>
                 			<?php if($_SESSION['access_id'] == '4'){ ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>bookings.php?user=<?php echo $_SESSION['user_id'];?>">My Bookings</a>
                 			<?php } else { ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $_SESSION['user_id'];?>">My Dashboard</a>
                 			<?php } ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id'];?>">Reserve a Seat</a>
							<a class="dropdown-item" href="<?php echo BASE_URI; ?>co-working.php?user=<?php echo $_SESSION['user_id'].'#container';?>">Reserve a Conference</a>

  							<div class="dropdown-divider"></div>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>logout.php">Logout</a>
  						</div>
  					</div>
                 </li>
                 <?php }else { ?>
                 <li><a class="hvr-underline-reveal" data-href-index="3" href="<?php echo BASE_URI; ?>login.php" style="color:#87a900;font-weight:bold">Sign In</a></li>
                 <?php } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
