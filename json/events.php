<?php

error_reporting(0);

include_once ('../config.php');

$query = "select booking_id, date_from, date_to, status, is_paid,is_booked, is_deleted, is_cancelled from Booking where is_deleted != '1' and is_cancelled != '1' and status != '0'";

$data_array = array();

if($result = mysqli_query($connect, $query)){
	
	
	while ($row = mysqli_fetch_array($result))
	{    
    		$color = 'green';
    		if($row['is_paid'] == '1' && $row['is_booked'] == '1'){
    			$color = '#0099CC'; //blue (paid)
    		}
    		if($row['is_booked'] == '1' && $row['is_paid'] == '0'){
    			$color = '#FF7A5A'; //orange (booked)
    		}
    		if($row['is_paid'] == '0' && $row['is_booked'] == '0'){
    			$color = '#81A594'; //green (reserved)
    		}
    		$data_array[] = array(
    			'title' => 'Booking No.: '.$row['booking_id'],
    			'start' => $row['date_from'] . 'T00:00:00',
    			'end' => $row['date_to']. 'T24:00:00',
    			'color'=> $color
    		);
   	 	
   }
}
$fp = fopen('res.json', 'w');
fwrite($fp, json_encode($data_array));
fclose($fp);

//echo json_encode($data_array);
?>