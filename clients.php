<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
} else {
	if($_SESSION['access_id'] == '4'){
		header("Location: ".BASE_URI);
	}
}



error_reporting(0);

$user_id = $_GET['user'];
date_default_timezone_set('Asia/Manila');


$latest_users = "select * from Users where is_deleted != '1' ORDER BY date_created DESC";



?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>

   <link rel="stylesheet" href="css/dataTables.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/datatable.responsive.css">
    <link rel="stylesheet" href="css/fixedColumns.dataTables.min.css">


    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
	<link href='css/fullcalendar.min.css' rel='stylesheet' />
	<link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />


		<style>



  #calendar {
    max-width: 900px;
    margin: 40px auto;

  }
  .fc-view-container {
  	background:#fff
  }
	.fc-header-toolbar h2 {
		font-size:26px;
	}
	.fc-content {
		font-size:12px;
	}
	.dataTables_info {font-size:10px}
	.pagination-mds * {font-size:10px}
	.search-sm {position:relative;}
	.search-icon {position: absolute;
    top: 3px;
    left: 5px;
    z-index: 2;
    color: #999;}
	.search-sm input {z-index:0}
	.dataTables_wrapper .dataTables_filter input {margin-left:0 !important;min-width:230px;padding-left:30px}
	table.dataTable tbody td * {font-size:12px;}
	.dt-buttons {visibility:hidden}
</style>
</head>
<body class="serve-revo-admin">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<?php include('dashboard_header.php'); ?>

<div class="content-wrap">
    <div class="left">
    	<div class="accordion-menu">
    		<div class="col">
  				<div class="acc-menu-link">
  					<a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
  						<i class="fa fa-folder"></i> Dashboard
  					</a>
    			</div>
    		</div>

  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Client Bookings
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Users
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Archive
  					</a>
    			</div>
  			</div>

        <div class="col">
          <div class="acc-menu-link">
            <a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
                <i class="fa fa-folder"></i> Reports
            </a>
          </div>
        </div>
		</div>
    </div>

    <!--*
    | Modal dialogs
    |
    |
    |*-->
    <!-- Add user modal-->
    <div class="modal fade" id="addusermodal" tabindex="-1" role="dialog" aria-labeledby="addusermodallabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <!-- close button -->
            <button type="button" class="close" data-dismiss="modal"aria-label="close">
              <span aria-hidden="true">&times;</span>
            </button>

            <h4 class="modal-title" id="addusermodallabel">Add User</h4>
          </div>

          <form action="process_add_user.php?user=<?= $user_id ?>" method="POST" id="add_user">

            <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
                  <label for="lastname">Lastname</label>
                  <input type="text" name="lastname" class="form-control" required>
                </div>

                <div class="col-md-4">
                  <label for="firstname">First Name</label>
                  <input type="text" name="firstname" class="form-control" required>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <label for="phone">Contact #(+63)</label>
                  <input type="number" name="phone" class="form-control" >
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <label for="company">Company</label>
                  <input type="text" name="company" class="form-control" required>
                </div>
              </div>

              <div class="row">
               <div class="col-md-4">
                  <label for="email">Email</label>
                  <span style="font-size:0.75em;" id="emailError" class="text-danger"></span>
                  <input type="email" name="email" class="form-control" required>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <label for="pass">Password</label>
                  <input id="pass" type="password" name="pass" class="form-control" value="123456"
                   required>
                </div>

                <div class="col-md-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" id="show_password"> Show password
                    </label>
                  </div>
                </div>
              </div>

                <div class="row">
                  <div class="col-md-4">
                    <label for="access_id">User Type</label>
                    <select class="form-control" name="access_id">
                      <option value="1">Super Admin</option>
                      <option value="2">Admin</option>
                      <option value="3">Staff</option>
                      <option value="4">Guest (Co-working)</option>
                    </select>
                  </div>
                </div>
            </div>

            <div class="modal-footer">
              <input class="btn btn-primary" type="submit"
                value="Submit">
            </div>

          </form>
        </div>
      </div>
    </div>

    <!-- sucess modal-->
    <div class="modal fade" id="sucessmodal" tabindex="-1" role="dialog" aria-labeledby="successmodallabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <!-- close button -->
            <button type="button" class="close" data-dismiss="modal"aria-label="close">
              <span aria-hidden="true">&times;</span>
            </button>

            <h4>Success!</h4>
          </div>

          <div class="modal-body">
            <p>User is sucessfully added!</p>
          </div>

          <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Right Content -->
    <div class="right">
    	<div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
    			<h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Dashboard</h3>
    			<div class="breadcrumbs" style="padding:0;margin-top:3px">
    			<a href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>
    			<span style="color:#999;font-weight:normal;font-size:14px;">  >  </span>
    			<a style="color:#999;font-weight:normal;font-size:14px;">User List</a>
    			</div>
    	</div>

    	<div class="">
    		<div class="col-sm-12">
    			<div style="background:#fff;">
    				<h6 style="border-bottom:1px solid #eee;padding:20px 15px 8px;color:#999">Users</h6>
    				<div style="padding:20px;position:relative">
    					<div class="booking-filters">
  							<div class="tab-actions">
                			<button class="btn btn-default print_pdf btn-sm ml10"><i class="fa fa-print"></i>&nbsp; Print PDF</button>
                			<button class="btn btn-default csv btn-sm ml10"><i class="fa fa-download"></i>&nbsp; CSV</button>

                      <!-- add user -->
                        <button class="btn btn-primary m110" data-toggle="modal" data-target="#addusermodal">
                          <i class="fa fa-plus">&nbsp;</i>
                          Add User
                        </button>
                  </div>
                </div>
    					</div>
    				<table id="usersDatatable" class="table" style="width:100%;font-size:12px;">
    					<thead>
    						<tr>
    							<th style="width:20px;padding-right:0"></th>
    							<th>Name/Company</th>
    							<th>Email</th>
    							<th>Contact</th>
    							<th>Date Registered</th>
    							<th>User Type</th>

    						</tr>
    					</thead>
    					<?php
    					 if($result = mysqli_query($connect, $latest_users)){
    if(mysqli_num_rows($result) > 0){
    	while($row = mysqli_fetch_array($result)){
    		echo '<tr data-user-id="'.$row['id'].'"><td style="width:20px;padding-right:0;"><a class="trash-tr fa fa-trash"></a></td><td style="margin-bottom:10px;"><img src="images/avatar.png" style="width:40px;height:40px;float:left;display:inline-block;margin-right:10px;">';
    		echo '<div style="line-height:16px;"><a href="'.BASE_URI.'client_page.php?user='.$row['id'].'">'.$row['firstname'].' ' .$row['lastname'].'</a><br /><span style="font-size:12px;color:#999">'.$row['company'].'</span></div>';
    		echo '</td><td>'.$row['email'].'</td><td>'.$row['phone'].'</td><td>'.
          (new DateTime($row['date_created']))->format('Y-m-d | h:i A').'</td><td>'.$row['user_type'].'</td></tr>';
    	}
    }
}
    					?>
    				</table></div>

    			</div>
    		</div>
    	</div>
    </div>
</div>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
 <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script src="js/dataTables.buttons.min.js"></script>
         <script src="js/datatable.responsive.js"></script>
        <script src="js/dataTables.fixedColumns.min.js"></script>
        <script src="js/buttons.print.min.js"></script>
        <script src="js/buttons.flash.min.js"></script>
        <script src="js/buttons.html5.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>



<script src='js/lib/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>

<script>
var base_url = $('.baseurl').val();

// step 2: convert data structure to JSON
$(document).on('click', '.print_pdf', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-print').click();
	}, 200);
});
$(document).on('click', '.csv', function(){
	var _this = $(this);
	$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	setTimeout(function(){
		$('.dataTables_wrapper').find('.dt-buttons .buttons-csv').click();
	}, 200);
});

  $(document).ready(function() {
		$('#usersDatatable').DataTable({
				"paging":   true,
		        "info":     true,
		        "bLengthChange": false,
		        "bDestroy": true,
		        "order": [[ 1, "desc" ]],
		        responsive: true,
		         "dom": '<"pull-left"B><"pull-right"lfr>tip',
		        buttons: [
            		'copy', 'csv', 'excel', 'pdf', 'print'
        		]

		  	});
  });
$(document).on('click', '.trash-tr', function(e){

  if(!confirm('Are you sure you want to delete this user ?')) {
    e.preventDefault();
    return false;
  }

	var count = $(this).closest('table').find('tbody tr.tr-item').length;

	if($(this).closest('table').find('tbody tr.tr-item').length === 1){
		return;
	}else {
		$(this).parent().closest('tr').animate( {backgroundColor:'#FAFEFF'}, 200).fadeOut(200,function() {
			var user_id = $(this).attr('data-user-id');
			var obj = {
				'item_id' : user_id,
				'table' : 'Users'
			}
			$.post(base_url+"process_delete.php", obj, function (data) {

			});

			$(this).remove();
		});
	}
});//delete table row

/*
var onFormSubmitSucess = function(d) {
  $('#sucessmodal').modal('toggle'); // show ok dialog
  location.reload();
}
*/
/* 03/12/18:
 * Show password if show password check box is checked
 *
 */
$(document).on('click', '#show_password', function() {
  var showPassword = document.getElementById('show_password').checked;
  var passwordField = document.getElementById('pass');

  if(showPassword) {
    // change password type to textbox
    passwordField.type = 'text';
  } else {
    passwordField.type = 'password';
  }
});

/*
 * Validate Form Input on submit
 */
$(document).on('submit', '#add_user', function(form) {
  console.log("@onSubmit()");
  //form.preventDefault();
  // check if email is already taken
  var email = $('#add_user input[name=email]').val();
  $.ajax({
    async: false,
    url: base_url + 'api_user_validation_v2.php',
    data: {
      func: 'user-attr-exists', // call this check function
      x: 'email', // column
      val: email
    },
    //dataType: 'html',
    success: function(e) {
      var result = JSON.parse(e);
      console.log(result.found);

      if(result.found) {
        $('#add_user #emailError').html('Email already exists.');
        form.preventDefault(); // cancel submission
      }
    },
    error: function(x, e) {
      console.log(x);
      console.log(e);
    }
  });

});

</script>

</body>
</html>
