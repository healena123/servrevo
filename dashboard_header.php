<?php
include_once ('config.php');
include_once ('util.php');

?>
<nav class="navbar navbar-default" style="position:fixed;width:100%;display:inline-block;z-index:999;border-bottom:3px solid #87a900;background:rgb(50,50,50);">
    <div class="container-fluid" style="">
    	<!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#serve-header-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand">
              <?php if(isLoggedin()): ?>
                <a href="#"><img style="height:25px;width:auto" class="logo-m" src="images/serv_logo_wh.png"></a>
              <?php else: ?>
                <a href="<?php echo BASE_URI; ?>?user=<?php echo $_SESSION['user_id'];?>"><img style="height:25px;width:auto" class="logo-m" src="images/serv_logo_wh.png"></a>
              <?php endif; ?>
            </div>
        </div>


    	<!--<div style="position:absolute;top:15px;left:20px;">
            	<a href="/"><img style="height:25px;width:auto" class="logo-m" src="images/serv_logo_wh.png"></a>
        	</div>-->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" aria-expanded="true" id="serve-header-bar">
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['email'])){ ?>
                <li class="login-li">
                 	<div class="dropdown">
                 		<a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-href-index="1">
                 			<i class="fa fa-user"></i>&nbsp;<?php echo $_SESSION['email']; ?>
                 		</a>
                 		<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                      <!-- profile link -->
                      <a class="dropdown-item" href="<?= BASE_URI . 'client_page.php?user=' . $_SESSION['user_id']?>">
                        My Profile
                      </a>

                 			<?php if($_SESSION['access_id'] == '4'){ ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>bookings.php?user=<?php echo $_SESSION['user_id'];?>">My Bookings</a>
                 			<?php } else { ?>
                 			<a class="dropdown-item hidden" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $_SESSION['user_id'];?>">Bookings</a>
                 			<?php } ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id'];?>">Reserve a Seat</a>
							<a class="dropdown-item" href="<?php echo BASE_URI; ?>co-working.php?user=<?php echo $_SESSION['user_id'].'#container';?>">Reserve a Conference</a>
  							<div class="dropdown-divider"></div>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>logout.php">Logout</a>
  						</div>
  						<div class="dropdown-collapse-m">
  							<?php if($_SESSION['access_id'] == '4'){ ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>bookings.php?user=<?php echo $_SESSION['user_id'];?>">My Bookings</a>
                 			<?php } else { ?>
                 			<a class="dropdown-item hidden" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $_SESSION['user_id'];?>">Bookings</a>
                 			<?php } ?>
                 			<a class="dropdown-item" href="<?php echo BASE_URI; ?>reservation.php?user=<?php echo $_SESSION['user_id'];?>">Reserve a Seat</a>
							<a class="dropdown-item" href="<?php echo BASE_URI; ?>co-working.php?user=<?php echo $_SESSION['user_id'].'#container';?>">Reserve a Conference</a>
  							<div class="dropdown-divider"></div>
  							<?php if($_SESSION['access_id'] == '1' || $_SESSION['access_id'] == '3' || $_SESSION['access_id'] == '2'){ ?>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $_SESSION['user_id'];?>">Dashboard</a>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $_SESSION['user_id'];?>">Client Bookings</a>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $_SESSION['user_id'];?>">Users</a>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $_SESSION['user_id'];?>">History</a>
  							<?php } ?>
  							<a class="dropdown-item" href="<?php echo BASE_URI; ?>logout.php">Logout</a>
  						</div>
  					</div>
                 </li>
                 <?php }else { ?>
                 <li><a class="hvr-underline-reveal" data-href-index="3" href="<?php echo BASE_URI; ?>login.php" style="color:#87a900;font-weight:bold">Sign In</a></li>
                 <?php } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
