/**
 * Created by Lon on 6/30/2017.
 */

jQuery(document).ready(function(){
    jQuery('#serve-revo-main.serve-revo-offshore .row1-body #mainCarousel').carousel({
        interval: false,
        pause: true
    });

    jQuery(".carousel").swipe({

        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'left') jQuery(this).carousel('next');
            if (direction == 'right') jQuery(this).carousel('prev');

        },

    });

    var heights = $("#serve-revo-main.serve-revo-offshore .row2-container-box .row-box-content").map(function() {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);

    $("#serve-revo-main.serve-revo-offshore .row2-container-box .row-box-content").height(maxHeight);

    var heights = $("#serve-revo-main.serve-revo-offshore .row2-body .row2-icon-box-bordered").map(function() {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);

    $("#serve-revo-main.serve-revo-offshore .row2-body .row2-icon-box-bordered").height(maxHeight);
});
