<?php
//
// Main file for updating a user profile
//

// FIXME: Revalidate submitted fields

// Startup Routines
session_start();
error_reporting(E_ALL); // <<= DEBUG

include('config.php');

// I. Validation

// Check if a user is actually logged in
if(!isset($_SESSION['user_id'])) {
    header('Location: ' . BASE_URI . 'index.php');
}

// Check if the submitted user id actually matches the currently
// logged in user_id (aka: the user is editing his own profile)

if($_SESSION['user_id'] != $_POST['id']) {
    header('Location: ' . BASE_URI . 'index.php');
}

// Collect the variables
$data = [
    //'id' => $_POST['user_id'],
    'firstname' => $_POST['firstname'],
    'lastname' => $_POST['lastname'],
    'phone' => $_POST['phone'],
    'company' => $_POST['company'],
    'username' => $_POST['username']
];

// Query String
$qs = sprintf('update users set %s',
    implode(
        array_map(
            // SQL SET Clause
            function($x) { return $x . ' = ?'; },
            array_keys($data)),
    ', ')
);

// Query parameter types
$types = 'sssss';

// will the password be changed as well ?
if(intval($_POST['update-password'])) {
    $data['password'] = $_POST['new-password'];
    // concatenate password field and parameter type
    $qs .= ', password = md5(?)';
    $types .= 's';
}

// WHERE clause
$qs .= ' where id = ?';
$types .= 'i';

// Values of each parameters + id
$values = array_values($data);
$values[] = $_POST['id'];

var_dump($values);

$stmt = $connect->prepare($qs);
$stmt->bind_param($types, ...$values); // Unpack $data array's values
$updateOk = 1;

if(!$stmt->execute()) {
    $updateOk = 0;
}

header(sprintf('Location: %sclient_page.php?user=%d&update=%d',
    BASE_URI,
    $_SESSION['user_id'],
    $updateOk
));

