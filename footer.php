<footer class="footer-m2" style="display:none">
    <div class = "container">
        <div class = "row">
            <div class = "col-sm-12 col-xs-12">
                <img src="images/footer-logo.png" />
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Phone: (632) 635 7626</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Email: start@servrevo.com</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h5>Copyright 2016 ServRevo Corp.</h5>
                <p>10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</p>
            </div>
        </div>
    </div>
</footer>

<footer class="footer">
    <div class="center">
        <div class="row">
        	<div class="col-sm-7">
            <a href="#"><img class="logo" src="images/serv_logo_wh.png"></a>
          </div>

          <div class="col-sm-3">
          	<div>
          		<a href="https://www.facebook.com/servrevocorp/" style="margin-right:5px;"><img src="images/facebook.png" style="height:32px; width:32px;"></a>
          		<a href="https://www.linkedin.com/company/servrevo-corp/" style="margin-right:5px;"><img src="images/linkedin.png" style="height:32px; width:32px;"></a>
              <a href="https://www.instagram.com/servrevocoworking/"><img src="images/instagram.png" style="height:32px; width:32px;"></a>
          	</div>
          </div>
        </div>

        <div class="row" style="margin-top:1em;">
          <center>
            <p class="col" style="display:inline;">Phone: (632) 635 7626 | </p>
            <p class="col" style="display:inline;">Email: start@servrevo.com</p>
          </center>
        </div>

        <div class="address" style="margin-top:.1em;">10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</div>

    </div>
    <div class="copy col-sm-12">
    	<ul class="col-sm-6 footer-nav" style="margin:0">
    			<li><a href="<?php echo BASE_URI; ?>terms.php">Terms &amp; Condition</a> |</li>
    			<li><a href="<?php echo BASE_URI; ?>contact.php">Contact Us</a></li>
    		</ul>
    	<p class="col-sm-6">Copyright 2016 ServRevo Corp.</p>
    </div>
</footer>
