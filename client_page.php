<?php

include ('config.php');

// Startup Routines
error_reporting(0);
session_start();

// Not logged in ?
if(session_id() == '' || !isset($_SESSION['email']) ) {
  header("Location: ".BASE_URI);
}

//
// Helper Functions
//

// Is the user viewing his own profile?
function ownProfile() {
  //echo '<pre>' . $_SESSION['user_id'] . ' ' . $_GET['user'] . '</pre>';
  return $_SESSION['user_id'] == $_GET['user'];
}

// Is the logged in user an admin (super_admin, admin)?
function isAdmin() {
  return $_SESSION['access_id'] == 1 || $_SESSION['access_id'] == 2;

}

// Redirect to Main Page if $_GET[user] is not set [04/18/18]
if(empty($_GET['user'])) {
  header('Location: ' . BASE_URI . 'client_page.php?user=' . $_SESSION['user_id']);
}

$user_id = $_GET['user'];
date_default_timezone_set('Asia/Manila');

$seat_ref = "Select * from Seats_ref";

$reserved_seats = "
Select sr.id as reservation_id, sr.booking_id, sr.user_id, sr.seat_id, sr.date as reserve_date, sr.date_created,
s_ref.seat_no, s_ref.tbl_no, s_ref.price,
u.name, u.email, u.phone
from Seats_reservation sr
left join Seats_ref s_ref on s_ref.id=sr.seat_id
left join Users u on u.id=sr.user_id
";

/**$bookings = "select b.*, u.name, u.email, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id where b.user_id='$user_id' order by b.date_created desc";**/
$bookings2 = "select b.*, u.name, u.email, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id where user_id='$user_id' and b.is_deleted != '1' and booking_type = '0' order by b.date_created desc";

$bookings = "select b.*, u.name, u.email, u.date_created as date_registered
from Booking b
left join Users u on u.id=b.user_id where user_id='$user_id' and b.is_deleted != '1' and booking_type = '1' order by b.date_created desc";


$latest = "select b.*, u.name, u.email, u.date_created as date_registered, max(b.date_created) as latest_date
from Booking b
left join Users u on u.id=b.user_id where b.user_id='$user_id'";

$user_info = "Select * from Users where id=$user_id";

if ($result=mysqli_query($connect, $user_info))
{
  if(mysqli_num_rows($result) > 0){
    // Fetch one and one row
    while ($row=mysqli_fetch_array($result))
    {
      $id = $row['id'];
      $fullname= $row['firstname'] . ' ' . $row['lastname'];
      $firstname=$row['firstname'];
      $lastname=$row['lastname'];
      $email=$row['email'];
      $company=$row['company'];
      $phone=$row['phone'];
      $date_created=$row['date_created'];
      $username = $row['username'];
    }
  } else {
    // User does not exist
    // FIXME: Show a custom 404 error page instead
    header('Location: ' . BASE_URI . 'client_page.php?user=' . $_SESSION['user_id']);
  }
}
?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <title>Home</title>
  <!-- CUSTOM STYLESHEETS -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/hover.css"/>
  <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
  <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
  <link rel="stylesheet" type="text/css" href="css/style.css"/>
  <link rel="stylesheet" type="text/css" href="css/layout.css"/>
  <link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
  <link rel="stylesheet" type="text/css" href="css/modal.css"/>
</head>

<style>
  .tab {
    overflow: hidden;
    background-color: #66bb6a;
  }

  /* Style the buttons inside the tab */

  .tab button {
    background-color: inherit;
    color: white;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 8px 16px;
    transition: 0.3s;
    font-size: 17px;
  }

  /* [Added: 05/24/18] */
  .center {
    margin: auto;
    width: 70%;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
    background-color: #e8f5e9;
    color: black;
  }

  /* Create an active/current tablink class */
  .tab button.active {
    background-color: #2e7d32;
  }

  /* Style the tab content */
  .tabcontent {
    background: #e8f5e9;
    display: none;
    padding: 6px 12px;
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s;
  }

  /* Fade in tabs */
  @-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
  }

  @keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
  }

  input[type=text] , input[type=password] {
    border: none;
    background-color: inherit;
    font-size:12px;
    border-bottom: 1px solid #a5d6a7;
  }

  div.error {
    color:red;
    font-size: 13px;
  }

  input.field-muted {
    color: gray;
  }

  div.field-error {
    color: red;
    font-size: 13px;
  }

  div.field-ok {
    color: green;
    font-size: 13px;
  }

  div.hidden-field {
    display: none;
  }

  textarea {
    width: 100%;
    height: 150px;
    padding: 12px 20px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    background-color: #f8f8f8;
    font-size: 16px;
    resize: none;
  }

  .button:hover {
    opacity:1;
  }
</style>

<body class="serve-revo-admin" style="">
  <input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">

  <?php include('dashboard_header.php'); ?>

  <div class="content-wrap">

    <?php if(isAdmin()): /* show sidebar if the user is an administrator */?>

    <div class="left">
      <div class="accordion-menu">

        <div class="col">
          <div class="acc-menu-link">
           <a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
            <i class="fa fa-folder"></i> Dashboard
          </a>
        </div>
      </div>

      <div class="col">
        <div class="acc-menu-link">
          <a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
            <i class="fa fa-folder"></i> Client Bookings
          </a>
        </div>
      </div>

      <div class="col">
        <div class="acc-menu-link">
          <a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
            <i class="fa fa-folder"></i> Users
          </a>
        </div>
      </div>

      <div class="col">
        <div class="acc-menu-link">
          <a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
            <i class="fa fa-folder"></i> Archive
          </a>
        </div>
      </div>

      <div class="col">
        <div class="acc-menu-link">
          <a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
            <i class="fa fa-folder"></i> Reports
          </a>
        </div>
      </div>

    </div> <!-- accordion-menu -->
  </div> <!-- left -->

<?php endif; ?>

<div class="<?= isAdmin() ? 'right' : 'center' ?>">

 <div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
   <h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Profile</h3>
   <div class="breadcrumbs" style="padding:0;margin-top:3px">
     <a href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>
     <span style="color:#999;font-weight:normal;font-size:14px;">  >  </span>
     <a style="color:#999;font-weight:normal;font-size:14px;">Profile</a>
   </div>
 </div>

 <div class="col-sm-12" style="padding:0">
  <div class="col-md-12 col-sm-12 rcontent-right-section">

    <!-- show a message if $_GET['update'] is present -->
    <?php
    if(isset($_GET['update'])):
      $code = intval($_GET['update']);

      if($code):
        echo '<p style="color: #87a900;">Profile Sucessfully Updated.</p>';

      else:
        echo '<p class="text-warning">Unable to update profile. Please try again.</p>';

      endif; // $code
    endif; // isset($_GET['update']
    ?>

    <div class="booking-list-holder" style="background:#fff; margin-bottom:20px;">
      <div style="border-bottom:1px solid #eee;padding-bottom:10px;margin-bottom:20px">
       <div class="tab">
         <button id="profile-tab-trigger" class="tablinks" onclick="openCity(event, 'Profile')">Profile</button>


         <?php
       // show edit button if the user is viewing his own profile:
         if(ownProfile()): ?>
         <a style="font-size:24px;float:right;margin-top:5px; margin-right:5px; color:white;"
         class="js-signin-modal-trigger"
         data-signin="signup" href="#" data-toggle="modal" data-target="#modalRegister">
         <span class="fa fa-edit"></span>
       </a>
     <?php endif; ?>

     <div id="modalRegister" class="modal fade" role="dialog">
      <div class="modal-dialog" style="background:#f1f8e9;width:30em;max-width:93%;border-radius:2%;">
       <div class="w3-panel">
        <div class="w3-row-padding" style="margin:0 -16px">
         <br/>
         <form id="edit-form" method="POST" action="process_edit_profile.php">

          <input type="hidden" name="id" value="<?= $id ?>">

          <h5 style="margin-left:17px;">Personal Details</h5>

          <table class="w3-table w3-striped w3-white" style="border-radius:5px;font-size:13px;">

            <tr>
             <td>First Name:</td>
             <td><input required name="firstname" type="text" value="<?php echo $firstname?>"></input></td>
           </tr>
           <tr>
             <td>Last Name:</td>
             <td><input required name="lastname" type="text" value="<?php echo $lastname?>"></input></td>
           </tr>
           <tr>
             <td>Email:</td>
             <td><input class="field-muted" type="text" value="<?php echo $email; ?>" readonly></input></td>
           </tr>
           <tr>
             <td>Contact Number (+63):</td>
             <td><input required name="phone" type="number" value="<?php echo $phone; ?>"></input></td>
           </tr>
           <tr>
             <td>Company:</td>
             <td><input name="company" type="text" value="<?php echo $company; ?>"></input></td>
           </tr>
           <!--
           <tr>
             <td>Username:</td>
             <td><input required data-current-username="<?= $username ?>"
                name="username" type="text"  value="<?= $username ?>"></input>
                <div id="username-msg"></div>
              </td>
           </tr>
         -->
       </table>
       <br/>

       <h5 style="margin-left:17px;">Security</h5>

       <div id="password-field" class="hidden-field">

        <!-- update the password as well ?-->
        <input type="hidden" name="update-password" value="0">

        <table class="w3-table w3-striped w3-white" style="border-radius:5px;font-size:13px;">
         <tr>
           <td>Current Password:</td>
           <td><input name="current-password" type="password"></input>
            <div id="current-password-msg"></div>
          </td>
        </tr>
        <tr>
         <td>New Password:</td>
         <td>
          <input name="new-password" type="password"></input>
          <div id="new-password-msg"></div>
        </td>
      </tr>
      <tr>
       <td>Confirm Password:</td>
       <td>
        <input name="confirm-password" type="password" value></input>
        <div id="confirm-password-msg"></div>
      </td>
    </tr>
  </table>
</div> <!-- password-field -->

<a style="margin-left: 17px; font-size: 13px" data-edit-mode="0" id="change-password" href="#" style="margin-left:1em;">Change Password</a>

<br/>
<br/>
<button type="submit" id="submitbtn" class="button"
style="float:right; margin-bottom:15px; margin-right:1em; background:#004d40;color:white;">
Save Changes
</button>
</form>
</div> <!-- cd-signin-modal__container -->
</div>
</div>
</div>

</div>


<div id="Profile" class="tabcontent">
  <div class="w3-panel">
   <div class="w3-row-padding" style="margin:0 -16px">

    <h5>&nbsp; Personal Details</h5>
    <table class="w3-table w3-striped w3-white">
     <tr>
      <td><i class="fa fa-user w3-large"></i></td>
      <td>Name:</td>
      <td><i><?php echo $fullname?></i></td>
    </tr>
    <tr>
      <td><i class="fa fa-envelope w3-large"></i></td>
      <td>Email:</td>
      <td><i><?php echo $email; ?></i></td>
    </tr>
    <tr>
      <td><i class="fa fa-phone w3-large"></i></td>
      <td>Contact Number (+63):</td>
      <td><i><?php echo $phone; ?></i></td>
    </tr>
    <tr>
      <td><i class="fa fa-building w3-large"></i></td>
      <td>Company:</td>
      <td><i><?php echo $company; ?></i></td>
    </tr>
    <!--
    <tr>
      <td></td>
      <td>Username:</td>
      <td><i><?= $username ?></i></td>
    </tr>
  -->
</table>
</div>
</div>
</div>
</div>


<footer class="footer-m" style="display:none">
  <div class = "container-fluid">
    <div class = "row">
      <div class = "col-sm-12 col-xs-12">
        <img src="images/footer-logo.png" />
      </div>
      <div class = "col-sm-12 col-xs-12">
        <h4>Phone: +63 917 539 8008</h4>
      </div>
      <div class = "col-sm-12 col-xs-12">
        <h4>Email: start@servrevo.com</h4>
      </div>
      <div class = "col-sm-12 col-xs-12">
        <h5>Copyright 2016 ServRevo Corp.</h5>
      </div>
    </div>
  </div>
</footer>

<footer class="footer" style="width:100%;display:none">
  <div class = "container">
    <div class = "footer-nav">
      <a href="#"><img class="logo" src="images/footer-logo.png"></a>
      <p>Copyright 2016 ServRevo Corp.</p>
    </div>
    <div class = "footer-nav-items">
      <ul>
        <li><a>Phone: +63 917 539 8008</a></li>
        <li><a>Email: start@servrevo.com</a></li>
      </ul>
    </div>
  </div>
</footer>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>
<script src="js/placeholders.min.js"></script> <!-- polyfill for the HTML5 placeholder attribute -->

<!-- Main script file -->
<script src="js/profile/profile.js"></script>
<script>
var base_url = $('.baseurl').val();

$(document).on('click', '.book-reservation-btn', function(){
  var user_id = $('.user-id').val();
  var action = $(this).attr('data-action');
  var booking_id = $(this).closest('.booking-entry').find('input[name="booking_id"]').val();

  var url = base_url + 'process_booking.php?booking_id='+booking_id+'&user_id='+user_id+'&action='+action;

  $.post(url, '', function (data) {
    window.location.reload();
  })
});
$(document).on('click', '#registerBtn', function(){
  var err=false;
  $('#createAccount .required').each(function(){
    var el = $(this).val();
    if(el == ''){
      $(this).parent().addClass('has-error');
      err=true;
    } else {
      $(this).parent().removeClass('has-error');
      err=false;
    }
  })

  if(err==true){
    $('.err-msg').removeClass('invi');

  }else{
    $('.err-msg').addClass('invi');
    $('#createAccount').submit();
  }
});
</script>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>
