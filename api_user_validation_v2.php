<?php
//
// API for querying user attributes
//

error_reporting(E_ALL);
include('config.php');

//  Possible responses:
// found = true | false
// data = [not used]
// msg
class Response {
    var $found = null;
    var $data = null;
    var $msg = null;

    public function __construct($found = null, $data = null, $msg = null) {
        $this->found = $found;
        $this->data = $data;
        $this->msg = $msg;
    }
}

$_HANDLER = [
    // checks if a user's attribute already exists
    'user-attr-exists' => 'userAttrExists',
    // checks if a user's password is correct
    'user-pass-matches' => 'userPasswordMatches'
];

//
// Helper Functions
//
function check($val, $msg) {
    if(!isset($val)) {
        returnError($msg);
        return null; // NOOP
    }
    return $val;
}

function returnError($msg) {
    exit(json_encode(new Response(null, null, $msg)));
}

// check if there is any resulting row
//
// Arguments:
//  args -> dictionary in the form [column] = value
function hasRows($query) {
    $r = new Response();
    $result = false;

    if($query->execute()) {
        $r->found = false;
        $result = $query->get_result();

        if(mysqli_num_rows($result) > 0) {
            $r->found= true;
        }
    } else {
        $r->found =  null;
    }

    return $r;
}

//
// Functions
//

// Method: GET
// Checks if user attibute exists in the database
function userAttrExists() {
    global $connect;

    $column = check($_GET['x'], 'no-column');
    $val = check($_GET['val'], 'no-value');
    $command = "select * from users where $column = ?";
    $query = $connect->prepare($command);

    $query->bind_param("s", $val);

    return hasRows($query);
}

// Method: POST
// Checks if the string matches the current user's password
// sets Response.found = true
//
// Required Arguments:
// Post Variables
//      password (raw)
function userPasswordMatches() {
    global $connect;

    $userId = check($_SESSION['user_id'], 'no-id');
    $password = check($_POST['password'], 'no-password');
    $command = "select id from users where id = ? AND password = md5(?)";
    $query = $connect->prepare($command);

    $query->bind_param("is", $userId, $password);

    return hasRows($query);
}

/*
echo '<pre>';
var_dump($queryFunc);
var_dump($f);
var_dump($GLOBALS);
echo '</pre>';
*/

// BEGIN CODE
session_start();

$f = check($_GET['func'], 'no-function');
echo json_encode($_HANDLER[$f]());
