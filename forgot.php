<?php
include_once ('config.php');

$err = isset($_GET['err'])?$_GET['err']:'';
?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">

<?php include('main-header.php'); ?>

<div class="container-fluid user-page-content">
    <div class ="">
    	<h1>Forgot your password?</h1>
 		<div class="user-login-panel">
        	<form id="forgotPasswordForm" action="<?php echo BASE_URI; ?>forgot_password.php" method="post" autocomplete="off">
        		
        		<div class="err-msg"></div>
            			<?php if($err != ''){ ?><div style="color:red;margin-bottom:20px">Sorry, the account doesn't exist</div><?php } ?>
            	<div class="form-group mb20">
    				<label for="email" class="lbl-sm">Email</label>
    				<input type="text" class="form-control" name="email" id="name" size="50">
  				</div>
				
               
                <div style="height:10px;"></div>
                <div class="form-group text-left">
                	<button type="button" class="btn btn-block btn-serve-start" id="forgotBtn">Send password reset link</button>
                </div>
                <div style="text-align:center;margin:10px 0;">
                	<small style="font-size:12px;">
  						Return to <a href="<?php echo BASE_URI; ?>login.php">Login here.</a>
  					</small>
                </div>
               <div class="col-sm-12" style="font-size:10px;">Or, create account? <a href="<?php echo BASE_URI; ?>register.php">Sign Up here.</a></div>
            
        	</form>
   
		</div>
    </div>
</div>

<?php include('footer.php'); ?>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>

<script>
$(document).on('click', '#forgotBtn', function(){
	if($('#name').val() == ''){
		$('#name').parent().addClass('has-error');
		$('.err-msg').html('Email is required.');
	}else{
		$('#name').parent().removeClass('has-error');
		$('.err-msg').empty();
		$('#forgotPasswordForm').submit();
	}
});
</script>
</body>
</html>