<?php

//
// Parses data based on the given parameter strings and returns a JSON response
//
// GET parameters
// format = JSON | CSV
// date_from = ms elapsed since epoch
// date_to =   ms elapsed since epoch


// debug
error_reporting(E_ALL);

//
// include files
//
include_once('config.php');

// A response object that will be returned and converted to JSON
class ReportResponse {
    var $rows;

    public function __construct($rows = [][]) {
        $this->rows = $rows;
    }

    public function push($column) {
        $rows[] = $column;
    }
}

//
// Startup Routines
//

// Check if the currently logged in user is an administrator; (super_admin, admin)
// then return a 401 Unauthenticated JSON response if not
$userType = intval($_SESSION['access_id']);
if(!($userType == 1 || $userType == 2)) {
    exit(json_encode([ 'error' => 401 ]));
}

// Available data extraction formats
$_DATA_FORMATS = [
    'JSON' => 'getReport_JSON',
    'CSV' => 'getReport_CSV'
];

//
// Functions
//

// Helper Functions

function getParam($name) {

}

// Returns a formatted SQL WHERE clause
function getDateWhereClause($dateFrom, $dateTo) {
    if($dateFrom && $dateTo) {
        return 'where date_from >= ? && date_to <= ?';
    } else if($dateFrom) {
        return 'where date_from >= ?';
    } else if($dateTo) {
        return 'where date_to <= ?';
    } else {
        return '';
    }
}

// Binds the date parameters to a prepared statement object
// $p - A prepared statement object
function bindParameters($p, $dateFrom, $dateTo) {
    if($dateFrom && $dateTo) {
        $p->bind_param('', $dateFrom, $dateTo);
    } else if($dateFrom) {
        $p->bind_param('', $dateFrom);
    } else if($dateTo) {
        $-p->bind_param('', $dateTo);
    }
}

function getReport_JSON($dateFrom, $dateTo) {
    /*
    // TODO: Category
    Query String Format:

    select * from booking
    [ where (date_from >= %1 | date_to <= %2  |  date_from> >= %1 AND date_to <= %2) ]
    */

    global $connect;
    $s = 'select * from booking ' . getDateWhereClause($dateFrom, $dateTo);
    //$q = $connect->prepare($s);
    //bindParameters($q, $dateFrom, $dateTo);
    return json_encode([ 'query' => $s ]);
}


function getReport_CSV($dateFrom, $dateTo, $category) {

}

// Main Routine

$_DATA_FORMATS[$_GET['format']](getParam())
