
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Calendar2</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    
    <link href='calendar2/fullcalendar.min.css' rel='stylesheet' />
	<link href='calendar2/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    
    

<script src='calendar2/lib/moment.min.js'></script>
<script src='calendar2/lib/jquery.min.js'></script>
<script src='calendar2/fullcalendar.min.js'></script>
<script>

  $(document).ready(function() {

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      defaultDate: '2018-01-19',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'Caption1a',
          start: '2018-01-01'
        },
        {
          title: 'Caption1b',
          start: '2018-01-07',
          end: '2018-01-10'
        },
        {
          id: 999,
          title: 'Caption1',
          start: '2018-01-11'
        },
        {
          title: 'Caption2',
          url: 'http://google.com/',
          start: '2018-01-13'
        }
      ]
    });

  });

</script>
<style>


  #calendar {
    max-width: 500px;
    margin: 0 auto;
  }
  	.fc-toolbar.fc-header-toolbar {position:relative}
  	.fc-toolbar.fc-header-toolbar .fc-center {float:left !important}
	
	.fc-right,
	.fc-today-button {display:none}
	
	.fc-toolbar.fc-header-toolbar .fc-left {position:absolute;top:0;right:0}
	.fc-content-skeleton a {font-size:12px;color:#999}
	
	.fc-toolbar.fc-header-toolbar h2 {font-family: "Lemon/Milk";
    font-size: 20px;}
	.fc-row.fc-widget-header th {font-size:12px;color:#666}
	.fc-event-container .fc-day-grid-event {background: silver;
    color: #fff;
    border: transparent;}
    .fc-event-container .fc-content {font-size:12px;}
</style>
</head>
<body id="serve-revo-main">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" style="border-bottom:1px solid #f0f0f0">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#serve-header-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div>
                <a href="/"><img class="logo-m" src="images/servrevo-logo-m.png"></a>
                <a href="/"><img class="logo" src="images/servrevo-logo.png"></a>
                <p>Source. Setup. Start.</p>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" aria-expanded="true" id="serve-header-bar">
            <ul class="nav navbar-nav navbar-right">
                <li><a class="hvr-underline-reveal" href="/">Home</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="0" href="/offshore-services.html">Offshore Services</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="1" href="/seat-leasing.html">Seat Leasing</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="2" href="/payroll-management.html">HR & Payroll Management</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="3" href="/recruitment-staffing.html">Recruitment Staffing</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class = "container-fluid" style="background:#f9f9f9;padding:60px 0">
  <div id='calendar'></div>
</div>
<footer class="footer-m">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-sm-12 col-xs-12">
                <img src="images/footer-logo.png" />
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Phone: +63 917 539 8008</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Email: start@servrevo.com</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h5>Copyright 2016 ServRevo Corp.</h5>
            </div>
        </div>
    </div>
</footer>

<footer class="footer">
    <div class = "container">
        <div class = "footer-nav">
            <a href="#"><img class="logo" src="images/footer-logo.png"></a>
            <p>Copyright 2016 ServRevo Corp.</p>
        </div>
        <div class = "footer-nav-items">
            <ul>
                <li><a>Phone: +63 917 539 8008</a></li>
                <li><a>Email: start@servrevo.com</a></li>
            </ul>
        </div>
    </div>
</footer>

</body>
</html>
