<?php
$err = isset($_GET['err'])?$_GET['err']:'';
?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/layout.css"/>

</head>
<body id="serve-revo-main" class="user-page">

<?php include('main-header.php'); ?>

<div class="container-fluid user-page-content">
    <div class ="">
    	
    	<div class="col-12" style="text-align:center;font-size: 28px;line-height: 34px;color:rgba(183,130,28,1)">We sent you a link in your email for verification.</div>
  				<p class="join-instruction col-12" style="text-align:center;margin-bottom:20px">Click the link in your email or enter code below.</p>
 		<div class="user-login-panel">
        	<form id="verificationForm" action="/process_reset_password.php" method="post" autocomplete="off">
        		<input type="hidden" name="action" value="2">
        		<div class="err-msg"></div>
            			<?php if($err != ''){ ?><div style="color:red;margin-bottom:20px">The password reset code is no longer valid.</div><?php } ?>
            	<div class="form-group mb20">
    				<label for="email" class="lbl-sm">Enter code here</label>
    				<input type="text" class="form-control" name="code" id="code" size="50">
  				</div>
				
               
                <div style="height:10px;"></div>
                <div class="form-group text-left">
                	<button type="button" class="btn btn-block btn-serve-start" id="verificationBtn">Verify Reset Code</button>
                </div>
                
            
        	</form>
   
		</div>
    </div>
</div>

<footer class="footer-m">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-sm-12 col-xs-12">
                <img src="images/footer-logo.png" />
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Phone: +63 917 539 8008</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Email: start@servrevo.com</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h5>Copyright 2016 ServRevo Corp.</h5>
            </div>
        </div>
    </div>
</footer>

<footer class="footer" style="position:fixed;bottom:0;width:100%">
    <div class = "container">
        <div class = "footer-nav">
            <a href="#"><img class="logo" src="images/footer-logo.png"></a>
            <p>Copyright 2016 ServRevo Corp.</p>
        </div>
        <div class = "footer-nav-items">
            <ul>
                <li><a>Phone: +63 917 539 8008</a></li>
                <li><a>Email: start@servrevo.com</a></li>
                <li><small>10F Strata 2000, F. Ortigas Jr. Road Ortigas Center, San Antonio, Pasig, 1605 Metro Manila</small></li>
            </ul>
        </div>
    </div>
</footer>
<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/main.js" type="text/javascript"></script>

<script>
$(document).on('click', '#verificationBtn', function(){
	if($('#code').val() == ''){
		$('#code').parent().addClass('has-error');
		$('.err-msg').html('Please enter code.');
	}else{
		$('#code').parent().removeClass('has-error');
		$('.err-msg').empty();
		$('#verificationForm').submit();
	}
});
</script>
</body>
</html>