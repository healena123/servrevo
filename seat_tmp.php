
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Calendar1</title>
    <!-- CUSTOM STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>
    
    <link rel="stylesheet" href="calendar1/components/bootstrap2/css/bootstrap.css">
	<link rel="stylesheet" href="calendar1/components/bootstrap2/css/bootstrap-responsive.css">
	<link rel="stylesheet" href="calendar1/css/calendar.css">
    
    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/header.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

	

</head>
<body id="serve-revo-main">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" style="border-bottom:1px solid #f0f0f0">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#serve-header-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div>
                <a href="/"><img class="logo-m" src="images/servrevo-logo-m.png"></a>
                <a href="/"><img class="logo" src="images/servrevo-logo.png"></a>
                <p>Source. Setup. Start.</p>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" aria-expanded="true" id="serve-header-bar">
            <ul class="nav navbar-nav navbar-right">
                <li><a class="hvr-underline-reveal" href="/">Home</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="0" href="/offshore-services.html">Offshore Services</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="1" href="/seat-leasing.html">Seat Leasing</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="2" href="/payroll-management.html">HR & Payroll Management</a></li>
                <li><a class="hvr-underline-reveal" data-href-index="3" href="/recruitment-staffing.html">Recruitment Staffing</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class = "container-fluid" style="background:#f9f9f9;padding:60px 0">
	
	<div class="page-header" style="display:none">

		<div class="pull-right form-inline" style="display:none">
			<div class="btn-group">
				<button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
				<button class="btn" data-calendar-nav="today">Today</button>
				<button class="btn btn-primary" data-calendar-nav="next">Next >></button>
			</div>
			<div class="btn-group">
				<button class="btn btn-warning" data-calendar-view="year">Year</button>
				<button class="btn btn-warning active" data-calendar-view="month">Month</button>
				<button class="btn btn-warning" data-calendar-view="week">Week</button>
				<button class="btn btn-warning" data-calendar-view="day">Day</button>
			</div>
		</div>

		<h3></h3>
		<small>To see example with events navigate to march 2013</small>
	</div>

	<div class="row">
		<div class="span4"></div>
		<div class="span4">
			<div id="calendar"></div>
		</div>
		<div class="span4"></div>
		<div class="span3" style="display:none">
			<div class="row-fluid">
				<select id="first_day" class="span12">
					<option value="" selected="selected">First day of week language-dependant</option>
					<option value="2">First day of week is Sunday</option>
					<option value="1">First day of week is Monday</option>
				</select>
				<select id="language" class="span12">
					<option value="">Select Language (default: en-US)</option>
					<option value="bg-BG">Bulgarian</option>
					<option value="nl-NL">Dutch</option>
					<option value="fr-FR">French</option>
					<option value="de-DE">German</option>
					<option value="el-GR">Greek</option>
					<option value="hu-HU">Hungarian</option>
					<option value="id-ID">Bahasa Indonesia</option>
					<option value="it-IT">Italian</option>
					<option value="pl-PL">Polish</option>
					<option value="pt-BR">Portuguese (Brazil)</option>
					<option value="ro-RO">Romania</option>
					<option value="es-CO">Spanish (Colombia)</option>
					<option value="es-MX">Spanish (Mexico)</option>
					<option value="es-ES">Spanish (Spain)</option>
					<option value="es-CL">Spanish (Chile)</option>
					<option value="es-DO">Spanish (República Dominicana)</option>
					<option value="ru-RU">Russian</option>
					<option value="sk-SR">Slovak</option>
					<option value="sv-SE">Swedish</option>
					<option value="zh-CN">简体中文</option>
					<option value="zh-TW">繁體中文</option>
					<option value="ko-KR">한국어</option>
					<option value="th-TH">Thai (Thailand)</option>
				</select>
				<label class="checkbox">
					<input type="checkbox" value="#events-modal" id="events-in-modal"> Open events in modal window
				</label>
				<label class="checkbox">
					<input type="checkbox" id="format-12-hours"> 12 Hour format
				</label>
				<label class="checkbox">
					<input type="checkbox" id="show_wb" checked> Show week box
				</label>
				<label class="checkbox">
					<input type="checkbox" id="show_wbn" checked> Show week box number
				</label>
			</div>

			<h4>Events</h4>
			<small>This list is populated with events dynamically</small>
			<ul id="eventlist" class="nav nav-list"></ul>
		</div>
	</div>


	<script type="text/javascript" src="calendar1/components/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="calendar1/components/underscore/underscore-min.js"></script>
	<script type="text/javascript" src="calendar1/components/bootstrap2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="calendar1/components/jstimezonedetect/jstz.min.js"></script>
	<script type="text/javascript" src="calendar1/js/language/bg-BG.js"></script>
	<script type="text/javascript" src="calendar1/js/language/nl-NL.js"></script>
	<script type="text/javascript" src="calendar1/js/language/fr-FR.js"></script>
	<script type="text/javascript" src="calendar1/js/language/de-DE.js"></script>
	<script type="text/javascript" src="calendar1/js/language/el-GR.js"></script>
	<script type="text/javascript" src="calendar1/js/language/it-IT.js"></script>
	<script type="text/javascript" src="calendar1/js/language/hu-HU.js"></script>
	<script type="text/javascript" src="calendar1/js/language/pl-PL.js"></script>
	<script type="text/javascript" src="calendar1/js/language/pt-BR.js"></script>
	<script type="text/javascript" src="calendar1/js/language/ro-RO.js"></script>
	<script type="text/javascript" src="calendar1/js/language/es-CO.js"></script>
	<script type="text/javascript" src="calendar1/js/language/es-MX.js"></script>
	<script type="text/javascript" src="calendar1/js/language/es-ES.js"></script>
	<script type="text/javascript" src="calendar1/js/language/es-CL.js"></script>
	<script type="text/javascript" src="calendar1/js/language/es-DO.js"></script>
	<script type="text/javascript" src="calendar1/js/language/ru-RU.js"></script>
	<script type="text/javascript" src="calendar1/js/language/sk-SR.js"></script>
	<script type="text/javascript" src="calendar1/js/language/sv-SE.js"></script>
	<script type="text/javascript" src="calendar1/js/language/zh-CN.js"></script>
	<script type="text/javascript" src="calendar1/js/language/cs-CZ.js"></script>
	<script type="text/javascript" src="calendar1/js/language/ko-KR.js"></script>
	<script type="text/javascript" src="calendar1/js/language/zh-TW.js"></script>
	<script type="text/javascript" src="calendar1/js/language/id-ID.js"></script>
	<script type="text/javascript" src="calendar1/js/language/th-TH.js"></script>
	<script type="text/javascript" src="calendar1/js/calendar.js"></script>
	<script type="text/javascript" src="calendar1/js/app.js"></script>

	
</div>
<footer class="footer-m">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-sm-12 col-xs-12">
                <img src="images/footer-logo.png" />
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Phone: +63 917 539 8008</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h4>Email: start@servrevo.com</h4>
            </div>
            <div class = "col-sm-12 col-xs-12">
                <h5>Copyright 2016 ServRevo Corp.</h5>
            </div>
        </div>
    </div>
</footer>

<footer class="footer">
    <div class = "container">
        <div class = "footer-nav">
            <a href="#"><img class="logo" src="images/footer-logo.png"></a>
            <p>Copyright 2016 ServRevo Corp.</p>
        </div>
        <div class = "footer-nav-items">
            <ul>
                <li><a>Phone: +63 917 539 8008</a></li>
                <li><a>Email: start@servrevo.com</a></li>
            </ul>
        </div>
    </div>
</footer>

</body>
</html>
