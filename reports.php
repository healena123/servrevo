<?php
session_start();

include ('config.php');

if(session_id() == '' || !isset($_SESSION['email']) ) {
	header("Location: ".BASE_URI);
} else {
	if($_SESSION['access_id'] == '4'){
		header("Location: ".BASE_URI);
	}
}



error_reporting(0);

$user_id = $_GET['user'];
date_default_timezone_set('Asia/Manila');


$latest_users = "select * from Users where is_deleted != '1' ORDER BY date_created DESC";

?>
<!DOCTYPE html>
<html class="nojs html css_verticalspacer" lang="en-US" style="height:100%">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Home</title>
    <!-- CUSTOM STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome/font-awesome.min.css"/>

    <link rel="stylesheet" href="css/dataTables.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/datatable.responsive.css">
    <link rel="stylesheet" href="css/fixedColumns.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
		<link rel="stylesheet" type="text/css" href="css/admin.css"/>



    <link rel="stylesheet" type="text/css" href="css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="css/dashboard-header.css"/>
    <link rel="stylesheet" type="text/css" href="css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
		<link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
		<link href='css/fullcalendar.min.css' rel='stylesheet' />
		<link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114355569-1"></script>
		<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());

					gtag('config', 'UA-114355569-1');
		</script>
		<style>
		body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
		.w3-row-padding img {margin-bottom: 12px}
		.bgimg {
		    background-position: center;
		    background-repeat: no-repeat;
		    background-size: cover;
		    background-image: url('/w3images/profile_girl.jpg');
		    min-height: 100%;
		}
		.dropbtn {
		    background-color: inherit;
		    color: black;
		    padding: 10px;
		    font-size: 16px;
		    border: 1px solid grey;
		    width:20em;
		}

		.dropdown {
		    position: relative;
		    display: inline-block;
		}

		.dropdown-content {
		    display: none;
		    position: absolute;
		    background-color: #f1f1f1;
		    min-width: 160px;
		    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		    z-index: 1;
		    width:16.3em;
		}

		.dropdown-content a {
		    color: black;
		    padding: 12px 16px;
		    text-decoration: none;
		    display: block;
		}

		.dropdown-content a:hover {background-color: #ddd}

		.dropdown:hover .dropdown-content {
		    display: block;
		}

		.dropdown:hover .dropbtn {
		    background-color: #3e8e41;
		}
		table {
		    border-collapse: collapse;
		    border-spacing: 0;
		    width: 100%;
		    border: 1px solid #ddd;
		}

		th, td {
		    text-align: left;
		    padding: 8px;
		}

  #calendar {
    max-width: 900px;
    margin: 40px auto;

  }
  .fc-view-container {
  	background:#fff
  }
	.fc-header-toolbar h2 {
		font-size:26px;
	}
	.fc-content {
		font-size:12px;
	}
	.dataTables_info {font-size:10px}
	.pagination-mds * {font-size:10px}
	.search-sm {position:relative;}
	.search-icon {position: absolute;
    top: 3px;
    left: 5px;
    z-index: 2;
    color: #999;}
	.search-sm input {z-index:0}
	.dataTables_wrapper .dataTables_filter input {margin-left:0 !important;min-width:230px;padding-left:30px}
	table.dataTable tbody td * {font-size:12px;}
	.dt-buttons {visibility:hidden}
</style>
</head>
<body class="serve-revo-admin">
<input type="hidden" value="<?php echo BASE_URI; ?>" class="baseurl">
<?php include('dashboard_header.php'); ?>


<div class="content-wrap">
    <div class="left">
    	<div class="accordion-menu">
    		<div class="col">
  				<div class="acc-menu-link">
  					<a data-toggle="collapse" href="<?php echo BASE_URI; ?>dashboard.php?user=<?php echo $user_id; ?>" data-target="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
  						<i class="fa fa-folder"></i> Dashboard
  					</a>
    			</div>
    		</div>

  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_bookings.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Client Bookings
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>clients.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
  						<i class="fa fa-folder"></i> Users
  					</a>
    			</div>

  			</div>
  			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>client_history.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Archive
  					</a>
    			</div>

  			</div>
			<div class="col">
  				<div class="acc-menu-link">
  					<a class="collapsed" href="<?php echo BASE_URI; ?>reports.php?user=<?php echo $user_id; ?>" data-toggle="collapse" data-target="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample3">
  						<i class="fa fa-folder"></i> Reports
  					</a>
    			</div>
  			</div>
			</div>
    </div>

		<!-- Right Content -->
		<div class="right">
			<div style="background:#fff;padding:10px 20px 10px 20px;margin-bottom:10px">
				<h3 class="" style="text-align:left;font-weight:normal;color:#87a900;margin:0">Reports</h3>
				<div class="breadcrumbs" style="padding:0;margin-top:3px">
					<a href="" style="color:#87a900;opacity:0.6;font-weight:normal;font-size:14px;">Home</a>
					<span style="color:#999;font-weight:normal;font-size:14px;">  >  </span>
					<a style="color:#999;font-weight:normal;font-size:14px;">Reports</a>
				</div>
			</div>
		</div>

		<div class="w3-center">
		<div class="container">
			<div class="row" >
				<div class="col-sm-2" style="margin-top:1em;"></div>
				<div class="col-sm-4" style="margin-top:1em;">
					<div class="row" style="background:#fff; padding:1em;">

							<h3 style="text-align:center;">Reports</h3>
							<br />
								<form id="processReservation" action="<?php echo BASE_URI; ?>process_reserve.php" method="post" autocomplete="off">
							<div class="dropdown" >
							  <button class="dropbtn">Select Category</button>
							  <div class="dropdown-content">
							    <a href="#">Sales Report</a>
							  </div>
							</div>


							<div class="text-center err-msg"></div>
							<input type="hidden" name="reservation_dates" id="reservation-dates">
							<input type="hidden" name="reservation_seats" id="reservation-seats">
							<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
							<input type="hidden" name="total_days_reserved" id="total-days">
							<input type="hidden" name="total_seats_reserved" id="total-seats">
							<input type="hidden" name="total_reservation_amt" class="total-amt-hdn" value="0">

							<div class="row">
								<div class="col-sm" style="display:inline-block;text-align:right;">
									<label class="col-xs-4" style="margin-left:1px;">Start</label>
									<span class="col-xs-7" style="padding:0;">
										<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
										<input type="text" value="<?php echo $start; ?>" placeholder="Start Date" style="padding-left:30px;" name="date_from" onkeyup="getnewdate()" class="form-control required datepicker-input" id="datepickerInput1" size="50">
									</span>
								</div>
								<div class="col-sm" style="display:inline-block;text-align:right;">
									<label class="col-xs-4" style="margin-left:2px;">End</label>
									<span class="col-xs-7" style="padding:0;">
										<i class="fa fa-calendar" style="position:absolute;top:9px;left:10px;color:#ccc;font-size:14px;"></i>
										<input type="text" value="<?php echo $end; ?>" placeholder="End Date" style="padding-left:30px;" name="date_to" class="form-control datepicker-input" id="datepickerInput2" size="50">
									</span>
								</div>

					<div class="reserve-other-info" style="display:inline-block;width:100%;font-size:16px;font-weight:400;margin-bottom:20px;">


					</div>
					</div>

					<div class="input">
					      <!--<button type="button" id="checkAvailabilityBtn" class="btn btn-serve-callback">Check Availability</button>-->

					      <button type="button" id="reserveBtn" class="btn btn-serve-callback" style=" background-color: #A1D03C   !important;width:90%;margin-left:1.5em;">Search</button>

					  </div>
					<br/>

					</form>
					</div>
				</div>

				<div class="col-sm-6" style="margin-top:1em;">
					<div style="background:#fff; padding:1em;">
						<div>
							<header class="w3-container w3-center" style="padding:60px 16px" id="home">
								<h1 class="w3-jumbo"><b>Sales Report</b></h1>
								<p>Start date - End date</p>

								<button class="w3-button w3-light-grey w3-padding-large w3-margin-top">
									<i class="fa fa-download"></i> Download data
								</button>
							</header>

							<!-- About Section -->
							<div class="w3-content w3-justify w3-text-grey w3-padding-32" id="about" style="margin-top:-5em;">
								<h2>Table</h2>
								<hr class="w3-opacity">
							 <div style="overflow-x:auto;">
							<table>
								<tr>
									<th>Booking ID</th>
									<th>Date</th>
									<th>No. of seats</th>
									<th>Total Amount</th>

								</tr>
								<tr>
									<td>#123456</td>
									<td>05-17-18</td>
									<td>7</td>
									<td>4000</td>

								</tr>
								<tr>
									<td>#789123</td>
									<td>08-11-99</td>
									<td>11</td>
									<td>734</td>

								</tr>
								<tr>
									<td>#626489</td>
									<td>05-30-97</td>
									<td>14</td>
									<td>67</td>

								</tr>
							</table>
						</div>

						</div>
					</div>
				</div>
			</div>
		</div>

</div>

<!-- PLUGIN SCRIPTS -->
<script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/co-working.js" type="text/javascript"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/jquery-ui.min.js"></script>

<script type="text/javascript">
var base_url = $('.baseurl').val();

function init(){
	var currentDate = new Date();

	var m = currentDate.getMonth()+1;
	var d = currentDate.getDate();
	m = m<10?'0'+m:m;
	d = d<10?'0'+d:d;
	var date_from = '';
	var date_to = '';
	date_from = currentDate.getFullYear() +'-'+m+'-'+d;
	date_to = currentDate.getFullYear() +'-'+m+'-'+d;
	var check = "<?php echo $_GET['check']; ?>";

	if(check != ''){
		date_from = "<?php echo $start; ?>";
		date_to = "<?php echo $end; ?>";
	}
		var w = date_from + ','+date_to;
		getAvailableSeats(w);
}
$(document).ready(function(){
	seatPlacement('');
	init();

})
$( document ).ready(function() {
});
$(document).on('focus', '.datepicker-input', function(){

	    var dateToday = new Date();
	    var dates = $("#datepickerInput1, #datepickerInput2").datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: dateToday,
				beforeShowDay: noWeekendsOrHolidays,
	      onSelect: function(selectedDate) {
	          var option = this.id == "datepickerInput1" ? "minDate" : "maxDate",
	          instance = $(this).data("datepicker"),
	          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	          dates.not(this).datepicker("option", option, date);
									console.log(selectedDate);
									getAvailableSeats('');
									updateReservationDetails();
									computeTotalAmount();
	      }
	    });
	// var dateToday = new Date();
	// $(this).datepicker({
	// 	dateFormat: 'yy-mm-dd',
	// 	minDate: dateToday,
	// 	beforeShowDay: noWeekendsOrHolidays,
	// 	onSelect: function(dateStr) {
	// 			console.log(dateStr);
	// 			getAvailableSeats('');
	// 			updateReservationDetails();
	// 			computeTotalAmount();
	// 		},
	// 		onClose: function(selectedDate) {
	//         	$('.content-loader').removeClass('hidden');
	//         	if(selectedDate == '') $('.content-loader').addClass('hidden');
	//       	}
	// 	});
})
function getAvailableSeats(w){
	if(w==''){
	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	w = date_from + ','+date_to;
}
	var url = base_url+'reserved_seats.php?dates='+w;

	$.post(url, '', function (data) {
		//clear Seats selection
		$('#total-seats').val(0);
		$('#total-seats-reserve').val(0);

		arr=[];
		if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
				$('.content-loader').addClass('hidden');
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});
			$('.content-loader').addClass('hidden');
		}

		});
}
$(document).on('click', '#checkAvailabilityBtn', function(){

	var date_from = $('#datepickerInput1').val() != '' ? $('#datepickerInput1').val() : '';
	var date_to = $('#datepickerInput2').val()  != '' ? $('#datepickerInput2').val() : '';
	var w = date_from + ','+date_to;

	if(date_from == ''){
		$('.err-msg').append('Please select dates.');
	} else {
		$(this).html('<img src="'+base_url+'images/ajax-loader.gif">');
		$('.seat-placement-holder').css({'opacity':'0.5'});

		$('.err-msg').empty();
		var url = base_url + 'reserved_seats.php?dates='+w;

		$.post(url, '', function (data) {
			if(data.length){
			setTimeout(function(){
				$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
				$('.seat-placement-holder').css({'opacity':'1'});
			}, 1000);
			setTimeout(function(){
				data = $.parseJSON(data);
	    		$('#seatsAvailablePerDate').empty();
	    		if(data.length != 0){
	    			seatPlacement(data);
	    		} else {
	    			seatPlacement('');
	    		}
	    	}, 1200);
		} else {
			$('#checkAvailabilityBtn').html('CHECK AVAILABILITY');
			$('.seat-placement-holder').css({'opacity':'1'});

		}

		});
	}
});
var arr = [];
$(document).on('change', '#seatPlacement .chk', function(){

	var el = $(this).val();

	$(this).parent().find('input, label').addClass('selected');

	arr.push(el);


	if (!$(this).is(":checked")) {
		for(var i = arr.length; i--;) {
          if(arr[i] === el) {
              arr.splice(i, 1);
          }
        }
	}

	$('.no-of-seats').html(arr.length);
	$('#total-seats-reserve').val(arr.length);
	$('#total-seats').val(arr.length);



	var p = "";
	var total = 0;
	$.each(arr, function(i,v){
    	p += v+',';

    });
    $('#reservation-seats').val(p);


	if(arr.length > 0) {
		$('.reserve-other-info').removeClass('hidden');
		$('#checkAvailabilityBtn').addClass('hidden');
	} else {
		$('.reserve-other-info').addClass('hidden');
		$('#checkAvailabilityBtn').removeClass('hidden');
	}
	updateReservationDetails();
	computeTotalAmount();
});
$(document).on('click', '#reserveBtn', function(e){

		if($('#datepickerInput1').val() == '' || $('#datepickerInput2').val() == ''){
			$('.err-msg').html('Fields cannot be empty.');
			e.preventDefault();
		} else if($('#total-seats-reserve').val() == 0){
			e.preventDefault();
			$('.err-msg').html('Please select seats.');
		}else{
			var userid = "<?php echo $_GET['user'];?>";
			if(userid == ''){
					var strtdt = document.getElementById('datepickerInput1').value;
					var enddt = document.getElementById('datepickerInput2').value;
					var url = base_url+'login.php?strt='+strtdt+'&end='+enddt+'&check=1';
					window.location.href = url;
			}
			else{
				$('.err-msg').empty();
				$('#processReservation').submit();
			}
		}


});
function computeTotalAmount(){
	var d = ($('#total-days').val() != '') ? $('#total-days').val() : 0;
	var s = ($('#total-seats').val() != '')?$('#total-seats').val() : 0;
	var p = 400;
	var total = parseInt(d)*parseInt(s)*parseInt(p);

	$('.total-amt-hdn').val(total);
	$('.seat-total-amt').text(total);
}
function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }
function seatPlacement(data){

	var content = '';
		var s_id = 0;
		for(var i=1;i<=9;i++){
			content += '<div class="entry-tbl tbl-"'+i+'">';


        	for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";
        		var reserved_class="reserved";

        	//content += '<span><input type="checkbox" name="seat_no[]" value="'+seat_id+'" '+check_class+'></span>';
        		content += '<span style="position:relative;">'
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';

        					}
        				}
        				el += ' class="chk reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk reserved"></label>'

				content += '</span>';
        	}
        	content += '</div>';
        	}

        	content += '</div>';

        	if(i%3==0){
        		content += '<div class="col-xs-12" style="display:inline-block;width:100%;float:left:margin:0;"></div>';
        	}
        }
        // LAST TABLE
        content += '<div class="entry-tbl last-tbl-placement tbl-'+i+'">';

        for(var x=1;x<=2;x++){
        	var seat_class = '';
        	if(x==1){
        		seat_class = 'l-seat';
        	}
        	if(x==2){
        		seat_class = 'r-seat';
        		content += '<div class="tbl-bg"></div>';
        	}
        	content += '<div class="'+seat_class+'">';
        	for(var s=1;s<=3;s++){
        		s_id++;

        		var seat_id = '';
        		if(x==1){
        			seat_id = s;
        		}
        		if(x==2){
        		 	seat_id = s+3;
        		}
        		var check_class="";

        		content += '<span style="position:relative;">';
				console.log(data.length);
					if(data.length){

						var el = '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'"';

        				for(var y=0;y<data.length;y++){

        					if(data[y].seat_id == s_id){
        						 el += ' checked="checked"';
        						 el += ' disabled';
        					}
        				}
        				el += ' class="chk chk1 reserved">';
        				content += el;

        			}else {

        				content += '<input type="checkbox" name="seat_no" value="'+s_id+'" id="seatId'+s_id+'" class="chk chk1">'

        			}
					content += '<label for="seatId'+s_id+'" class="lbl-chk lbl-chk1 reserved"></label>'

				content += '</span>';

        	}
        	content += '</div>';
        }
        content += '</div>';


        $('#seatPlacement').html(content);
        $('.reservation-legend-holder').removeClass('hidden');
	}
$(document).on('change', '.datepicker-input', function(){
	var el = $(this).val();
	updateReservationDetails();
});
function updateReservationDetails(){
		var start = $("#datepickerInput1").datepicker("getDate"),
   		end = $("#datepickerInput2").datepicker("getDate"),
    	currentDate = new Date(start.getTime()),
    	between = [],
    	count = 0,
    	sd,
		reservation_dates = '',
		start_date = changeDateFormat(start),
		date_range="";
		console.log(end);
		//COUNT NUMBER OF DAYS
		//LIST DATES BETWEEN DATE RANGE
		if(start != null && end != null) {
			while (currentDate <= end) {

				if(currentDate.getDay() < 6 && currentDate.getDay() != 0)
					between.push(new Date(currentDate));
    			currentDate.setDate(currentDate.getDate() + 1);
			}
			count = between.length;
		}
		if(start != null && end == null){
			count = 1;
		}

		//CHANGE FORMAT OF DATES ALL DATES
		//SAVE TO INPUT HIDDEN

		if(start_date != null) {
			reservation_dates = $("#datepickerInput1").val();
		}
		if(start != null && end != null) {
			reservation_dates = "";
			$.each(between, function(i,v){

				sd = new Date(v);
				var m = sd.getMonth() + 1;
				var d = sd.getDate();
				m = m < 10? '0'+m: m;
				//d = d < 10? '0'+d: d;
				var nd = sd.getFullYear()+'-'+m+'-'+d;
				reservation_dates += nd+',';
			});
		}
		//FORMAT DATE RANGE FOR VIEW
		if(start != null && end != null) {
			date_range = changeDateFormat(start) +' - '+ changeDateFormat(end);
		}else {
			if(start != null && end == null)
				date_range = changeDateFormat(start);
			if(start == null && end != null)
				date_range = changeDateFormat(end);
		}
		//DISPLAY INFO
		$('#total-days').val(count);
		$('.date-range-view').text(date_range);
		$('#reservation-dates').attr('value', reservation_dates);
	}
function changeDateFormat(date){

	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

	var d = new Date(date);
	return monthNames[d.getMonth()] + ' '+ d.getDate() + ', ' + d.getFullYear();
}
function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}
var natDays = [
  [1, 1, 'ph'],
  [12, 25, 'ph']
];
function nationalDays(date) {
    for (i = 0; i < natDays.length; i++) {
      if (date.getMonth() == natDays[i][0] - 1
          && date.getDate() == natDays[i][1]) {
        return [false, natDays[i][2] + '_day'];
      }
    }
  return [true, ''];
}
/*$(".selector").datepicker({ beforeShowDay: noWeekendsOrHolidays})

function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}*/
/*$(document).on('click', '.datepicker-input', function(ev){
	ev.preventDefault();
	$('#datepicker-modal').removeData();
	$('#datepicker-modal').val(null).trigger("change");
	var el = $(this).attr('id');

	$('#datepicker-modal').modal('show');
	var dateToday = new Date();

	jQuery("#datepicker").datepicker({
		minDate: dateToday,
    	onSelect: function (dateText, inst) {
    		var dates = addOrRemoveDate(dateText);

			$(this).data('datepicker').inline = true;

			$('#datepicker-modal').modal('hide');
        },
        onClose: function() {
    		$(this).data('datepicker').inline = false;
		},
        beforeShowDay: function (date) {
            var year = date.getFullYear();
            // months and days are inserted into the array in the form, e.g "01/01/2009", but here the format is "1/1/2009"
            var month = padNumber(date.getMonth() + 1);
            var day = padNumber(date.getDate());
            // This depends on the datepicker's date format
            var dateString = month + "/" + day + "/" + year;

            var gotDate = jQuery.inArray(dateString, dates);
            if (gotDate >= 0) {
                // Enable date so it can be deselected. Set style to be highlighted
                return [true, "ui-state-highlight"];
            }
            // Dates not in the array are left enabled, but with no extra style
            return [true, ""];
        }

    });
});
// Maintain array of dates
var dates = new Array();

function addDate(date) {
    if (jQuery.inArray(date, dates) < 0) {
        dates.push(date);
        $('#selected-dates').val(dates);
   	}
   	$('#reservation-dates').val(dates);

   	$('#dateSelected').empty();
   	var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    }else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

   	$.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });

}

function removeDate(index) {
    dates.splice(index, 1);
    $('#selected-dates').val(dates);
    $('#reservation-dates').val(dates);

    $('#dateSelected').empty();

    var txt = ' Date';
    if(dates.length > 1){
    	txt = ' Dates';
    } else {
    	txt = ' Date';
    }
    $('#datepicker-input').val(dates.length + txt + ' selected');

    $('#datepicker-input').val(dates.length + ' selected');
    $.each(dates, function(i,v){
    	$('#dateSelected').append('<div>'+v+'</div>');
    });
}

// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
    var index = jQuery.inArray(date, dates);
    if (index >= 0) {
        removeDate(index);

    } else {
        addDate(date);

     }

}*/
// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
    var ret = new String(number);
    if (ret.length == 1)
        ret = "0" + ret;
    return ret;
}
//jQuery(function () {
$(document).on('click', '#completeRegisterBtn', function(){
	$('#completeRegisterForm').submit();
});
//});

$(document).on('click', '#registerBtn', function(){
	var err=false;
	$('#createAccount .required').each(function(){
		var el = $(this).val();
		if(el == ''){
			$(this).parent().addClass('has-error');
			err=true;
		} else {
			$(this).parent().removeClass('has-error');
			err=false;
		}
	})

	if(err==true){
		$('.err-msg').removeClass('invi');

	}else{
		$('.err-msg').addClass('invi');
		$('#createAccount').submit();
	}
});
</script>

</body>
</html>
